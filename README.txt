ASSIGNMENT 3 GS2
CATERPILLAR LANE

The game requires fullscreen to run.

Rabbit Rush v0.1
Visit us at www.rabbit-rush.com for more info!
(next version slated for early November release)
----------------------------------------------------------
Controls
----------------------------------------------------------
Esc to exit fullscreen to quit the game
Saving is automatic

----------------------------------------------------------
Credits
----------------------------------------------------------
Main Team
Laura Hales - game 2 3d modelling
Sarah Lin - game 2 3d modelling
Tamara Clarke - game 2 3d modelling
Kevin Chen - programmer

Others
Joe Liu - game 1 2d art
Peter Halasz - game 3 photography
Rebekah Lin - game 2 SFX

----------------------------------------------------------
Resources
----------------------------------------------------------
Special thanks to Radio Giraffe for his music
Special thanks to Christopher Garcia, Radio Giraffe
for letting us use his music. https://soundcloud.com/radiogiraffe

Other resources

BGM - Gymnopedie No. 3 by Kevin MacLeod
Freesounds.org
contador by zorrodg
yay by zut50
wow by willy-ineedthatappcom
locked-door2 by leady
dorm-door-opening by pagancow
keys-jangle-01 by koops
door-handle-jiggle-05 by d-w
wooden-close by simpsi
heavy-thud 1 by arithni
smashing-1 by timbre
concrete1 by faceonmars
www.bfxr.net for sound effects
UltimateFPS by VisionPunk
UnitySerializer by Mike Talbot
Pool manager by path-o-logical
textures from CGtextures.com
Minercraftory by Jayvee D. Enaguas
