#pragma strict

//fades camera in and out during load and death

var noFirstFade: boolean;

var fadeOutTexture : Texture2D;
var fadeSpeed = 1.0;

var drawDepth = -1000;

private var alpha = 0.5; 
private var fadeDir = -1; 

function OnGUI()
{
 	//AudioListener.volume = Mathf.Lerp(AudioListener.volume, (-0.5*fadeDir + 0.5), 5*Time.deltaTime); //fades audio out
	alpha += fadeDir * fadeSpeed * Time.deltaTime;	
	alpha = Mathf.Clamp01(alpha);	

	GUI.color.a = alpha;
 
	GUI.depth = drawDepth;
 
	GUI.DrawTexture(Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
}

function fadeIn(time: float)
{
	fadeSpeed = time;
	fadeDir = -1;	
}

function fadeOut(time: float)
{
	fadeSpeed = time;
	fadeDir = 1;	
}
 
function fadeInFirst(time: float)
{
	fadeDir = -1;	
}

function Start()
{
	if(!noFirstFade)
	{
		alpha=1;
		fadeInFirst(1);
	}
}