#pragma strict

var texPress: Texture2D;
var texRelease: Texture2D;

var text: GameObject; text = transform.Find("Text").gameObject;
var textText: String;

var colorNorm: Color;
var colorEnter: Color;
var colorDown: Color;
var colorDisabled: Color;

var hover: boolean;
var disabled: boolean;

function Start ()
{
	text.GetComponent(TextMesh).text = textText;
}

function Update ()
{
	if(disabled)
	{
		renderer.material.color = colorDisabled;
		renderer.material.mainTexture = texRelease;
		text.transform.localPosition.z = -0.025;
		text.GetComponent(TextMesh).renderer.material.color = colorDisabled;
	}
	else
	{
		if(!hover)
		{
			renderer.material.color = colorNorm;
			renderer.material.mainTexture = texRelease;
		}
		text.GetComponent(TextMesh).renderer.material.color = (renderer.material.color + Color.white)/2;
	}
}

function OnMouseEnter()
{
	hover = true;
	if(!disabled)
	{
		renderer.material.color = colorEnter;
	}
}

function OnMouseDown()
{
	if(!disabled)
	{
		renderer.material.color = colorDown;
		renderer.material.mainTexture = texPress;
		text.transform.localPosition.z = 0.025;
	}
}

function OnMouseUp()
{
	if(!disabled)
	{
		renderer.material.color = colorEnter;
		renderer.material.mainTexture = texRelease;
		text.transform.localPosition.z = -0.025;
	}
}

function OnMouseExit()
{
	if(!disabled)
	{
		renderer.material.color = colorNorm;
	}
}

function OnMouseUpAsButton()
{
	hover = false;
	if(!disabled)
	{
		gameObject.SendMessage("Press");
	}
}