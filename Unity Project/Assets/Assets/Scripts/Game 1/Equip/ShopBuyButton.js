#pragma strict
import System.Collections.Generic;

var upgradeScript: Game1SaveData;
var itemList: List.<StoreItem>; itemList = new List.<StoreItem>();

var init: boolean;

var number: int;
var upgrade: Upgrade;

var hover: boolean;

function Start ()
{
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	renderer.material.color = Color(1,0.55,0.45,1);
}

function Update ()
{
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	if(!init)
	{
		itemList = upgradeScript.itemList;
		
		//sets upgrade
		upgrade = upgradeScript.upgradesList[itemList[number-1].upgrade];
		//changes sprite to show
		renderer.material.mainTexture = upgrade.icon;
		init = true;
	}
	
	if(itemList[number-1].bought)
	{
		renderer.material.mainTexture = Resources.Load("Icon18");
		renderer.material.color = Color(1,1,1,0.4);
	}
	if(!hover)
	{
		if(!itemList[number-1].bought && ShopControl.selectedItem != gameObject)
		{
			renderer.material.color = Color(1,0.55,0.45,1);
		}
		if(!itemList[number-1].bought && ShopControl.selectedItem == gameObject)
		{
			renderer.material.color = Color(1,0.9,0.9,1);
		}
	}
}

function OnMouseDown()
{
	if(!itemList[number-1].bought)
	{
		renderer.material.color = Color(1,0.25,0.25,1);
	}
	ShopControl.selectedItem = gameObject;
}

function OnMouseEnter()
{
	if(!itemList[number-1].bought)
	{
		renderer.material.color = Color(1,1,0.5,1);
	}	
	hover = true;
}

function OnMouseExit()
{
	if(!itemList[number-1].bought && ShopControl.selectedItem != gameObject)
	{
		renderer.material.color = Color(1,0.55,0.45,1);
	}
	if(!itemList[number-1].bought && ShopControl.selectedItem == gameObject)
	{
		renderer.material.color = Color(1,0.9,0.9,1);
	}
	hover = false;
}

function OnMouseUp()
{
	if(!itemList[number-1].bought && ShopControl.selectedItem != gameObject)
	{
		renderer.material.color = Color(1,0.55,0.45,1);
	}
	if(!itemList[number-1].bought && ShopControl.selectedItem == gameObject)
	{
		renderer.material.color = Color(1,0.9,0.9,1);
	}
}