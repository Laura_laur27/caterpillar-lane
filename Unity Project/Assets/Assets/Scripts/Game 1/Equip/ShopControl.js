#pragma strict
import System.Collections.Generic;

static var selectedItem: GameObject;

var upgradeScript: Game1SaveData;
var itemList: List.<StoreItem>; itemList = new List.<StoreItem>();
var item1: StoreItem;
var item2: StoreItem;
var item3: StoreItem;
var item4: StoreItem;

var confirmButton: GameObject; confirmButton = GameObject.Find("Buy Confirm Button");

var textName: GameObject; textName = GameObject.Find("Text Name");
var textLvl: GameObject; textLvl = GameObject.Find("Text Level");
var textBlurb: GameObject; textBlurb = GameObject.Find("Text Blurb");
var textGold: GameObject; textGold = GameObject.Find("Text Gold");
var textCost: GameObject; textCost = GameObject.Find("Text Cost");
var textDistance: GameObject; textDistance = GameObject.Find("Text Distance");

var costList = new int[5]; costList = [500, 750, 1250, 2000, 4000];
var costs = new int[4];
var currcost: int;
var canBuy: boolean; canBuy = false;
var bought: boolean; bought = false;

function Start ()
{
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	itemList = upgradeScript.itemList;
	
	//if there are no set stores
	if(itemList.Count <= 0)
	{
		var i: int;
		//active 1
		var upgradeNo: int;
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(0, 6);
			if(upgradeScript.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		var level: int; level = upgradeScript.upgradesList[upgradeNo].level;
		var cost: int; cost = 2*costList[upgradeScript.upgradesList[upgradeNo].level];
		item1.num = 1;
		item1.upgrade = upgradeNo;
		item1.level = level + 1;
		item1.bought = false;
		item1.cost = cost;
		upgradeScript.itemList.Add(item1);
		
		//item 2
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(0, 6);
			if(upgradeScript.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		level = upgradeScript.upgradesList[upgradeNo].level;
		cost = 2*costList[upgradeScript.upgradesList[upgradeNo].level];
		item2.num = 2;
		item2.upgrade = upgradeNo;
		item2.level = level + 1;
		item2.bought = false;
		item2.cost = cost;
		upgradeScript.itemList.Add(item2);
		
		//item 3
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(6, 18);
			if(upgradeScript.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		level = upgradeScript.upgradesList[upgradeNo].level;
		cost = costList[upgradeScript.upgradesList[upgradeNo].level];
		item3.num = 3;
		item3.upgrade = upgradeNo;
		item3.level = level + 1;
		item3.bought = false;
		item3.cost = cost;
		upgradeScript.itemList.Add(item3);
		
		//item 4
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(6, 18);
			if(upgradeScript.upgradesList[upgradeNo].level != 5 && upgradeNo != upgradeScript.itemList[2].upgrade)
			{
				break;
			}
		}
		level = upgradeScript.upgradesList[upgradeNo].level;
		cost = costList[upgradeScript.upgradesList[upgradeNo].level];
		item4.num = 4;
		item4.upgrade = upgradeNo;
		item4.level = level + 1;
		item4.bought = false;
		item4.cost = cost;
		upgradeScript.itemList.Add(item4);
		
		/*
		//trigger special item
		if(PlayerPrefs.GetInt("StoreItem1") == PlayerPrefs.GetInt("StoreItem2"))
		{
			SpecialItem();
		}
		*/
	}
	
	//selectedItem = GameObject.Find("Buy Button 1");
}

function Update ()
{
	if(selectedItem != null)
	{
		currcost = upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].cost;
	}

	//sees if can buy
	if(selectedItem != null && Game1SaveData.totalGold >= currcost && upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].bought == false)
	{
		canBuy = true;
		confirmButton.GetComponent(ShopConfirmButton).canBuy = true;
	}
	else
	{
		canBuy = false;
		confirmButton.GetComponent(ShopConfirmButton).canBuy = false;
	}
		
	//TEXT if none selected
	if(selectedItem == null)
	{
		textName.GetComponent(TextMesh).text = "Upgrade";
		textLvl.GetComponent(TextMesh).text = "";
		textBlurb.GetComponent(TextMesh).text = "";
		textCost.GetComponent(TextMesh).text = "";
	}
	else
	{
		if(!itemList[selectedItem.GetComponent(ShopBuyButton).number-1].bought)
		{
			textName.GetComponent(TextMesh).text = selectedItem.GetComponent(ShopBuyButton).upgrade.name;
			textLvl.GetComponent(TextMesh).text = (upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].level).ToString();
			textBlurb.GetComponent(TextMesh).text = BreakLine(selectedItem.GetComponent(ShopBuyButton).upgrade.blurb, 33) + "\n" + "\n" + BreakLine(selectedItem.GetComponent(ShopBuyButton).upgrade.blurbLvl, 33);
			textCost.GetComponent(TextMesh).text = currcost.ToString();
		}
		else
		{
			textName.GetComponent(TextMesh).text = "SOLD OUT!";
			textLvl.GetComponent(TextMesh).text = "";
			textBlurb.GetComponent(TextMesh).text = "";
			textCost.GetComponent(TextMesh).text = "---";
		}
	}

	textGold.GetComponent(TextMesh).text = Game1SaveData.totalGold.ToString();
	textDistance.GetComponent(TextMesh).text = (Game1SaveData.nextDistance - Game1SaveData.totalDistance).ToString("F0");
}

function Buy ()
{
	//checks if enough money - if so, buy item
	if(canBuy)
	{
		Game1SaveData.totalGold -= currcost;
		upgradeScript.upgradesList[selectedItem.GetComponent(ShopBuyButton).upgrade.num].level += 1;
		
		//sets playerprefs
		upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].bought = true;
	}
}

function BreakLine (input: String, length: int): String
{
	//splits string
	var words: String[];
	var result: String; result = "";
	var line: String; line = "";
	words = input.Split(" "[0]);
	for(word in words)
	{
		var temp: String;
		temp = line + " " + word;
		if(temp.Length > length)
		{
			result += line + "\n";
			line = word;
		}
		else
		{
			line = temp;
		}
	}
	
	result += line;
	
	return result.Substring(1,result.Length-1);
}

function SpecialItem ()
{

}