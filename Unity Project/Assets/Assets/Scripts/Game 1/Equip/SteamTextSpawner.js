#pragma strict
import System.Collections.Generic;

var timer: float; timer = 0;
var element: int; element = 0;

var textList: List.<String>; textList = new List.<String>();

var stringList = new String[12]; stringList = ["lol","you srtill playing that game","still*","lol you should come over >.>","bored as hell","ah fuckit I’m heading over","catch","lol before I forget", "do you know about the hack>","in the shop, press the carrot 4 times","saw it on a forum","catch"];
var timeList = new float[12]; timeList = [8.0,4,2,6,2.5,5,2,4,3.5,3,3,2];
var timeFlutter: float; timeFlutter = (Random.value-0.5)*1;

var text1: GameObject; text1 = GameObject.Find("Steam Text 5");
var text2: GameObject; text2 = GameObject.Find("Steam Text 4");
var text3: GameObject; text3 = GameObject.Find("Steam Text 3");
var text4: GameObject; text4 = GameObject.Find("Steam Text 2");
var text5: GameObject; text5 = GameObject.Find("Steam Text 1");
var box: GameObject; box = GameObject.Find("Box");

function Awake ()
{
	DontDestroyOnLoad (transform.gameObject);
}
	
function Start ()
{
	for(var i = 0; i<5; i++)
	{
		textList.Add("");
	}
}

function Update ()
{
	timer += Time.deltaTime;
	
	//after x seconds, add string
	if(element < 12 && timer>(timeList[element]+timeFlutter)*1.5)
	{
		audio.Play();
		
		//adds string to text list
		textList.Add(stringList[element]);
		if(textList.Count >= 6)
		{
			textList.RemoveAt(0);
		}
		
		timeFlutter = (Random.value-0.5)*1;
		
		//updates text
		text1.guiText.text = textList[4];
		text2.guiText.text = textList[3];
		text3.guiText.text = textList[2];
		text4.guiText.text = textList[1];
		text5.guiText.text = textList[0];
		
		//refreshes cooldown
		text5.GetComponent(SteamText).timer = text4.GetComponent(SteamText).timer;
		text4.GetComponent(SteamText).timer = text3.GetComponent(SteamText).timer;
		text3.GetComponent(SteamText).timer = text2.GetComponent(SteamText).timer;
		text2.GetComponent(SteamText).timer = text1.GetComponent(SteamText).timer;
		text1.GetComponent(SteamText).timer = 15;
		box.GetComponent(SteamBox).timer = 8;
		
		timer = 0;
		element ++;
	}
	if(element >= 12 && timer > 10)
	{
		Destroy(gameObject);
	}
}