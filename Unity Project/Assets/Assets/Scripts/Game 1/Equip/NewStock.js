#pragma strict

function Start ()
{
	if(Game1SaveData.stockNew == false)
	{
		Destroy(gameObject);
	}
	
	//blink
	InvokeRepeating("Blink", 0, 0.66);
}

function Update ()
{
	Game1SaveData.stockNew = false;
}

function Blink ()
{
	if(renderer.material.color.a > 0.9)
	{
		renderer.material.color.a = 0;
	}
	else
	{
		renderer.material.color.a = 1;
	}
}

function OnMouseEnter ()
{
	Destroy(gameObject);
}