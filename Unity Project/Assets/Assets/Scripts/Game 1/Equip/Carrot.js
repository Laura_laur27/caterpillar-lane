#pragma strict

var counter: int; counter = 0;

function Start ()
{

}

function Update ()
{
	if(counter >= 4)
	{
		Invoke("Glitch", 2);
		Destroy(GameObject.Find("Exit Button"));
	}
}

function OnMouseDown()
{
	counter ++;
}

function OnMouseExit ()
{
	counter = 0;
}

function Glitch()
{
	Instantiate(Resources.Load("Camera Screen Out Pre"), Vector3(0,0,-20), transform.rotation);
	Destroy(gameObject);
}