#pragma strict

var level: int;
var quit: boolean;
var menu: boolean;

function Start ()
{

}

function Update ()
{

}

function OnMouseEnter()
{
	if(!menu)
	{
		renderer.material.color = Color(1,0.4,0,0.4);
	}
	else
	{
		renderer.material.color = Color(0,1,1,1);
	}
}

function OnMouseDown()
{
	if(!menu)
	{
		renderer.material.color = Color(1,0,0,0.5);
	}
	else
	{
		renderer.material.color = Color(1,0.66,0,1);
	}
}

function OnMouseExit()
{
	if(!menu)
	{
		renderer.material.color = Color(1,0,0,0.2);
	}
	else
	{
		renderer.material.color = Color(1,1,1,1);
	}
		
}

function OnMouseUpAsButton()
{
	if(!quit)
	{
		if(level == 1 && Game1SaveData.rounds == 1)
		{
			Application.LoadLevel(7);
		}
		else
		{
			Application.LoadLevel(level);
		}
	}
	else
	{
		Application.Quit();
		Screen.fullScreen = false;
	}
}