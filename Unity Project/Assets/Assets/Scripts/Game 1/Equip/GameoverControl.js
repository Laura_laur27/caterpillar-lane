#pragma strict
import System.Collections.Generic;

var scriptSave: Game1SaveData; scriptSave = GameObject.Find("Game 1 Save").GetComponent(Game1SaveData);
var popList : List.<int>; popList = new List.<int>();

var textGold: GameObject; textGold = GameObject.Find("Text Gold");
var goldOld: int;
var textGoldTotal: GameObject; textGoldTotal = GameObject.Find("Text Gold Total");
var textDistance: GameObject; textDistance = GameObject.Find("Text Distance");
var distanceOld: int;
var textDistanceTotal: GameObject; textDistanceTotal = GameObject.Find("Text Distance Total");
var textInvaded: GameObject; textInvaded = GameObject.Find("Text Invaded");

//for generating new stock
var costList = new int[5]; costList = [500, 750, 1250, 2000, 4000];
var item1: StoreItem;
var item2: StoreItem;
var item3: StoreItem;
var item4: StoreItem;

var timer: float; timer = 1.5;
var ticker: float; ticker = 0;

var stockCheck: boolean; stockCheck = false;
var newStock: boolean; newStock = false;

function Start ()
{
	//does poplist
	popList = scriptSave.popList;
	
	//changes values
	goldOld = Game1SaveData.totalGold;
	distanceOld = Game1SaveData.totalDistance;
	Game1SaveData.totalGold += Game1SaveData.lastGold;
	Game1SaveData.totalDistance += Game1SaveData.lastDistance;
	
	//changes colors
	textGold.GetComponent(TextMesh).renderer.material.color = Color(1,0.9,0.5);
	textGoldTotal.GetComponent(TextMesh).renderer.material.color = Color(1,0.5,0);
	textDistance.GetComponent(TextMesh).renderer.material.color = Color(1,0.9,0.5);
	textDistanceTotal.GetComponent(TextMesh).renderer.material.color = Color(1,0.5,0);
	textInvaded.GetComponent(TextMesh).renderer.material.color = Color(1,0.9,0.5);
}

function Update ()
{
	var speed: float; speed = 1.5;
	//changes text of UI
	textGold.GetComponent(TextMesh).text = Game1SaveData.lastGold.ToString();
	textGoldTotal.GetComponent(TextMesh).text = goldOld.ToString();
	textDistance.GetComponent(TextMesh).text = Game1SaveData.lastDistance.ToString("F0");
	textDistanceTotal.GetComponent(TextMesh).text = distanceOld.ToString("F0");
	textInvaded.GetComponent(TextMesh).text = Game1SaveData.housesInvaded.ToString();

	//after a while, adds gold and distance
	if(timer>0)
	{
		timer -= Time.deltaTime;
	}
	else
	{
		Game1SaveData.lastGold = Mathf.Lerp(Game1SaveData.lastGold, 0, ticker/speed);
		goldOld = Mathf.Lerp(goldOld, Game1SaveData.totalGold, ticker/speed);
		Game1SaveData.lastDistance = Mathf.Lerp(Game1SaveData.lastDistance, 0, ticker/speed);
		distanceOld = Mathf.Lerp(distanceOld, Game1SaveData.totalDistance, ticker/speed);
		ticker += Time.deltaTime;
	}
	
	if(timer<=0 && Game1SaveData.totalDistance >= Game1SaveData.nextDistance)
	{
		NewStock();
	}
	if(Input.GetKeyDown("m"))
	{
		NewStock();
	}
}

function NewStock ()
{
	if(!stockCheck)
	{
		//popup newstock!
		var pop: GameObject;
		pop = Instantiate(Resources.Load("Text Popup"), Vector3(0,3.22,-7), transform.rotation);
		pop.GetComponent(TextMesh).renderer.material.color = Color(1,1,0.5);
		pop.GetComponent(TextMesh).text = "NEW STOCK!";
		
		//changes nextdistance
		Game1SaveData.nextDistance += 500 + Game1SaveData.stockIteration*100;
		Game1SaveData.stockIteration ++;
		Game1SaveData.stockNew = true;
		
		//generate new stock
		scriptSave.itemList.Clear();
		
		var i: int;
		//active 1
		var upgradeNo: int;
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(0, 6);
			if(scriptSave.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		var level: int; level = scriptSave.upgradesList[upgradeNo].level;
		var cost: int; cost = 2*costList[scriptSave.upgradesList[upgradeNo].level];
		item1.num = 1;
		item1.upgrade = upgradeNo;
		item1.level = level + 1;
		item1.bought = false;
		item1.cost = cost;
		scriptSave.itemList.Add(item1);
		
		//item 2
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(0, 6);
			if(scriptSave.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		level = scriptSave.upgradesList[upgradeNo].level;
		cost = 2*costList[scriptSave.upgradesList[upgradeNo].level];
		item2.num = 2;
		item2.upgrade = upgradeNo;
		item2.level = level + 1;
		item2.bought = false;
		item2.cost = cost;
		scriptSave.itemList.Add(item2);
		
		//item 3
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(6, 18);
			if(scriptSave.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		level = scriptSave.upgradesList[upgradeNo].level;
		cost = costList[scriptSave.upgradesList[upgradeNo].level];
		item3.num = 3;
		item3.upgrade = upgradeNo;
		item3.level = level + 1;
		item3.bought = false;
		item3.cost = cost;
		scriptSave.itemList.Add(item3);
		
		//item 4
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(6, 18);
			if(scriptSave.upgradesList[upgradeNo].level != 5 && upgradeNo != scriptSave.itemList[2].upgrade)
			{
				break;
			}
		}
		level = scriptSave.upgradesList[upgradeNo].level;
		cost = costList[scriptSave.upgradesList[upgradeNo].level];
		item4.num = 4;
		item4.upgrade = upgradeNo;
		item4.level = level + 1;
		item4.bought = false;
		item4.cost = cost;
		scriptSave.itemList.Add(item4);
		
		stockCheck = true;
	}
}