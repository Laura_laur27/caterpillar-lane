#pragma strict

var timer: float; timer = 0;

function Start ()
{
	guiText.pixelOffset.x = 15;
	guiText.material.color = Color(1,0.9,0.75);
}

function Update ()
{
	if(timer > 0)
	{
		timer -= Time.deltaTime;
	}
	if(timer <= 0)
	{
		guiText.material.color.a = 0;
	}
	else
	{
		guiText.material.color.a = timer/2;
	}
}