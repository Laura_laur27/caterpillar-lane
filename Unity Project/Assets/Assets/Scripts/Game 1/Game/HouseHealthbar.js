#pragma strict

//controls color & size of healthbar

var health: float;
var maxHealth: float;
var color1: Material; color1 = Resources.Load("healthbar1");
var color2: Material; color2 = Resources.Load("healthbar2");
var color3: Material; color3 = Resources.Load("healthbar3");
var color4: Material; color4 = Resources.Load("healthbar4");


function Start ()
{
	maxHealth = transform.parent.GetComponent(House).health;
	if(transform.parent.GetComponent(House).titan)
	{
		Destroy(gameObject);
	}
}

function Update ()
{
	if(!transform.parent.GetComponent(House).titan)
	{
		//health = health
		health = transform.parent.GetComponent(House).health;
		var adjHealth = health;
		while(adjHealth>100)
		{
			adjHealth -= 100;
		}
		
		//changes transform
		var percentile = adjHealth/100;
		if(percentile<0)
		{
			percentile = 0;
		}
		transform.localScale.x = 1.25*percentile;
		transform.localPosition.x = 0.625*percentile - 0.625;
		
		//changes color
		if(health<=100)
		{
			renderer.material = color1;
		}
		if(health<=200 && health>100)
		{
			renderer.material = color2;
		}
		if(health<=300 && health>200)
		{
			renderer.material = color3;
		}
		if(health>300)
		{
			renderer.material = color4;
		}
	}
}