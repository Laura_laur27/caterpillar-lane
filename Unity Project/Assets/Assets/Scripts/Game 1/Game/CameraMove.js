#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

var BGM: GameObject; BGM = GameObject.Find("BGM");

var song: int;

var started: boolean; started = false;

//special spawns
var hudL: GameObject;
var hudR: GameObject;

var musicBox: GameObject; musicBox = GameObject.Find("Music Box");

var powerup: GameObject;
var titan: GameObject;
var house: GameObject;

//controls camera movement
static var camVelocity: float;
var timer: float; timer = 0;
var count: int; count = 3;

var playing: boolean;

function Start ()
{
	powerup.active = false;
	titan.active = false;
	musicBox.transform.position.y = 18.5;
}

function Update ()
{
	//Slowly moves the camera upwards
	transform.position.y += camVelocity*Time.deltaTime;
	
	//checks if house has been invaded
	if(Game1Var.currGold > 0 && !started)
	{
		started = true;
		timer = -0.85;
	 	InvokeRepeating("Count", 1.85, 1);
	 	Invoke("Sound", 1.7);
		powerup.active = true;
		titan.active = true;
		house.GetComponent(HouseGenerator).enabled = true;
		hudL.active = false;
		hudR.active = false;
		song = Random.Range(0,3);
		Game1Var.cameraFX.Burst(3.5);
		Game1Var.soundApplause.audio.Play();
		Game1Var.soundCheer.audio.Play();
	}
	
	//adds to timer
	if(started)
	{
		timer += Time.deltaTime;
	}
	else
	{
		GameObject.Find("Camera GUI").GetComponent(ColorCorrectionCurves).saturation = 0.4;
	}
	
	if(timer>0.64 && timer<0.8)
	{
		//song = Random.Range(0,3);
	}
	
	//slowly increase velocity over time
	if(timer<4)
	{
		camVelocity = 0;
	}
	else if(timer>=4 && timer<40)
	{
		camVelocity = 0.025*timer + 1.2;
	}	
	else
	{
		camVelocity = 0.0125*timer + 1.2;
	}
	
	//when sound is on, begin
	if(playing && timer>=4 && timer<11)
	{
		musicBox.transform.position.y = Mathf.Lerp(musicBox.transform.position.y, transform.position.y + 9.6, 0.02);
	}
	else if(playing && timer>=11)
	{
		musicBox.transform.position.y = Mathf.Lerp(musicBox.transform.position.y, transform.position.y + 18.5, 0.01);
	}
	if(timer >= 15)
	{
		musicBox.active = false;
	}
	
	//hour of destiny
	if(upgradesList[11].equipped && Game1Var.totalPop < 150)
	{
		camVelocity *= (0.0006*upgradesList[11].level + 0.0062)*Game1Var.totalPop/1.2 -0.06*upgradesList[11].level + 0.3;
	}
	//BGM.audio.volume = 0;
}

function Count ()
{
	if(count>=0)
	{
		var popUp: GameObject;
		popUp = Instantiate(Resources.Load("Text Popup"), Camera.main.transform.position + Vector3(0, 10, 1), transform.rotation);
		popUp.GetComponent(TextMesh).text = count.ToString();
		popUp.renderer.material.color = Color(1, 0.5, 0.8);
		popUp.transform.localScale.x *= 1.3;
		//Game1Var.cameraFX.Burst(0.5);
		count --;
	}
}

function Sound ()
{
	audio.Play();
	Invoke("Sound2", 4);
}

function Sound2 ()
{
	playing = true;
	if(song == 0)
	{
		BGM.audio.clip = Resources.Load("SleepC");
		GameObject.Find("Text Music").GetComponent(TextMesh).text = "Sleeping Clouds";
	}
	if(song == 1)
	{
		BGM.audio.clip = Resources.Load("ExtremeL");
		GameObject.Find("Text Music").GetComponent(TextMesh).text = "Extremely Loud\nwith Nothing to Say";
	}
	if(song == 2)
	{
		BGM.audio.clip = Resources.Load("Dear May");
		GameObject.Find("Text Music").GetComponent(TextMesh).text = "Dear May";
	}
	BGM.audio.Play();
}