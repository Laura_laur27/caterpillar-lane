#pragma strict

var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject;
var objectSelect = new List.<GameObject>();

function Start ()
{
	Screen.showCursor = false;
}

function Update ()
{
	//sets cursor to mouse position
	var pos: Vector3;
	var ray: Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	var hit: RaycastHit;
	if (Physics.Raycast(ray, hit))
	{
		pos = hit.point;
		pos.z = 0;
	}
	rigidbody.MovePosition(pos);
}

function OnTriggerStay (collider: Collider)
{
	Debug.Log("YO");
	if(collider.tag == "House")
	{
		varScript.objectHover = collider.gameObject;
		Debug.Log("YO2");
	}
}

function OnTriggerExit (collider: Collider)
{
	if(collider.tag == "House")
	{
		varScript.objectHover = null;
	}
}