#pragma strict

var textPref: GameObject; textPref = Resources.Load("Text Popup");
var texR: Texture2D;
var texG: Texture2D;
var texB: Texture2D;

function Start ()
{

}

function Update ()
{

}

function Powerup (type: int)
{
	if(type == 1)
	{
		
		GameObject.Find("Main Camera/Camera GUI").GetComponent(ScreenOverlay).texture = texR;
	}
	if(type == 2)
	{
		GameObject.Find("Main Camera/Camera GUI").GetComponent(ScreenOverlay).texture = texB;
	}
	if(type == 3)
	{
		GameObject.Find("Main Camera/Camera GUI").GetComponent(ScreenOverlay).texture = texG;
	}
	Game1Var.cameraFX.Burst(3.5);
	Game1Var.soundApplause.audio.Play();
	Game1Var.soundCheer.audio.Play();
}