#pragma strict

var textPref: GameObject; textPref = Resources.Load("Text Popup");
var glow: GameObject; glow = GameObject.Find("Main Camera/Camera GUI/Powerup Glow");

function Start ()
{

}

function Update ()
{
	transform.position = Camera.main.transform.position;
	
	//constantly reduces alpha to 0
	glow.renderer.material.color.a -= 0.01;
}

function LevelUp (type: int)
{
	var popUp = Instantiate(textPref, Camera.main.transform.position + Vector3(0, 10, 1), transform.rotation);
	popUp.transform.localScale *= 1.5;
	if(type == 1)
	{
		popUp.GetComponent(TextMesh).text = "ATTACK+";
		glow.renderer.material.color = Color(0.5,0,0);
		popUp.renderer.material.color = Color(1, 0.7, 0.5);
	}
	if(type == 2)
	{
		popUp.GetComponent(TextMesh).text = "DEFENCE+";
		glow.renderer.material.color = Color(0,0.2,0.5);
		popUp.renderer.material.color = Color(0.4, 0.5, 1);
	}
	if(type == 3)
	{
		popUp.GetComponent(TextMesh).text = "SPEED+";
		glow.renderer.material.color = Color(0,0.5,0.2);
		popUp.renderer.material.color = Color(0.4, 1, 0.5);
	}
	glow.renderer.material.color.a = 1;
	Game1Var.cameraFX.Burst(5);
	Game1Var.soundApplause.audio.Play();
	Game1Var.soundCheer.audio.Play();
}