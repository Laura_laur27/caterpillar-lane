#pragma strict
import System.Collections.Generic;

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;
var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject; objectHover = varScript.objectHover;
var objectSelect = new List.<GameObject>(); objectSelect = varScript.objectSelect;

var lovePart: GameObject; lovePart = Resources.Load("Love Particles");
var part: GameObject;

//states
var underInvasion: boolean; underInvasion = false;
var invaded: boolean; invaded = false;

//stats
var attack: float; attack = 0;
var health: float; health = 0;
var breedRate: float; breedRate = 0;
var gold: int; gold = 0;
var unique: boolean;
var special: boolean;

//Titanstuff
var titan: boolean;
var titanInit: boolean;
var titanType: int; titanType = 0;
var titanRequire: int;
var sacArray = new int[6]; sacArray = [50, 100, 250, 500, 777, 1000];
var activated: boolean;

//special stuff
var specialPart: GameObject;
var invaderPart: GameObject;
var titanfallPart: GameObject;

//parameters
var population: int;
var populationFloat: float;
var breedingSpeed: float;

//children
var sprite: Transform;
var spriteMove: GameObject;
var textPopulation: Transform;

//internal stats & tickers
var growRate: float;

function Start ()
{
	//initialises parameters
	textPopulation = transform.Find("Text Population");
	sprite = transform.Find("Sprite");
	
	if(unique && upgradesList[9].equipped)
	{
		population *= 0.5*upgradesList[9].level + 2;
		sprite.transform.localScale.x *= 2;
		sprite.transform.localScale.z *= 2;
	}
	
	if(unique)
	{
		invaded = true;
	}
	
	if(!titan)
	{	
		part = Instantiate(lovePart, transform.position + Vector3(0, 2.5, -2), Quaternion.Euler(-90,0,0)); part.transform.parent = transform;
	}
	
	//chooses type of house on init;
	if(!unique && !titan)
	{
		var controllScript: HouseController; controllScript = GameObject.Find("House Controller").GetComponent("HouseController");
		var selectedType: HouseType; selectedType = controllScript.SetHouseType();
		attack = selectedType.attack;
		health = selectedType.health;
		breedRate = selectedType.breed;
		gold = selectedType.gold;
		sprite.renderer.material = selectedType.sprite;
		special = selectedType.special;
		if(special)
		{
			specialPart = Instantiate(selectedType.specialPart, transform.position + Vector3(0,0,-1), transform.rotation); specialPart.transform.parent = transform;
		}
	}

	//if populated, sets float as population	
	populationFloat = population;
	
	sprite.transform.localScale *= 0.85;
}

function Update ()
{
	//inits titan
	if(titan && !titanInit)
	{
		var randomNumber: int;	randomNumber = Random.Range(1,4);
		titanType = randomNumber;
		
		//sets breedrate to 1
		breedRate = 1;
		
		if(randomNumber == 1)
		{
			if(Game1Var.rabidityLvl >=5)
			{
				Destroy(gameObject);
			}
			//sets sprite
			spriteMove = Instantiate(Resources.Load("Titan Rabid"), transform.position + Vector3(0,0,-1.5), Quaternion.Euler(90,180,0)); spriteMove.transform.parent = transform;
			part = Instantiate(Resources.Load("PartRabid"), transform.position + Vector3(0,-1,0), Quaternion.Euler(90,0,0)); part.transform.parent = transform;
			titanRequire = sacArray[Game1Var.rabidityLvl];
		}
		if(randomNumber == 2)
		{
			if(Game1Var.virilityLvl >=5)
			{
				Destroy(gameObject);
			}
			//sets sprite
			spriteMove = Instantiate(Resources.Load("Titan Viril"), transform.position + Vector3(0,0,-1.5), Quaternion.Euler(90,180,0)); spriteMove.transform.parent = transform;
			part = Instantiate(Resources.Load("PartViril"), transform.position + Vector3(0,-1,0), Quaternion.Euler(90,0,0)); part.transform.parent = transform;
			titanRequire = sacArray[Game1Var.virilityLvl];
		}
		if(randomNumber == 3)
		{
			if(Game1Var.rushLvl >=5)
			{
				Destroy(gameObject);
			}
			//sets sprite
			spriteMove = Instantiate(Resources.Load("Titan Rush"), transform.position + Vector3(0,0,-1.5), Quaternion.Euler(90,180,0)); spriteMove.transform.parent = transform;
			part = Instantiate(Resources.Load("PartRush"), transform.position + Vector3(0,-1,0), Quaternion.Euler(90,0,0)); part.transform.parent = transform;
			titanRequire = sacArray[Game1Var.rushLvl];
		}
		titanInit = true;
	}
	
	//converts float to int
	population = Mathf.Round(populationFloat);
	
	if(!titan)
	{
		//changes brightness of house and love particles based on population
		var spriteAmount: float;
		if(population>0)
		{
			part.particleSystem.emissionRate = 0.0064*population + 1.75;
			spriteAmount = 0.01*population;// + 0.5;
			sprite.renderer.material.color = Color.Lerp(Color(0.6,0.4,0.3), Color(1.0,0.5,0.5), spriteAmount);
		}
		else if(population<0)
		{
			part.particleSystem.emissionRate = 0;
			spriteAmount = 0.1*population;// + 0.5;
			sprite.renderer.material.color = Color.Lerp(Color(0.3,0.7,0.2), Color(0.2,1,0.1), spriteAmount);
		}
		else
		{
			part.particleSystem.emissionRate = 0;
			sprite.renderer.material.color = Color(0.25,0.25,0.25);
		}
		//changes size of hearts based on pop
		part.particleSystem.startSize = 0.002*population + 1.02;
		
		//grows rabbits when occupied by 2 or more rabbits
		if(population>1 && Game1Var.totalPop < 1000)
		{
			Grow();
		}
		
		//when in negatives (occupied by enemies
		if(population <= -2)
		{
			populationFloat -= 4*Time.deltaTime;
		}
			
		//changes gui text to show current population
		if(population>=0)
		{
			textPopulation.GetComponent(TextMesh).text = population.ToString();
			textPopulation.GetComponent(TextMesh).fontSize = 100;
			textPopulation.GetComponent(TextMesh).renderer.material.color = Color.white;
		}
		else
		{
			textPopulation.GetComponent(TextMesh).text = (-population).ToString();
			textPopulation.GetComponent(TextMesh).fontSize = 80;
			textPopulation.GetComponent(TextMesh).renderer.material.color = Color(0.5, 1.0, 0.1);
		}
		
		//notifies game when finally invaded
		if (!invaded && health<=0 && population >= 0 &&!unique &&!special)
		{
			InvasionComplete ();
		}
		if (!invaded && health<=0 && population >= 1 &&!unique &&special)
		{
			InvasionComplete ();
		}
			
		//deletes population when far enough
		if (transform.position.y - Camera.main.transform.position.y <= -17.5)
		{
			populationFloat = 0;
		}
		/*
		//shakes house
		if (population>=1)
		{
			shakeHouse (1.0);
		}
		else
		{
			sprite.transform.eulerAngles = Vector3(90, 180, 0);
		}*/
	}
	else
	{
		//changes brightness of house and love particles based on population
		var spriteAmount2: float;
		if(!activated)
		{
			part.active = false;
			spriteAmount2 = population/titanRequire;
			spriteMove.renderer.material.color = Color.Lerp(Color(0.3,0.3,0.3), Color(0.5,0.5,0.5), spriteAmount2);
		}
		else
		{
			part.active = true;
			spriteMove.renderer.material.color = Color(0.6,0.6,0.6);
		}
	
		//changes gui text to show current population out of required population
		textPopulation.GetComponent(TextMesh).text = population.ToString() + "/" + titanRequire.ToString();
		
		//notifies game when finally invaded
		if (!invaded && population>=1)
		{
			invaded = true;
		}
		
		//notifies when activated
		if (!activated && population>=titanRequire)
		{
			Activated ();
		}
		
		//deletes population when far enough
		if (transform.position.y - Camera.main.transform.position.y <= -17.5)
		{
			populationFloat = 0;
		}
	
		//slowly moves the token upwards based on camera speed
		if(!activated)
		{
			transform.position.y += CameraMove.camVelocity*0.3*Time.deltaTime;
		}
		else
		{
			transform.position.y += CameraMove.camVelocity*0.6*Time.deltaTime;
		}

		//titanfall	
		if(upgradesList[7].equipped && population > 0)
		{
			var damage: float; damage = 3.75*upgradesList[7].level + 5.25; //determines damage of titans based on level
			for(object in Physics.OverlapSphere(transform.position, 5))
			{
				if(object.collider.gameObject.tag == "House")
				{
					var script: House; script = object.collider.gameObject.GetComponent("House");
					script.health -= damage*Time.deltaTime;
				}
			}
			if(population>1 && Game1Var.totalPop < 1000)
			{
				Grow();
			}
			transform.position.y += CameraMove.camVelocity*(0.2)*Time.deltaTime;
						
			if(!titanfallPart.active && activated)
			{
				titanfallPart.active = true;
			}
		}
		else
		{
			if(titanfallPart.active)
			{
				titanfallPart.active = false;
			}
		}
	}
}

function FixedUpdate ()
{
	if(!titan && population>=1)
	{
		shakeHouse (1.0);
	}
	else
	{
		sprite.transform.eulerAngles = Vector3(90, 180, 0);
	}
}

function Grow ()
{
	//grows rabbits based on base growth rate, number of rabbits in house, and global growth rate
	growRate = Game1Stats.modBreedRate * breedRate * (0.02 * populationFloat + 0.7); //number of rabbits to create/sec
	//counts down ticker, adds 1/4th of rabbits when hitting 0
	populationFloat += (growRate*Time.deltaTime);
	breedingSpeed = growRate;
}

function InvasionComplete ()
{
	NotificationCenter.DefaultCenter().PostNotification(this, "HouseInvaded");
	Game1Var.moneyControl.GainGold(gameObject, gold);
	Game1Var.housesInvaded += 1;
	//if invader equipped, gain gauge
	if(upgradesList[6].equipped)
	{
		NotificationCenter.DefaultCenter().PostNotification(this, "GainGauge", (0.625*upgradesList[6].level + 1.875));
		invaderPart.active = true;
	}
	invaded = true;
}

function Activated ()
{
	activated = true;
	if(titanType == 1)
	{
		Game1Var.rabidityLvl += 1;
		Game1Var.levelupControl.LevelUp(1);
	}
	if(titanType == 2)
	{
		Game1Var.virilityLvl += 1;
		Game1Var.levelupControl.LevelUp(2);
	}
	if(titanType == 3)
	{
		Game1Var.rushLvl += 1;
		Game1Var.levelupControl.LevelUp(3);
	}
}

function shakeHouse (amount: float)
{
	var speed = 25.0f; //how fast it shakes
	var rotate: float;
	rotate = amount*Mathf.Sin(Time.time * speed);
	sprite.transform.Rotate(Vector3(0, rotate, 0));
}