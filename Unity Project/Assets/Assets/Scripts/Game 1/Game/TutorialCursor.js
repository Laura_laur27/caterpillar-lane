#pragma strict

var scale: float;

function Start ()
{
	scale = 1.2;
	InvokeRepeating("ReverseScale", 0, 0.33);
}

function Update ()
{
	transform.localScale.x = Mathf.Lerp(transform.localScale.x, scale, 0.04);
	transform.localScale.y = Mathf.Lerp(transform.localScale.y, scale, 0.04);
}

function ReverseScale ()
{
	if(scale <= 1.3)
	{
		scale = 1.6;
	}
	else if(scale >= 1.3)
	{
		scale = 1.2;
	}
}