#pragma strict

var record: boolean;

var part: GameObject;

var textPref: GameObject; textPref = Resources.Load("Text Popup");
var glow: GameObject; glow = GameObject.Find("Main Camera/Camera GUI/Powerup Glow");
var trigger: boolean;

function Start ()
{
	Game1SaveData.breached = false;
}

function Update ()
{
	Debug.Log(Game1SaveData.record);
	Debug.Log(Game1SaveData.nextDistance);
	
	if(record && Game1SaveData.record > 10)
	{
		transform.parent.transform.position.y = Game1SaveData.record;
	}
	else if(record && Game1SaveData.record <= 10)
	{
		Destroy(transform.parent.gameObject);
	}
	else
	{
		transform.parent.transform.position.y = Game1SaveData.nextDistance;
	}
}

function OnTriggerEnter (collider: Collider)
{
	if(collider.tag == "Rabbit" && !trigger)
	{
		Game1Var.cameraFX.Burst(5.0);
		Game1Var.soundApplause.audio.Play();
		Game1Var.soundCheer.audio.Play();
		
		var popUp = Instantiate(textPref, Camera.main.transform.position + Vector3(0, 8, 1), transform.rotation);
		popUp.transform.localScale *= 1.1;
		if(record)
		{
			popUp.GetComponent(TextMesh).text = "NEW RECORD!";
			glow.renderer.material.color = Color(0.5,0.4,0,1);
			popUp.renderer.material.color = Color(1, 1, 0.5);
		}
		else
		{
			Game1SaveData.breached = true;
			popUp.GetComponent(TextMesh).text = Game1SaveData.nextDistance + "m" + "\n" + "BREACHED!";
			glow.renderer.material.color = Color(0.8,0.3,0,1);
			popUp.renderer.material.color = Color(1, 0.3, 0.2);		
		}
		collider.enabled = false;
		trigger = true;
		
		part.particleSystem.startSpeed = 9;
		part.particleSystem.Emit(100);
		part.particleSystem.Play();
	}
}