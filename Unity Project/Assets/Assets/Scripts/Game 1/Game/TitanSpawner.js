#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

var ticker: float; ticker = 0.0;

function Start ()
{
	upgradesList = upgradeScript.upgradesList;
}

function Update ()
{
	if(Game1Var.rabidityLvl >=5 && Game1Var.virilityLvl >=5 && Game1Var.rushLvl >=5)
	{
		Destroy(gameObject);
	}

	//every ticker seconds, spawn token
	if(ticker>0)
	{
		ticker -= Time.deltaTime;
	}
	else
	{
		TitanSpawn();
		ticker = 2.5;
	}
	
	//moves with camera
	transform.position.y = Game1Var.distanceTravelled;
}

function TitanSpawn ()
{
	var specialMod: float; specialMod = 1;
	if (upgradesList[13].equipped)//rabbits foot
	{
		specialMod = 0.25*upgradesList[13].level + 1;
	}
	if(Random.value<(specialMod*0.04))
	{
		//spawns titan prefab at random position
		var spawnPosition: Vector3;
		spawnPosition.x = Random.Range(-9, 9);
		spawnPosition.y = 20;
		spawnPosition.z = 0;
		var titan = Instantiate(Game1Var.prefabHouse, transform.position + spawnPosition, transform.rotation);
		var script: House; script = titan.GetComponent("House");
		script.titan = true;
	}
}