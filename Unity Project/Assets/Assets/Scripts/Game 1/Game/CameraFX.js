#pragma strict

var shakeCounter: float;
var CameraGUI: Transform; CameraGUI = transform.Find("Camera GUI");
var CameraGUI2: Transform; CameraGUI2 = transform.Find("Camera GUI2");
var sat: float;

function Start ()
{
	shakeCounter = 0;
	sat = 0.8;
}

function Update ()
{
	//reverts bloom back to normal
	CameraGUI.GetComponent(Bloom).bloomIntensity = Mathf.Lerp(CameraGUI.GetComponent(Bloom).bloomIntensity, 2.3, 0.015);
	CameraGUI.GetComponent(Bloom).bloomThreshhold = Mathf.Lerp(CameraGUI.GetComponent(Bloom).bloomThreshhold, 0.5, 0.015);
	
	//reverts chroma to normal
	GetComponent(Vignetting).chromaticAberration = Mathf.Lerp(GetComponent(Vignetting).chromaticAberration, 0.0, 0.01);
	
	//resets saturation
	CameraGUI.GetComponent(ColorCorrectionCurves).saturation = Mathf.Lerp(CameraGUI.GetComponent(ColorCorrectionCurves).saturation, sat, 0.02);
	
	//resets vignetting
	GetComponent(Vignetting).intensity = Mathf.Lerp(GetComponent(Vignetting).intensity, 2.0, 0.003);
	
	//resets overlay
	CameraGUI.GetComponent(ScreenOverlay).intensity = Mathf.Lerp(CameraGUI.GetComponent(ScreenOverlay).intensity, 0.0, 0.01);
	
	//slowly rotates camera back to norm
	//transform.rotation.eulerAngles.y = Mathf.Lerp(transform.rotation.eulerAngles.y, 0, 0.02);
	
	//shakes camera
	Shake(shakeCounter);
	if (shakeCounter <= 0)
	{
		shakeCounter = 0;
		//transform.rotation.ToEulerAngles(Vector3(0,0,0));
		transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0,0,0), 0.05);
	}
	else
	{
		shakeCounter -= 0.002;
	}
}

function Shake(amount: float)
{
	var speed = 30.0f; //how fast it shakes
	var rotate: float;
	rotate = amount*Mathf.Sin(Time.time * speed);
	transform.Rotate(Vector3(0, 0, rotate*0.75));
}

function Burst(amount: float)
{
	//amount 1-3 intesity is high, chroma -30-60
	shakeCounter = 0.275;
	CameraGUI.GetComponent(Bloom).bloomIntensity = amount*1.2;
	CameraGUI.GetComponent(Bloom).bloomThreshhold = 0.05;
	GetComponent(Vignetting).chromaticAberration = amount*-20;
}