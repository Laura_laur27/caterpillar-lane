#pragma strict

var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject; 
var objectSelect = new List.<GameObject>(); 

var sprite: Transform; sprite = transform.Find("Sprite");
var sprite1: Material;
var sprite2: Material;
var glow: Transform; glow = transform.Find("SpriteGlow");
var glow1: Material;
var glow2: Material;

function Start ()
{

}

function Update ()
{
	//Constantly Updates Hover object
	objectHover = varScript.objectHover;
	objectSelect = varScript.objectSelect;

	//if there is an object being hovered, go to it
	if(objectHover != null && objectSelect.Count == 0 && objectHover.tag == "House" && Time.timeScale > 0.1)
	{
		var script: House; script = objectHover.GetComponent("House");
		if(script.invaded)
		{
			sprite.renderer.material = sprite1;
			glow.renderer.material = glow1;
			transform.position = objectHover.transform.position + Vector3(0,0,-4.5);
		}
	}
	else if(objectHover != null && objectSelect.Count != 0 && Time.timeScale > 0.9)
	{
		sprite.renderer.material = sprite2;
		glow.renderer.material = glow2;
		transform.position = objectHover.transform.position + Vector3(0,0,-4.5);
	}
	else
	{
		transform.position = Vector3(-100,-100,-100);
	}
	
	//constantly rotates object
	transform.Rotate(0,0,30*Time.deltaTime);
}