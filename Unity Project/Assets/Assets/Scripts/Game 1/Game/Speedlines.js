#pragma strict

var power: float;
var cam: GameObject;

function Start ()
{
	power = 0;
}

function Update ()
{
	//constantly resets speedlines and effect when not powered
	if(power <= 0)
	{
		particleSystem.emissionRate = Mathf.Lerp(particleSystem.emissionRate, 0, 0.066);
		cam.GetComponent(ScreenOverlay).intensity = Mathf.Lerp(cam.GetComponent(ScreenOverlay).intensity, 0, 0.02);
	}
	else
	{
		power -= Time.deltaTime;
	}
}

function Burst ()
{
	power = 0.5;
	particleSystem.emissionRate = 200;
	cam.GetComponent(ScreenOverlay).intensity = 1;
}

function Sustain ()
{
	power = 0.1;
	particleSystem.emissionRate = Mathf.Lerp(particleSystem.emissionRate, 100, 0.05);
	cam.GetComponent(ScreenOverlay).intensity = Mathf.Lerp(cam.GetComponent(ScreenOverlay).intensity, 0.8, 0.05);
}