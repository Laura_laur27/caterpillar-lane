#pragma strict
import System.Collections.Generic;

class HazardType
{
	var name: String;
	var slow: float;
	var damage: float;
	var sprite: Material;
	var part: GameObject;
	var co1: float;
	var co2: float;
	var co3: float;
	var rate: float;
}

//generation script
var locations : List.<Vector3>; locations = new List.<Vector3>(); //contains locations for possible spawns
var xBound = 10.0;
var yBound = 13.0;

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

var hazardList: HazardType[];
var totalRate: float;

function Start ()
{
	//generates location vectors
	var currX = -xBound;
	var currY = -yBound;
	for(var i = 0; i<9; i++)
	{
		var newVector: Vector3;
		newVector.x = currX;
		newVector.y = currY;
		newVector.z = 0;
		locations.Add(newVector);
		
		currX += xBound;
		if(currX>xBound+0.1)
		{
			currX = -xBound;
			currY += yBound;
		}
	}
}

function Update ()
{
	CalculateHazardRates();
	
	//spawns
	if(transform.position.y - Camera.main.transform.position.y < 60)
	{
		RandomSpawn();
		transform.position.y += yBound*3;
	}
}

function RandomSpawn ()
{
	var spawnNum: int; spawnNum = Random.Range(0, 2); //chooses number of hazards in this spawn area
	var chosenLocations : List.<Vector3>; chosenLocations = new List.<Vector3>();  //the chosen locations for spawning
	
	for(var i = 0; i<spawnNum; i++)
	{
		var randomLocation: Vector3;
		var ticker: boolean; ticker = false;
		randomLocation = Vector3(1,1,1);
		while(!ticker)
		{
			randomLocation = locations[Random.Range(0, 9)];
			if(!chosenLocations.Contains(randomLocation))
			{
				chosenLocations.Add(randomLocation);
				ticker = true;
			}
		}
		var spawn: GameObject; spawn = Instantiate(Game1Var.prefabHazard, randomLocation + transform.position + Vector3((Random.value-0.5)*5, (Random.value-0.5)*5, 0), Quaternion.Euler(0,0,45));
		
		//inits the hazard
		var spawnScript: Hazard; spawnScript = spawn.GetComponent("Hazard");
		var selectedType: HazardType; selectedType = SetHazardType();
		
		//spawnScript.name = selectedType.name;
		spawnScript.slow = selectedType.slow;
		spawnScript.damage = selectedType.damage;
		spawnScript.sprite = spawnScript.transform.Find("Sprite");
		spawnScript.sprite.renderer.material = selectedType.sprite;
		if(selectedType.part != null)
		{
			Instantiate(selectedType.part, spawn.transform.position, spawn.transform.rotation);
		}
	}
}

function CalculateHazardRates ()
{
	var adjDistance = Game1Var.distanceTravelled/250;
	var rateToNow: float; rateToNow = 0;
	totalRate = 0;
	
	//calculates hazard rate & sums rates for total rate
	for(var i=0; i < hazardList.length; i++)
	{
		hazardList[i].rate = (hazardList[i].co1*adjDistance*adjDistance) + (hazardList[i].co2*adjDistance) + hazardList[i].co3;
		if (hazardList[i].rate < 0)
		{
			hazardList[i].rate = 0;
		}
		rateToNow = totalRate;
		totalRate += hazardList[i].rate;
		hazardList[i].rate += rateToNow;
	}
}

function SetHazardType () : HazardType
{
	//based on distance travelled, randomly returns house type
	var chosenHazard: HazardType;
	
	var i: int; i = 0;
	var randomFloat: float; randomFloat = Random.value*totalRate - 0.001;
	while (hazardList[i].rate < randomFloat)
	{
		i++;
	}
	chosenHazard = hazardList[i];

	return chosenHazard;
}