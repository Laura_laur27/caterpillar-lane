#pragma strict
import System.Collections.Generic;

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;
var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject;
var objectSelect = new List.<GameObject>(); 

function Start ()
{
	NotificationCenter.DefaultCenter().AddObserver(this, "GainGold");
	NotificationCenter.DefaultCenter().AddObserver(this, "GainGauge");
}

function Update ()
{
	//testing purposes only
	upgradesList = upgradeScript.upgradesList;
	
	//Constantly Updates Hover object
	objectSelect = varScript.objectSelect;
	objectHover = varScript.objectHover;
	
	SetHover();
	
	//moverabbits if player drags from one house to another - exception, when they drag from own house to themselves again
	if (Input.GetMouseButtonUp(0))
	{
		//updates selected objects
		if (objectSelect.Count > 0 && (objectHover != null) && !((objectSelect.Count == 1) && objectSelect[0] == objectHover))
		{
			MoveRabbits();
		}
	}
}

function MoveRabbits ()
{
	//Vacate X number of rabbits from all selected houses house
	var timewalked: boolean; timewalked = false;
	var selectNum : int = 0;
	for (house in objectSelect)
	{
		if(house != objectHover)
		{
			//With Dejavu, give chance of spawning clones
			var dejaVu: boolean; dejaVu = false;
			if(upgradesList[12].equipped && Random.value < (0.0375*upgradesList[12].level + 0.0325))
			{
				dejaVu = true;
			}
			
			var spawnBase: Vector3;
			
			//with timewalk, teleports to destination
			if(upgradesList[14].equipped && Random.value < (0.0375*upgradesList[14].level + 0.1125))
			{
				spawnBase = objectHover.transform.position;
				if(!timewalked)
				{
					Instantiate(Resources.Load("PartTimewalk"), objectHover.transform.position, transform.rotation);
					timewalked = true;
				}
			}
			else
			{
				spawnBase = objectSelect[selectNum].transform.position;
			}
			
			var scriptHouse: House = objectSelect[selectNum].GetComponent(House);
			//generates control group size based on house population and control size mod
			var vacatedNum: int;
			vacatedNum = (scriptHouse.population/3)+(5*Game1Stats.modControlSize);
			if(scriptHouse.population<vacatedNum)
			{
				vacatedNum = scriptHouse.population;
			}
			scriptHouse.populationFloat -= vacatedNum;
						
			//Instantiate X number of rabbits at location
			for (var i : int = 0; i < vacatedNum; i++)
			{
				var spawn: Transform;
				spawn = PoolManager.Pools["RabbitPool"].Spawn(Game1Var.prefabRabbit.transform, spawnBase + Vector3((Random.value-0.5)*3, (Random.value-0.5)*3, 0), Quaternion.identity);
				//Moves each rabbit to new location
				spawn.SendMessage("SetTarget", objectHover);
				
				if(dejaVu)//if dejavu was triggered
				{
					StartCoroutine(DejaVu(spawnBase, objectHover));
				}
			}
		}
		selectNum++;
	}
}

function DejaVu (origin: Vector3, target: GameObject)
{
	yield WaitForSeconds(1.0);
			
	var dspawn: Transform;
	dspawn = PoolManager.Pools["RabbitPool"].Spawn(Game1Var.prefabRabbit.transform, origin + Vector3((Random.value-0.5)*3, (Random.value-0.5)*3, 0), Quaternion.identity);
	//Moves each rabbit to new location
	dspawn.SendMessage("SetTarget", target);
}

function SetHover ()
{
	var houses : GameObject[];
	var closestHouse: GameObject;
	var closestDistance: float; closestDistance = 100;
	var pos: Vector3;
	var ray: Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	var hit: RaycastHit;
	if (Physics.Raycast(ray, hit))
	{
		pos = hit.point;
	}
	houses = GameObject.FindGameObjectsWithTag("House");
	for (singlehouse in houses)
	{
		var distance = (singlehouse.transform.position.x-pos.x)*(singlehouse.transform.position.x-pos.x)+(singlehouse.transform.position.y-pos.y)*(singlehouse.transform.position.y-pos.y);
		if(distance<closestDistance)
		{
			closestDistance = distance;
			closestHouse = singlehouse;
		}
	}
	if(closestDistance > 20)
	{
		varScript.objectHover = null;
	}
	else
	{
		varScript.objectHover = closestHouse;
	}
}

function GainGauge (note: Notification)
{
	var gaugeGain: float; gaugeGain = note.data;
	gaugeGain *= Game1Stats.modGaugeRate;
	Game1Var.gauge += gaugeGain;
}

function GainGold (note: Notification)
{
	var goldGain: int; goldGain = note.data;
	goldGain *= Game1Stats.modGoldRate;
	Game1Var.currGold += goldGain;
}