#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

//type: 1=gold, 2=gauge, 3=berserk, 4=invul, 5=haste
var powerupType: int;
var powerupSprite: Material;
var ticker: float; ticker = 7.5;

//sprites
var sprite1: Material;
var sprite2: Material;
var sprite3: Material;
var sprite4: Material;
var sprite5: Material;

function Start ()
{
	//Notifications
	NotificationCenter.DefaultCenter().AddObserver(this, "NewGameStart");
}

function Update ()
{
	//every second, check to see if powerup spawns
	if(ticker>0)
	{
		ticker -= Time.deltaTime;
	}
	else
	{
		CheckSpawn();
		ticker = 1.3f;
	}
	
	//moves with camera
	transform.position.y = Game1Var.distanceTravelled;
}

function CheckSpawn ()
{
	//checks to see if token spawns, if yes then init and send
	var chosenToken: float; chosenToken = Random.value;
	var specialMod: float; specialMod = 1.0; //for last three
	var luckyMod: float; luckyMod = 2.0;
	if (upgradesList[13].equipped)//rabbits foot
	{
		luckyMod = 0.25*upgradesList[13].level + 1;
	}
	if(chosenToken<0.2)
	{
		powerupType = 1;
		powerupSprite = sprite1;
		specialMod = 0.9;
	}
	else if(chosenToken<0.7)
	{
		powerupType = 2;
		powerupSprite = sprite2;
		specialMod = 0.8;
	}
	else if(chosenToken<0.8)
	{
		powerupType = 3;
		powerupSprite = sprite3;
		specialMod = 0.5;
	}	
	else if(chosenToken<0.9)
	{
		powerupType = 4;
		powerupSprite = sprite4;
		specialMod = 0.5;
	}
	else
	{
		powerupType = 5;
		powerupSprite = sprite5;
		specialMod = 0.5;
	}
	var chance: float;
	chance = specialMod*luckyMod*0.055;
	
	if(Random.value<chance)
	{
		var power: GameObject;
		var spawnDirection: int;
		var spawnPosition: Vector2; // position to be spawned;
		var direction: Vector3; //direction for powerup to move;
		
		//choose direction of spawn (left right)
		spawnDirection = Random.Range(1,3);

		if(spawnDirection == 1)//left
		{
			spawnPosition.x = -15;
			spawnPosition.y = Random.Range(-7, 20);
			direction = Vector3(1, 0, 0);
		}
		if(spawnDirection == 2)//right
		{
			spawnPosition.x = 15;
			spawnPosition.y = Random.Range(-7, 20);
			direction = Vector3(-1, 0, 0);
		}
		
		power = Instantiate(Game1Var.prefabPowerup, transform.position + spawnPosition, transform.rotation);
		var powerScript: Powerup;
		powerScript = power.GetComponent("Powerup");
		powerScript.powerupType = powerupType;
		//gets and sets sprite
		powerScript.sprite = power.transform.Find("Sprite");
		powerScript.sprite.renderer.material = powerupSprite;
		powerScript.travelDir = direction;
	}
}