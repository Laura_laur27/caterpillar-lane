#pragma strict

var check:boolean;
var timer = 1.0;
var newColor = new Color[6]; newColor = [Color.red, Color.blue, Color.yellow, Color.magenta];
var chosenColor: Color; chosenColor = Color.magenta;
var overlay: boolean;
var trees: boolean;

function Start () {

}

function Update ()
{
	if(overlay)
	{
		if(timer>0)
		{
			timer -= Time.deltaTime;
		}
		else
		{
			chosenColor = newColor[Random.Range(0,4)];
			timer = 0.5;
		}
		renderer.material.color = Color.Lerp(renderer.material.color, chosenColor, 0.05);
		renderer.material.color.a = 0.2;
	}
	else if(trees)
	{
		renderer.material.mainTextureOffset.y = Game1Var.distanceTravelled*-0.042;
	}
	else
	{
		renderer.material.mainTextureOffset.y = Game1Var.distanceTravelled*-0.018;
	}
	transform.position.y = Game1Var.distanceTravelled;
}