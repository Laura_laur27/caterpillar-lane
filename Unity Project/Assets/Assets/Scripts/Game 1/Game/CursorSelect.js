#pragma strict

var num: int;

var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject; 
var objectSelect = new List.<GameObject>();

function Start ()
{

}

function Update ()
{
	//Constantly Updates Selected object
	objectHover = varScript.objectHover;
	objectSelect = varScript.objectSelect;

	//if there is an object being hovered, go to it
	if(objectSelect.Count >= num && Time.timeScale > 0.1)
	{
		transform.position = objectSelect[num-1].transform.position + Vector3(0,0,-4);
	}
	else
	{
		transform.position = Vector3(-100,-100,-100);
	}
	
	//constantly rotates object
	transform.Rotate(0,0,30*Time.deltaTime);
}