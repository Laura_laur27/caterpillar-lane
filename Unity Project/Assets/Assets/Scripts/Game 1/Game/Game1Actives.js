#pragma strict
import System.Collections.Generic;

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;
var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject;
var objectSelected: GameObject;
var objectSelect = new List.<GameObject>();
var selected: float;

//active icon
var display: GameObject; display = GameObject.Find("Main Camera/Active Display");
var displayGlow: GameObject; displayGlow = GameObject.Find("Main Camera/Active Display Glow");
var canUse: boolean; canUse = true;

//states
var ticker: float; ticker = 0;

//for rewind
var timerRewind: float;
var targetY: float;

//for infest
var spriteNest1: Material;
var spriteNest2: Material;
var origin: GameObject;

var effect: GameObject;

function Start ()
{
	NotificationCenter.DefaultCenter().AddObserver(this, "NewGameStart");
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	upgradesList = upgradeScript.upgradesList;
	
	//changes icon
	if(upgradeScript.activeEquip.Count == 0)
	{
		display.renderer.material.color.a = 0;
	}
	display.renderer.material.mainTexture = upgradeScript.activeEquip[0].icon;
	displayGlow.renderer.material.color.a = 0;
}

function Update ()
{
	//testing purposes only
	
	upgradesList = upgradeScript.upgradesList;

	//Constantly Updates Hover object
	objectHover = varScript.objectHover;
	objectSelect = varScript.objectSelect;
	
	//if not using a skill, refreshed selected object
	if(selected<=0)
	{
		objectSelected = objectHover;
	}
	else
	{
		selected -= Time.deltaTime;
	}
	
	//Active skill use
	if(upgradeScript.activeEquip.Count != 0)
	{
		ActiveSkills();
	}
	
	//changes icon based on usability
	if(canUse)
	{
		display.renderer.material.color = Color.Lerp(display.renderer.material.color, Color(1,0.7,0.4,1), 0.1);
	}
	else
	{
		display.renderer.material.color = Color.Lerp(display.renderer.material.color, Color(0.5,0.4,0.33,0.5), 0.1);
	}
	displayGlow.renderer.material.color.a = Mathf.Lerp(displayGlow.renderer.material.color.a, 0, 0.02);
}

function FixedUpdate ()
{
	//Timewarp
	if(upgradeScript.activeEquip.Count > 0 && upgradesList[4] == upgradeScript.activeEquip[0])
	{
		Timewarp ();
	}
}

function ActiveSkills ()
{
	//Mercenary
	if(upgradesList[0] == upgradeScript.activeEquip[0])
	{
		Mercenary();
		if(Input.GetMouseButtonUp(1))
		{
			varScript.UnSelect();
		}
	}
	//RabbitGeddon
	if(upgradesList[1] == upgradeScript.activeEquip[0])
	{
		Rabbitgeddon();
	}
	//The Heat is on
	if(upgradesList[2] == upgradeScript.activeEquip[0])
	{
		TheHeatIsOn ();
	}
	//Infestation
	if(upgradesList[3] == upgradeScript.activeEquip[0])
	{
		Infestation ();
	}
	//Rewind
	if(upgradesList[5] == upgradeScript.activeEquip[0])
	{
		Rewind ();
	}
}

function Mercenary ()
{
	var cost: float; //cost of gauge/sec
	cost = 33.0;
	var rate: float; //rate of rabbits/sec
	
	if(objectHover != null && Input.GetMouseButton(1) && Game1Var.totalPop < 1000)//has target house, and button down
	{
		if(Game1Var.gauge > 0)
		{
			selected = 0.1;
			Game1Var.gauge -= cost*Time.deltaTime;
			displayGlow.renderer.material.color.a = 1;
			effect.GetComponent(Speedlines).Sustain();
			if(ticker>0)
			{
				ticker -= Time.deltaTime;
			}
			else
			{
				//spawns rabbits!
				if(Game1Var.currGold < 3000)
				{
					rate = (0.005*Game1Var.currGold + 3)*(10*upgradesList[0].level+10);
				}
				else
				{
					rate = 4*(10*upgradesList[0].level+10);
				}
				var rabbitNum: int;
				rabbitNum = Mathf.Round(rate/4);
				for (var i : int = 0; i < rabbitNum; i++)
				{
					var spawn: Transform;
					var direction: int; direction = 1;
					if(objectSelected.transform.position.x<0)
					{
						direction = -1;
					}
					spawn = PoolManager.Pools["RabbitPool"].Spawn(Game1Var.prefabRabbit.transform, objectSelected.transform.position + Vector3((Random.value-0.5)*3 + 15*direction, (Random.value-0.5)*30, 0), Quaternion.identity);
					//Moves each rabbit to new location
					if(spawn != null)
					{
						spawn.SendMessage("SetTarget", objectSelected);
					}
				}
				ticker = 0.25;
			}
		}
		else //not enough gauge :(
		{
			NotificationCenter.DefaultCenter().PostNotification(this, "NotEnoughGauge");
		}
	}
	
	if(Game1Var.gauge > 5)
	{
		canUse = true;
	}
	else
	{
		canUse = false;
	}
}

function MarchHare ()
{
	var cost: float; //cost of gauge
	var costList = new float[5]; costList = [100.0, 75, 50, 33, 25];
	cost = costList[upgradesList[1].level-1];
	if(objectHover != null && objectSelect.Count == 0 && Input.GetMouseButtonDown(1))//has target house, and button down, and no houses currently selected
	{
		if(Game1Var.gauge >= cost)
		{
			selected = 0.1;
			//cost gauge
			Game1Var.gauge -= cost;

			displayGlow.renderer.material.color.a = 1;
			effect.GetComponent(Speedlines).Sustain();
			//adds all selected houses to selection
			//creates list of all houses in game
			var houses : GameObject[];
			houses = GameObject.FindGameObjectsWithTag("House");
			for (singlehouse in houses)
			{
				//adds invaded houses to objectselect
				var script: House;
				script = singlehouse.GetComponent(House);
				if(script.invaded)
				{
					objectSelect.Add(singlehouse);
				}
			}
						
			//moves all rabbits in houses into house
			var selectNum : int = 0;
			for (house in objectSelect)
			{
				var scriptHouse: House = objectSelect[selectNum].GetComponent(House);
				//generates control group size based on house population (selects all)
				var vacatedNum: int;
				vacatedNum = scriptHouse.population;
				scriptHouse.populationFloat = 0;
				
				//Instantiate X number of rabbits at location
				for (var i : int = 0; i < vacatedNum; i++)
				{
					var spawn: Transform;
					spawn = PoolManager.Pools["RabbitPool"].Spawn(Game1Var.prefabRabbit.transform, objectSelect[selectNum].transform.position + Vector3((Random.value-0.5)*3, (Random.value-0.5)*3, 0), Quaternion.identity);
					//Moves each rabbit to new location
					if(spawn != null)
					{
						spawn.SendMessage("SetTarget", objectSelected);
					}
				}
				selectNum++;
			}
			
			//moves all external rabbits into houses
			var rabbits : GameObject[];
			rabbits = GameObject.FindGameObjectsWithTag("Rabbit");
			for (rabbit in rabbits)
			{
				var scriptRabbit: Rabbit;
				scriptRabbit = rabbit.GetComponent(Rabbit);
				scriptRabbit.target = objectSelected;
			}
			
			//clears selection
			objectSelect.Clear();
		}
		else //not enough gauge :(
		{
			NotificationCenter.DefaultCenter().PostNotification(this, "NotEnoughGauge");
		}
	}
	if(Game1Var.gauge >= cost)
	{
		canUse = true;
	}
	else
	{
		canUse = false;
	}
}

function TheHeatIsOn ()
{
	var cost: float; //cost of gauge/sec
	cost = 20.0;
	var rate: float; //rate of rabbits/sec
	
	if(Input.GetMouseButton(1))//has target house, and button down
	{
		if(Game1Var.gauge > 0)
		{
			displayGlow.renderer.material.color.a = 1;
			effect.GetComponent(Speedlines).Sustain();
			//turns on heat modifier in stats script
			Game1Stats.theHeatIsOn = true;
			
			//subtract gauge
			Game1Var.gauge -= cost*Time.deltaTime;
		}
		else //not enough gauge :(
		{
			Game1Stats.theHeatIsOn = false;
			NotificationCenter.DefaultCenter().PostNotification(this, "NotEnoughGauge");
		}
	}
	else
	{
		Game1Stats.theHeatIsOn = false;
	}
	if(Game1Var.gauge >= 5)
	{
		canUse = true;
	}
	else
	{
		canUse = false;
	}
}

function Infestation ()
{
	var cost: float; //cost of gauge
	var factor: float; factor = 0.5*upgradesList[3].level + 0.5;//power of infest, based on level;
	cost = 50;
	if(objectHover != null && objectHover.tag == "House" && objectSelect.Count == 0 && Input.GetMouseButtonDown(1) && Game1Var.totalPop < 1000)//has target house, and button down, and no houses currently selected
	{
		if(Game1Var.gauge >= cost)
		{
			selected = 0.1;
			displayGlow.renderer.material.color.a = 1;
			effect.GetComponent(Speedlines).Burst();
			var script: House; script = objectSelected.GetComponent("House");
			//Creats nest in place
			//changes health to 0
			script.health = 0;
			//adds x number of rabbits to house
			script.populationFloat += factor*100*(1 + (Game1Stats.modBreedRate-1)*0.2)*(1 + (Game1Stats.modStartRate-1)*0.2);
			//changes sprite into nest sprite
			script.sprite.renderer.material = spriteNest1;
			
			//Side blast after 0.6 secs
			origin = objectSelected;
			
			StartCoroutine(InfestationBlast (origin));
				
			//cost gauge
			Game1Var.gauge -= cost;
		}
		else //not enough gauge :(
		{
			NotificationCenter.DefaultCenter().PostNotification(this, "NotEnoughGauge");
		}
	}
	if(Game1Var.gauge >= cost)
	{
		canUse = true;
	}
	else
	{
		canUse = false;
	}
}

function InfestationBlast (house: GameObject)
{
	var factor: float; factor = 0.25*upgradesList[3].level + 0.75;//power of infest, based on level;
	var radius: float; radius = 1*upgradesList[3].level + 3;//determines radius of explosion based on level
	
	yield WaitForSeconds(0.5);
	
	for(object in Physics.OverlapSphere(house.transform.position, radius))
	{
		if(object.collider.gameObject.tag == "House" && object.collider.gameObject != house)
		{
			var script: House; script = object.collider.gameObject.GetComponent("House");

			//changes health to 0
			script.health = 0;
			//adds x number of rabbits to house
			script.populationFloat += factor*25*(1 + (Game1Stats.modBreedRate-1)*0.2)*(1 + (Game1Stats.modStartRate-1)*0.2);
			//changes sprite into nest sprite
			script.sprite.renderer.material = spriteNest2;
		}
	}
}

function Timewarp ()
{
	var cost: float; //cost of gauge
	var costList = new float[5]; costList = [30.0, 25, 20, 15, 10];
	cost = costList[upgradesList[4].level-1];
	if(Input.GetMouseButton(1) && Time.timeScale != 0)//button down
	{
		if(Game1Var.gauge > 0)
		{
			displayGlow.renderer.material.color.a = 1;
			effect.GetComponent(Speedlines).Sustain();
			//slowmotion on
			Time.timeScale = 0.2;//Mathf.Lerp(Time.timeScale, 0.1, 0.4);
			Time.fixedDeltaTime = 0.02*Time.timeScale;
			GameObject.Find("BGM").gameObject.audio.pitch = 0.75;
			//subtract gauge
			Game1Var.gauge -= cost*1.5*Time.deltaTime;
		}
		else //not enough gauge :(
		{
			Time.timeScale = Mathf.Lerp(Time.timeScale, 1.0, 0.15);
			Time.fixedDeltaTime = 0.02;
			GameObject.Find("BGM").gameObject.audio.pitch = 1;
			NotificationCenter.DefaultCenter().PostNotification(this, "NotEnoughGauge");
		}
	}
	else
	{
		Time.timeScale = Mathf.Lerp(Time.timeScale, 1.0, 0.15);
		Time.fixedDeltaTime = 0.02;
		GameObject.Find("BGM").gameObject.audio.pitch = 1;
	}
	if(Game1Var.gauge >= 5)
	{
		canUse = true;
	}
	else
	{
		canUse = false;
	}
}

function Rewind ()
{
	var cost: float; //cost of gauge
	var costList = new float[5]; costList = [100.0, 80, 60, 40, 20];

	cost = costList[upgradesList[5].level-1];
	if(Input.GetMouseButtonDown(1))//when mouse down
	{
		if(Game1Var.gauge >= cost)
		{
			displayGlow.renderer.material.color.a = 1;
			effect.GetComponent(Speedlines).Burst();
			timerRewind = 1.0;
			targetY = Camera.main.transform.position.y - 20;
				Debug.Log(targetY);

			//cost gauge
			Game1Var.gauge -= cost;
		}
		else //not enough gauge :(
		{
			NotificationCenter.DefaultCenter().PostNotification(this, "NotEnoughGauge");
		}
	}
	
	//Rewinds 
	if(timerRewind>0)
	{
		Camera.main.transform.position.y = Mathf.Lerp(Camera.main.transform.position.y, targetY, 0.1);

		timerRewind -= Time.deltaTime;
	}
	if(Game1Var.gauge >= cost)
	{
		canUse = true;
	}
	else
	{
		canUse = false;
	}
}

function Rabbitgeddon ()
{
	var cost: float; //cost of gauge
	var costList = new float[5]; costList = [100.0, 80, 60, 40, 20];

	cost = costList[upgradesList[1].level-1];
	if(Input.GetMouseButtonDown(1))//when mouse down
	{
		if(Game1Var.gauge >= cost)
		{
			Game1Var.gauge -= cost;

			displayGlow.renderer.material.color.a = 1;
			effect.GetComponent(Speedlines).Burst();
			
			var enemies: GameObject[];
			enemies = GameObject.FindGameObjectsWithTag("Enemy");
			for(enemy in enemies)
			{
				enemy.GetComponent(Enemy).Death();
			}
			var hazards: GameObject[];
			hazards = GameObject.FindGameObjectsWithTag("Hazard");
			for(hazard in hazards)
			{
				Destroy(hazard);
			}
		}
		else //not enough gauge :(
		{
			NotificationCenter.DefaultCenter().PostNotification(this, "NotEnoughGauge");
		}
	}
	
}