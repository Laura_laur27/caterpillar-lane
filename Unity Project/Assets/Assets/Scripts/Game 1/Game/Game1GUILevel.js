#pragma strict

var type: int;
var level: int;

function Start ()
{
	if(type == 1)
	{
		renderer.material.color = Color(0.5,0.25,0.25,0);
	}
	if(type == 2)
	{
		renderer.material.color = Color(0.25,0.5,0.5,0);
	}
	if(type == 3)
	{
		renderer.material.color = Color(0.33,0.5,0.25,0);
	}
}

function Update ()
{
	if(type == 1 && Game1Var.rabidityLvl >= level)
	{
		renderer.material.color.a = Mathf.Lerp(renderer.material.color.a, 1, 0.1);
	}
	if(type == 2 && Game1Var.virilityLvl >= level)
	{
		renderer.material.color.a = Mathf.Lerp(renderer.material.color.a, 1, 0.1);
	}
	if(type == 3 && Game1Var.rushLvl >= level)
	{
		renderer.material.color.a = Mathf.Lerp(renderer.material.color.a, 1, 0.1);
	}
}