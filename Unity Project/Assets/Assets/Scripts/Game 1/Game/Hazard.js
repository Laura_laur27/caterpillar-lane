#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

//parameters
var name: String;
var slow: float;
var slowMod: float;
var damage: float;

//children
var sprite: Transform; sprite = transform.Find("Sprite");

function Start ()
{

}

function Update ()
{
	//destroys rabbit when off screen
	if (transform.position.y - Camera.main.transform.position.y <= -40)
	{
		Destroy(gameObject);
	}
	
	if(slow == 1 && damage == 0)
	{
		Destroy(gameObject);
	}
	
	if(upgradesList[16].equipped)
	{
		sprite.renderer.material.color.a = Mathf.Lerp(sprite.renderer.material.color.a, 1, 0.02);
	}
}

function OnTriggerEnter (collider: Collider)
{
	if(collider.gameObject.tag == "Rabbit")
	{
		var script: Rabbit; script = collider.gameObject.GetComponent("Rabbit");
		script.speedCC = (slow+((1-slow)*(1-Game1Stats.modCC)));
		script.TakeDamage(damage/Time.deltaTime/2);
		if(upgradesList[16].equipped)
		{
			sprite.renderer.material.color.a -= 0.02;
			script.sprite.renderer.material.color = Color(0.3,1,0);
		}
	}
}

function OnTriggerExit (collider: Collider)
{
	if(collider.gameObject.tag == "Rabbit")
	{
		var script: Rabbit; script = collider.gameObject.GetComponent("Rabbit");
		script.speedCC = 1;
	}
}