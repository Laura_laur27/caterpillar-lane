#pragma strict
import System.Collections.Generic;

var ticker: float; ticker = 0.0;
var houses = new List.<GameObject>();
var target: GameObject;
var empty: boolean;
var spawnPosition: Vector3;

var spawnNo: int;

function Start ()
{

}

function Update ()
{
	//every ticker seconds, spawn x ammount of enemies
	if(ticker>0)
	{
		ticker -= Time.deltaTime;
	}
	else
	{
		EnemySpawn();
		if(Random.value < 0.33)
		{
			EnemySpawn();
		}
		ticker = 2 + (Random.value-0.5)*3;
	}
	
	//spawn number
	spawnNo = Mathf.RoundToInt(0.33*(0.1143*Game1Var.distanceTravelled + 4.2857));
	
	//moves with camera
	transform.position.y = Game1Var.distanceTravelled;
}

function EnemySpawn ()
{
	houses.Clear();

	//chooses random house
	for(object in Physics.OverlapSphere(transform.position, 20))
	{
		if(object.collider.gameObject.tag == "House")
		{
			if(!object.gameObject.GetComponent(House).titan)
			{
				houses.Add(object.gameObject);
			}
		}
	}
	if(houses.Count <= 0)
	{
		empty = true;
	}
	else
	{
		empty = false;
		target = houses[Random.Range(0, houses.Count)];
	}
	
	//chooses random position
	spawnPosition.x = Random.Range(-9, 9);
	spawnPosition.y = transform.position.y + 25;
	spawnPosition.z = 0;
	
	//Instantiate X number of enemies at location
	if(!empty && Game1Var.distanceTravelled>100)
	{
		for (var i : int = 0; i < spawnNo; i++)
		{
			var spawn: Transform;
			spawn = PoolManager.Pools["EnemyPool"].Spawn(Game1Var.prefabEnemy.transform, spawnPosition + Vector3((Random.value-0.5)*3, (Random.value-0.5)*3, 0), Quaternion.identity);
			//Moves each rabbit to new location
			spawn.SendMessage("SetTarget", target);
		}
	}
}