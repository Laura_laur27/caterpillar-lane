#pragma strict

var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject; 
var objectSelect = new List.<GameObject>();

var num: int;
var origin: Vector3;
var target: Vector3;

function Start ()
{

}

function Update ()
{
	//Constantly Updates Selected object
	objectHover = varScript.objectHover;
	objectSelect = varScript.objectSelect;
	
	//checks if active
	if(objectSelect.Count >= num && Time.timeScale > 0.1)
	{
		Target();
	}
	else
	{
		transform.position = Vector3(-100,-100,-100);
	}
}

function Target ()
{
	//sets origin vector
	origin = objectSelect[num-1].transform.position;
	origin.z = -5;
	//sets target vector
	if(objectHover != null)
	{
		target = objectHover.transform.position;
		target.z = -5;
	}
	else
	{
		var ray: Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		var hit: RaycastHit;
		if (Physics.Raycast(ray, hit))
		{
			target = hit.point;
			target.z = -5;
		}
	}

	//centers the target
	if(objectHover != null)
	{
		transform.position = (target+origin)/2;
	}
	else
	{
		transform.position = (target+origin)/2 + (target-origin).normalized*1.4;
	}

	//rotates line based on angle between two vectors
	var direction: float; direction = -1;
	if(target.x-origin.x<0)
	{
		direction = 1;
	}
	transform.eulerAngles.z = Vector3.Angle(Vector3(0,1,0), (target-origin))*direction;
	
	//stretches based on vector
	if(objectHover != null)
	{
		transform.localScale.y = (target-origin).magnitude - 5;
	}
	else
	{
		transform.localScale.y = (target-origin).magnitude - 2.5;
	}
	if(transform.localScale.y<0)
	{
		transform.localScale.y = 0;
	}
	
}