#pragma strict

//controls color of healthbar base

var health: float; 
var color1: Material; color1 = Resources.Load("healthbar0");
var color2: Material; color2 = Resources.Load("healthbar1");
var color3: Material; color3 = Resources.Load("healthbar2");
var color4: Material; color4 = Resources.Load("healthbar3");


function Start ()
{
	if(transform.parent.GetComponent(House).titan)
	{
		Destroy(gameObject);
	}
}

function Update ()
{
	if(!transform.parent.GetComponent(House).titan)
	{
		health = transform.parent.GetComponent(House).health;
		
		if(health<=100)
		{
			renderer.material = color1;
		}
		else if(health<=200)
		{
			renderer.material = color2;
		}
		else if(health<=300)
		{
			renderer.material = color3;
		}
		else
		{
			renderer.material = color4;
		}
	}
}