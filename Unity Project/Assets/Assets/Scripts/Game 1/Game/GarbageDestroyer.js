#pragma strict

function Start ()
{

}

function Update ()
{
	transform.position.y = Game1Var.distanceTravelled - 60;
}

function OnCollisionEnter(collision : Collision)
{
	if(collision.gameObject.tag == "rabbit")
	{
		PoolManager.Pools["RabbitPool"].Despawn(collision.transform);
	}
	else
	{
		Destroy(collision.gameObject);
	}
}