#pragma strict

var textPop: Transform; textPop = transform.Find("Text Population");
var textPopTimer: int; textPopTimer = 5;
var textGold: Transform; textGold = transform.Find("Text Gold");
var textDistance: Transform; textDistance = transform.Find("Text Distance");

function Start ()
{

}

function Update ()
{
	if(textPopTimer < 0)
	{
		textPop.GetComponent(TextMesh).text = Game1Var.totalPop.ToString();
		textPopTimer = 5;
	}
	else
	{
		textPopTimer -= 1;
	}
	
	textGold.GetComponent(TextMesh).text = Game1Var.currGold.ToString();
	textDistance.GetComponent(TextMesh).text = Game1Var.distanceTravelled.ToString("F0") + "m";
}