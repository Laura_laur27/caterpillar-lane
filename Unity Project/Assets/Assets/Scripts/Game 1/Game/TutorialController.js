#pragma strict
//other scripts
var varScript: Game1Var; varScript = GameObject.Find("Controller1").GetComponent("Game1Var");
var objectHover: GameObject; 
var objectSelect = new List.<GameObject>(); 
var BGM: GameObject; BGM = GameObject.Find("BGM");

var timeSpeed: float;

var resuming: boolean;

var inTut: boolean;
var introTut: boolean; introTut = false;

var cursor: GameObject;
var startHouse: GameObject;
var endHouse: GameObject;
var pauseOverlay: GameObject;

var titan: GameObject;
var hazard: GameObject;
var powerup: GameObject;
var activeOb: GameObject;

var tutTimer: float;

var objTit: GameObject;
var objPow: GameObject;
var objHaz: GameObject;
var actTit: boolean;
var actPow: boolean;
var actHaz: boolean;

function Start ()
{
	InvokeRepeating("Search", 5, 1);
}

function Update ()
{
	tutTimer -= Time.deltaTime;
	
	//Constantly Updates Hover object
	objectHover = varScript.objectHover;
	objectSelect = varScript.objectSelect;

	//Time.timeScale = timeSpeed;

	//follows camera - parent
	//transform.position.y = Camera.main.transform.position.y;
	
	//does intro tutorial until first invade
	if(Game1Var.externalPop <= 0 && !introTut)
	{
		IntroTut();
	}
	if(Game1Var.externalPop > 0 && !introTut)
	{
		inTut = false;
		introTut = true;
	}
	
	if(inTut)
	{
		cursor.active = true;
	}
	else
	{
		cursor.active = false;
	}
	
	if(!Game1SaveData.debugger && tutTimer<=0)
	{
		if(objTit != null && objTit.transform.position.y-Game1Var.distanceTravelled <= 9 && !Game1SaveData.tutTitan)
		{
			TitanTut(objTit.transform);
		}
		if(objPow != null && objPow.transform.position.x <= 8 && objPow.transform.position.x >= -8 && !Game1SaveData.tutPowerup)
		{
			PowerupTut(objPow.transform);
		}
		if(objHaz != null && objHaz.transform.position.y-Game1Var.distanceTravelled <= 9 && !Game1SaveData.tutHazard)
		{
			HazardTut(objHaz.transform);
		}
		if(Game1Var.gauge >= 60 && !Game1SaveData.tutActive)
		{
			ActiveTut();	
		}
	}
	
	if(resuming && Time.timeScale < 0.96)
	{
		//speeds up time
		if(Time.timeScale < 0.95)
		{
			Time.timeScale = Mathf.Lerp(Time.timeScale, 1, 0.5);
			//Time.fixedDeltaTime = 0.02 * Time.timeScale;
		}
		else
		{
			Time.timeScale = 1;
			Time.fixedDeltaTime = 0.02;
	
			//greys screen out
			
		}	
		//increases music volume
		Camera.mainCamera.audio.volume = 1;
		if(GameObject.FindGameObjectWithTag("BGM") != null)
		{
			BGM = GameObject.FindGameObjectWithTag("BGM");
			BGM.audio.volume = 1;
		}
		pauseOverlay.renderer.material.color.a = 0.0;
		Camera.main.GetComponent(CameraFX).sat = 0.8;
	}
}

function Search ()
{
	if(!(actTit && actPow && actHaz))
	{
		if(!actTit)
		{
			var tempHouse: GameObject[];
			tempHouse = GameObject.FindGameObjectsWithTag("House");
			for(house in tempHouse)
			{
				if(house.GetComponent(House).titan)
				{
					objTit = house;
					actTit = true;
					break;
				}
			}
		}
		
		if(!actPow)
		{
			var tempPow: GameObject;
			tempPow = GameObject.FindGameObjectWithTag("Powerup");
			if(tempPow != null)
			{
				objPow = tempPow;
				actPow = true;
			}
		}	
		if(!actHaz)
		{
			var tempHaz: GameObject;
			tempHaz = GameObject.FindGameObjectWithTag("Hazard");
			if(tempHaz != null)
			{
				objHaz = tempHaz;
				actHaz = true;
			}
		}
	}
}

function Pause ()
{
	resuming = false;
	
	//gradually slows down time
	if(Time.timeScale > 0.05)
	{
		Time.timeScale = Mathf.Lerp(Time.timeScale, 0, 0.33);
		Time.fixedDeltaTime = 0.02 * Time.timeScale;
	}
	else
	{
		Time.timeScale = 0;
		Time.fixedDeltaTime = 0.02;
		
		//greys screen out
		pauseOverlay.renderer.material.color.a = 0.33;
		Camera.main.GetComponent(CameraFX).sat = 0.0;
		
	}
	
	//decreases music volume
	Camera.mainCamera.audio.volume = 0.4;
	if(GameObject.FindGameObjectWithTag("BGM") != null)
	{
		BGM = GameObject.FindGameObjectWithTag("BGM");
		BGM.audio.volume = 0.4;
	}
}

function Resume ()
{
	resuming = true;
	tutTimer = 4;
}

function IntroTut ()
{
	inTut = true;
	
	//if no house is selected, hover to start house
	if(objectSelect.Count <= 0)
	{
		cursor.transform.position = startHouse.transform.position + Vector3(0,0,-4.5);
	}
	else
	{
		cursor.transform.position = Vector3.Lerp(cursor.transform.position, endHouse.transform.position + Vector3(0,0,-4.5), 0.1);
	}
}

function TitanTut (pos: Transform)
{
	inTut = true;
	Pause();
	cursor.transform.position = pos.position + Vector3(0,0,-9);
	titan.active = true;
}

function HazardTut (pos: Transform)
{
	inTut = true;
	Pause();
	cursor.transform.position = pos.position + Vector3(0,0,-9);
	hazard.active = true;
}

function PowerupTut (pos: Transform)
{
	inTut = true;
	Pause();
	cursor.transform.position = pos.position + Vector3(0,0,-9);
	powerup.active = true;
}

function ActiveTut ()
{
	inTut = true;
	Pause();
	cursor.transform.position = Vector3(100,100,100);
	activeOb.active = true;
}

function StockTut ()
{

}