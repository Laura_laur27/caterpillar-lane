#pragma strict

var colorStart: Color;
var colorEnd: Color;
var timer: float; timer = 0;

function Start ()
{

}

function Update ()
{
	if(timer <= 0)
	{
		timer = 0.66;
	}
	else
	{
		timer -= Time.deltaTime;
	}
	renderer.material.color = Color.Lerp(colorStart, colorEnd, 1-timer/0.66);
	renderer.material.color.a = Mathf.Lerp(0.6, 0.2, 1-timer/0.66);
	transform.localScale.x = Mathf.Lerp(2, 5, 1-timer/0.66);
	transform.localScale.z = Mathf.Lerp(2, 5, 1-timer/0.66);
}