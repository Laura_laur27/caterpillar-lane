#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

//states

//parameters
var attack: float;
var speed: float; speed = 1;
var target: GameObject;

//objects
var sprite: Transform;
var enemyDeathPrefab: GameObject; enemyDeathPrefab = Resources.Load("EnemyDeath");

function OnSpawned ()
{
	sprite = transform.Find("Sprite");
	
	//changes sprite to 0
}

function Update ()
{
	//destroys when off screen
	if (transform.position.y - Camera.main.transform.position.y <= -17)
	{
		PoolManager.Pools["EnemyPool"].Despawn(transform);
	}
	
	//interact with houses once close enough
	if (target != null && target.tag == "House")
	{
		InteractHouse();
	}
}

function FixedUpdate ()
{
	//moves rabbit towards destination
	if(target != null)
	{
		Travel ();
	}
	else
	{
		target == Game1Var.garbageDestroyer;
	}
}

function InteractHouse ()
{
	var toHouse: Vector3; //vector towards house
	toHouse = (target.transform.position - transform.position);
	toHouse.z = 0;
	if (toHouse.magnitude <= 2)//goes into house when close enough
	{
		GoIn ();
	}
}

function SetTarget (house: GameObject)
{
	//sets the target of the rabbit
	target = house;
}

function Travel ()
{
	//moves enemy towards destination
	transform.position = transform.position.MoveTowards(transform.position, target.transform.position + Vector3(0, 0, 0), speed*0.065);

	//rotates rabbit to face target house
	transform.LookAt(target.transform.position, Vector3.back);
}

function GoIn ()
{
	var script: House = target.GetComponent(House);
	script.populationFloat -= 1;
	PoolManager.Pools["EnemyPool"].Despawn(transform);
}

function OnCollisionEnter (collision: Collision)
{
	if(collision.gameObject.tag == "Rabbit")
	{
		//damages in aoe
		var radius: float; radius = 2;//determines radius of explosion based on level
		var damage: float; damage = 25; //determines damage of explosion based on level
		for(object in Physics.OverlapSphere(transform.position, radius))
		{
			if(object.collider.gameObject.tag == "Rabbit")
			{
				//var script: Rabbit; script = object.collider.gameObject.GetComponent("Rabbit");
				//script.health -= 2.5*damage;
				object.SendMessage("TakeDamage", damage/(Time.deltaTime*0.5));
			}
		}
		Death();
	}
}

function Death ()
{
	if(Random.value < 0.65)
	{
		PoolManager.Pools["EnemyDeathPool"].Spawn(enemyDeathPrefab.transform, transform.position + Vector3(0, 0, 0), Quaternion.Euler(90,0,0));
	}
	PoolManager.Pools["EnemyPool"].Despawn(transform);
}