#pragma strict

class HouseType
{
	var name: String;
	var special: boolean;
	var specialPart: GameObject;
	var health: float;
	var attack: float;
	var breed: float;
	var gold: int;
	var sprite: Material;
	var co1: float;
	var co2: float;
	var co3: float;
	var rate: float;
}

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

var houseTypeList: HouseType[];
var totalRate: float;

function Start ()
{
	upgradesList = upgradeScript.upgradesList;
}

function Update ()
{
	CalculateHouseRates();
}

function CalculateHouseRates ()
{
	var adjDistance = Game1Var.distanceTravelled/250;
	var rateToNow: float; rateToNow = 0;
	totalRate = 0;
	
	//calculates house rate & sums rates for total rate
	for(var i=0; i < houseTypeList.length; i++)
	{
		houseTypeList[i].rate = (houseTypeList[i].co1*adjDistance*adjDistance) + (houseTypeList[i].co2*adjDistance) + houseTypeList[i].co3;
		if (houseTypeList[i].rate < 0)
		{
			houseTypeList[i].rate = 0;
		}
		rateToNow = totalRate;
		totalRate += houseTypeList[i].rate;
		houseTypeList[i].rate += rateToNow;
	}
}

function SetHouseType () : HouseType
{
	//based on distance travelled, randomly returns house type
	var chosenHouse: HouseType;
	
	//Checks if house turns out to be special type of house
	var randomNumber = Random.Range(1,4);
	var specialLvl: float; specialLvl = 0;
	var specialMod: float; specialMod = 1;
	if (upgradesList[13].equipped)//rabbits foot
	{
		specialMod = 0.25*upgradesList[13].level + 1;
	}
	//checks if lucky
	if(Random.value < (specialMod*0.045))
	{
		chosenHouse = houseTypeList[randomNumber+4];
	}
	else //if not, generate normal house
	{
		var i: int; i = 0;
		var randomFloat: float; randomFloat = Random.value*totalRate - 0.001;
		while (houseTypeList[i].rate < randomFloat)
		{
			i++;
		}
		chosenHouse = houseTypeList[i];
	}
	return chosenHouse;
}