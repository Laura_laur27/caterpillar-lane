#pragma strict

var yMax: float; yMax = -4.815;
var yMin: float; yMin = -13.72;

function Start ()
{

}

function Update ()
{
	if(Game1Var.gauge>0)
	{
		transform.localScale.z = Mathf.Lerp(transform.localScale.z, 21*Game1Var.gauge/100, 0.2);
	}
	else
	{
		transform.localScale.z = Mathf.Lerp(transform.localScale.z, 0, 0.2);
	}
	//updates position
	transform.localPosition.y = Mathf.Lerp(transform.localPosition.y, Mathf.Lerp(yMin, yMax, Game1Var.gauge/100), 0.2);
}