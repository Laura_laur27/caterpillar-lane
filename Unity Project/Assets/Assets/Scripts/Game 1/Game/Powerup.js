#pragma strict

//type: 1=gold, 2=gauge, 3=berserk, 4=invul, 5=haste
//params
var powerupType: int;
var travelDir: Vector3;
var powerupTimer: float; powerupTimer = 5;

var activated: boolean; activated = false;
var activeTime: float; activeTime = 10.0;

var shakeCounter: float;
var colorCounter: float; colorCounter = 0;

//objects
var sprite: Transform; sprite = transform.Find("Sprite");

function Start ()
{
	particleSystem.emissionRate = 0;
}

function Update ()
{
	transform.position += travelDir * Time.deltaTime * 2;
	
	//destroys when off screen
	if ((transform.position.y - Camera.main.transform.position.y <= -25) || (transform.position.y - Camera.main.transform.position.y >= 25))
	{
		Destroy (gameObject);
	}
	
	//SHAKE
	Shake(shakeCounter);
	if (shakeCounter <= 0)
	{
		shakeCounter = 0;
	}
	else
	{
		shakeCounter -= 0.05;
	}
	
	//change color
	sprite.renderer.material.color = Color.Lerp(Color(0.5,0.5,0.5), Color(1,1,1), colorCounter);
	if(powerupType == 1 || powerupType == 2)
	{
		colorCounter -= 0.025;
		if(particleSystem.emissionRate>0)
		{
			particleSystem.emissionRate -= 1;
		}
	}
}

function OnTriggerEnter (collider: Collider)
{
	if(collider.gameObject.tag == "Rabbit")
	{
		if(powerupType == 1)//money
		{
			Game1Var.moneyControl.GainGold(gameObject, 10);
			particleSystem.emissionRate = 60;
			shakeCounter = 2;
			colorCounter = 1;
		}
		else if(powerupType == 2)//gauge
		{
			NotificationCenter.DefaultCenter().PostNotification(this, "GainGauge", 0.5);
			particleSystem.emissionRate = 60;
			shakeCounter = 2;
			colorCounter = 1;
		}
		else//special
		{
			ActivatePower ();
		}
	}
}

function ActivatePower ()
{
	activated = true;
	
	if(powerupType == 3)//berserk
	{
		Game1Var.berserk = activeTime;
		Game1Var.invulnerable = -0.1;
		Game1Var.haste = -0.1;
		Game1Var.powerupControl.Powerup(1);
	}
	if(powerupType == 4)//invuln
	{
		Game1Var.berserk = -0.1;
		Game1Var.invulnerable = activeTime;
		Game1Var.haste = -0.1;
		Game1Var.powerupControl.Powerup(2);
	}
	if(powerupType == 5)//haste
	{
		Game1Var.berserk = -0.1;
		Game1Var.invulnerable = -0.1;
		Game1Var.haste = activeTime;
		Game1Var.powerupControl.Powerup(3);
	}
	Destroy(gameObject);
}

function Shake(amount: float)
{
	var speed = 30.0f; //how fast it shakes
	var rotate: float;
	rotate = amount*Mathf.Sin(Time.time * speed);
	//transform.Rotate(Vector3(0, 0, rotate));
}