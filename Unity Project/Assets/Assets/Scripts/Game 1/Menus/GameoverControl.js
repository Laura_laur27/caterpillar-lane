#pragma strict
import System.Collections.Generic;

var scriptSave: Game1SaveData; scriptSave = GameObject.Find("Game 1 Save").GetComponent(Game1SaveData);
var popList : List.<int>; popList = new List.<int>();

var sound: GameObject;
var textPref: GameObject; textPref = Resources.Load("Text Popup");

var textGold: GameObject; textGold = GameObject.Find("Text Gold");
var goldOld: int;
var textDistance: GameObject; textDistance = GameObject.Find("Text Distance");
var distanceOld: int;
var textToken: GameObject; textToken = GameObject.Find("Text Token");
var tokenOld: int;
var tokenRan: int;
var tokenBonus: int;
var textInvaded: GameObject; textInvaded = GameObject.Find("Text Invaded");
var invadedOld: int;

var timer: float; timer = 0;
var ticker: float; ticker = 0;

var stockCheck: boolean; stockCheck = false;
var newStock: boolean; newStock = false;

var newRecord: boolean; newRecord = false;

var tut: GameObject;

function Start ()
{
	tut.active = false;
	
	//changes values
	goldOld = 0;
	distanceOld = 0;
	Game1SaveData.totalGold += Game1SaveData.lastGold;
	Game1SaveData.totalDistance += Game1SaveData.lastDistance;
	
	tokenOld = 0;
	tokenRan = Mathf.RoundToInt(Random.Range(0,6));
	Debug.Log(tokenRan);
	Game1SaveData.totalStock += tokenRan;
	invadedOld = 0;
	
	//sees if setting new record
	if(Game1SaveData.lastDistance > Game1SaveData.record)
	{
		Game1SaveData.record = Mathf.RoundToInt(Game1SaveData.lastDistance);
		newRecord = true;
	}
	
	//changes colors
	textGold.GetComponent(TextMesh).renderer.material.color = Color(1,0.9,0.5);
	textToken.GetComponent(TextMesh).renderer.material.color = Color(0.33,0.66,1);
	textInvaded.GetComponent(TextMesh).renderer.material.color = Color(1,0.4,0.2);
}

function Update ()
{
	var speed: float; speed = 2;
	//changes text of UI
	textGold.GetComponent(TextMesh).text = goldOld.ToString();
	textDistance.GetComponent(TextMesh).text = distanceOld.ToString() + "m";
	textToken.GetComponent(TextMesh).text = tokenOld.ToString();
	textInvaded.GetComponent(TextMesh).text = invadedOld.ToString();

	timer += Time.deltaTime;
	
	//after a while, adds gold and distance
	if(timer>=1.0)
	{
		goldOld = Mathf.Lerp(goldOld, Game1SaveData.lastGold, ticker/speed);
		distanceOld = Mathf.Lerp(distanceOld, Game1SaveData.lastDistance, ticker/speed);
		tokenOld = Mathf.Lerp(tokenOld, tokenRan + tokenBonus, ticker/speed);
		invadedOld = Mathf.Lerp(invadedOld, Game1SaveData.housesInvaded, ticker/speed);
		ticker += Time.deltaTime;
	}
	
	var popUp: GameObject;
	if(timer>=1.5 && Game1SaveData.breached)
	{
		//breach bonus
		Game1SaveData.totalStock += 10;
		tokenBonus += 10;
		audio.Play();
		sound.audio.Play();
		popUp = Instantiate(textPref, Camera.main.transform.position + Vector3(0, 0, 1), transform.rotation);
		popUp.transform.localScale *= 0.7;
		popUp.GetComponent(TextMesh).text = "BREACH!" + "\n" + "10 BONUS TOKENS";
		popUp.renderer.material.color = Color(1, 0.75, 0.75);
		scriptSave.Breach();
		Game1SaveData.breached = false;
	}
	if(timer>=4 && newRecord)
	{
		//newrecord bonus
		Game1SaveData.totalStock += 5;
		tokenBonus += 5;
		audio.Play();
		sound.audio.Play();
		popUp = Instantiate(textPref, Camera.main.transform.position + Vector3(0, 0, 1), transform.rotation);
		popUp.transform.localScale *= 0.7;
		popUp.GetComponent(TextMesh).text = "NEW RECORD!" + "\n" + "5 BONUS TOKENS";
		popUp.renderer.material.color = Color(1, 0.75, 0.75);
		newRecord = false;
	}
	if(timer>=6.5 && !Game1SaveData.tutStock)
	{
		tut.active = true;
		Game1SaveData.tutStock = true;
	}
}