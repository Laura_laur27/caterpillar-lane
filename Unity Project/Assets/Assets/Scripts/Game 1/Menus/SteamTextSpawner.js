#pragma strict
import System.Collections.Generic;
import System;

var timer: float; timer = 0;
var element: int; element = 0;

var textList: List.<String>; textList = new List.<String>();

var BGM: GameObject;


var stringList = new String[12]; stringList = ["lol","you srtill playing that game","still*","lol you should come over >.>","bored as hell","ah fuckit I’m heading over","catch","lol before I forget", "do you know about the hack>","in the shop, press the carrot 4 times","saw it on a forum","catch"];
var timeList = new float[12]; timeList = [8.0,4,2,6,2.5,5,2,4,3.5,3,3,2];
var timeFlutter: float; timeFlutter = (UnityEngine.Random.value-0.5)*1;

var text1: GameObject; text1 = GameObject.Find("Steam Text 5");
var text2: GameObject; text2 = GameObject.Find("Steam Text 4");
var text3: GameObject; text3 = GameObject.Find("Steam Text 3");
var text4: GameObject; text4 = GameObject.Find("Steam Text 2");
var text5: GameObject; text5 = GameObject.Find("Steam Text 1");
var box: GameObject; box = GameObject.Find("Box");

var boxBig: GameObject; boxBig = GameObject.Find("Box Big");
var button: GameObject; button = GameObject.Find("Steam Button");

function Awake ()
{
	DontDestroyOnLoad (transform.gameObject);
}

function Start ()
{
	for(var i = 0; i<5; i++)
	{
		textList.Add("");
	}
	
	//sets and inits steam button
	button.renderer.material.color.a = 0;
	var buttonScript: SteamButton;
	buttonScript = button.GetComponent(SteamButton);
	buttonScript.box = boxBig;
	buttonScript.box2 = box;
	boxBig.active = false;
}

function Update ()
{
	timer += Time.deltaTime;
	
	//after x seconds, add string
	if(element < 12 && timer>(timeList[element]+timeFlutter)*1.5)
	{
		button.renderer.material.color.a = 0.8;
		box.active = true;
		
		Camera.mainCamera.audio.volume = 0.4;
		if(GameObject.FindGameObjectWithTag("BGM") != null)
		{
			BGM = GameObject.FindGameObjectWithTag("BGM");
			BGM.audio.volume = 0.4;
		}
		
		audio.Play();
		
		stringList[element] = "[" + System.DateTime.Now.Hour.ToString() + ":" + System.DateTime.Now.Minute.ToString() + "] " + stringList[element];
		
		var newText: String;
		newText = "[" + System.DateTime.Now.Hour.ToString() + ":" + System.DateTime.Now.Minute.ToString() + "] " + stringList[element];
		
		//adds string to text list
		textList.Add(stringList[element]);
		if(textList.Count >= 6)
		{
			textList.RemoveAt(0);
		}
		
		timeFlutter = (UnityEngine.Random.value-0.5)*1;
		
		//updates text
		text1.GetComponent(TextMesh).text = textList[4];
		text2.GetComponent(TextMesh).text = textList[3];
		text3.GetComponent(TextMesh).text = textList[2];
		text4.GetComponent(TextMesh).text = textList[1];
		text5.GetComponent(TextMesh).text = textList[0];
		
		//refreshes cooldown
		text5.GetComponent(SteamText).timer = text4.GetComponent(SteamText).timer;
		text4.GetComponent(SteamText).timer = text3.GetComponent(SteamText).timer;
		text3.GetComponent(SteamText).timer = text2.GetComponent(SteamText).timer;
		text2.GetComponent(SteamText).timer = text1.GetComponent(SteamText).timer;
		text1.GetComponent(SteamText).timer = 15;
		box.GetComponent(SteamBox).timer = 8;
		
		timer = 0;
		element ++;
	}
	if(element >= 12 && timer > 10)
	{
		Button();
	}
}

function Button ()
{
	var buttonScript: SteamButton;
	buttonScript = button.GetComponent(SteamButton);
	buttonScript.stringList = stringList;
	buttonScript.on = true;
}