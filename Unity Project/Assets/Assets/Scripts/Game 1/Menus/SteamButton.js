#pragma strict

var init: boolean; init = false;
var textList = new GameObject[12];
var stringList = new String[12];

var on: boolean; on = false;

var box: GameObject;
var steamText: GameObject; steamText = Resources.Load("Steam Text Basic");

var box2: GameObject;

function Start ()
{

}

function Update ()
{

}

function OnMouseEnter ()
{
	renderer.material.color = Color(1,1,1,0.8);
}

function OnMouseExit ()
{
	renderer.material.color = Color(0.5,0.5,0.5,0.8);
}

function OnMouseDown ()
{
	renderer.material.color = Color(1,1,1,1);
	
	box2.active = !box2.active;
	
	if(on)
	{
		box.active = !box.active;
		
		if(!init)
		{
			InitText();
			init = true;
		}
	}
}

function OnMouseUp ()
{
	renderer.material.color = Color(1,1,1,0.8);
}

function InitText ()
{
	for(var i = 0; i<12; i++)
	{
		var posY: float;
		posY = 0.53723 - i*1.2;
		textList[i] = Instantiate(steamText, Vector3(-18.5, posY, box.transform.position.z-0.5), Quaternion.Euler(0,0,0));
		textList[i].GetComponent(TextMesh).text = stringList[i];
		textList[i].transform.parent = box.transform;
	}
}