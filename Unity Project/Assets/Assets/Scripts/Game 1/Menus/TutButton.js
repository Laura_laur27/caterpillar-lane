#pragma strict

var shop: boolean;
var equip: boolean;

var count: int; count = 0;
var back: GameObject; back = GameObject.Find("Background");

var page2: Texture2D;
var page3: Texture2D;

function Start ()
{

}

function Update ()
{

}

function OnMouseEnter()
{
	renderer.material.color = Color(1,0.4,0,0.4);
}

function OnMouseDown()
{
	renderer.material.color = Color(1,0,0,0.5);
}

function OnMouseExit()
{
	renderer.material.color = Color(1,0,0,0.2);	
}

function OnMouseUpAsButton()
{
	if(shop)
	{
		//loads shop menu
		Application.LoadLevel(3);
	}
	else if(equip)
	{
		//loads equip tutorial
		Application.LoadLevel(2);
	}
	else
	{
		if(count == 0)
		{
			//loads page 2
			back.renderer.material.mainTexture = page2;
		}
		if(count == 1)
		{
			//loads page 3
			back.renderer.material.mainTexture = page3;
		}
		if(count == 2)
		{
			//loads game
			Application.LoadLevel(1);
		}
	}
	count ++;
}