#pragma strict

var spiders: GameObject; spiders = GameObject.Find("Spiders");
var boss: GameObject; boss = GameObject.Find("Boss");

function Start ()
{
	GameObject.Find("Text Record").GetComponent(TextMesh).text = Game1SaveData.record.ToString() + "m";
}

function Update ()
{

}

function OnMouseOver()
{
	renderer.material.color.a = Mathf.Lerp(renderer.material.color.a, 0.25, 0.1);
	spiders.renderer.material.mainTextureOffset.y += Time.deltaTime*0.09;
	boss.GetComponent(Sprite).framesPerSecond = 5;//Mathf.Lerp(boss.GetComponent(Sprite).framesPerSecond, 6, 0.02);
}

function OnMouseDown()
{
	Application.LoadLevel(1);
}

function OnMouseExit()
{
	renderer.material.color.a = 0;//Mathf.Lerp(renderer.material.color.a, 0, 0.1);
	boss.GetComponent(Sprite).framesPerSecond = 0;
}