#pragma strict
import System.Collections.Generic;

static var selectedItem: GameObject;

var textPref: GameObject; textPref = Resources.Load("Text Popup");
var sound: GameObject;

var upgradeScript: Game1SaveData;
var itemList: List.<StoreItem>; itemList = new List.<StoreItem>();
var item1: StoreItem;
var item2: StoreItem;
var item3: StoreItem;

//var item4: StoreItem;

var confirmButton: GameObject; confirmButton = GameObject.Find("Buy Confirm Button");

var textName: GameObject; textName = GameObject.Find("Text Name");
var textLvl: GameObject; textLvl = GameObject.Find("Text Level");
var textBlurb: GameObject; textBlurb = GameObject.Find("Text Blurb");
var textGold: GameObject; textGold = GameObject.Find("Text Gold");
var textCost: GameObject; textCost = GameObject.Find("Text Cost");
var textStock: GameObject; textStock = GameObject.Find("Text Stock");

var costList = new int[5]; costList = [500, 750, 1250, 2000, 4000];
var costs = new int[3];
var currcost: int;
var canBuy: boolean; canBuy = false;
var bought: boolean; bought = false;

var spin1: float; spin1 = 0;
var spin2: float; spin2 = 0;
var spin3: float; spin3 = 0;
var spinTimer: float; spinTimer = 0;

var stop1: boolean;
var stop2: boolean;
var stop3: boolean;

var lucky1: boolean;
var lucky2: boolean;
var lucky3: boolean;

function Start ()
{
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	itemList = upgradeScript.itemList;
	
	//if there are no set stores
	if(itemList.Count <= 0)
	{
		var i: int;
		//active 1
		var upgradeNo: int;
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(0, 18);
			if(upgradeScript.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		var level: int; level = upgradeScript.upgradesList[upgradeNo].level;
		var cost: int; cost = costList[upgradeScript.upgradesList[upgradeNo].level];
		if(upgradeScript.upgradesList[upgradeNo].active)
		{
			cost *= 2;
		}
		item1.num = 1;
		item1.upgrade = upgradeNo;
		item1.level = level + 1;
		item1.bought = false;
		item1.cost = cost;
		upgradeScript.itemList.Add(item1);
		
		//item 2
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(0, 18);
			if(upgradeScript.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		level = upgradeScript.upgradesList[upgradeNo].level;
		cost = costList[upgradeScript.upgradesList[upgradeNo].level];
		if(upgradeScript.upgradesList[upgradeNo].active)
		{
			cost *= 2;
		}
		item2.num = 2;
		item2.upgrade = upgradeNo;
		item2.level = level + 1;
		item2.bought = false;
		item2.cost = cost;
		upgradeScript.itemList.Add(item2);
		
		//item 3
		for(i = 0; i<30; i++)
		{
			upgradeNo = Random.Range(0, 18);
			if(upgradeScript.upgradesList[upgradeNo].level != 5)
			{
				break;
			}
		}
		level = upgradeScript.upgradesList[upgradeNo].level;
		cost = costList[upgradeScript.upgradesList[upgradeNo].level];
		if(upgradeScript.upgradesList[upgradeNo].active)
		{
			cost *= 2;
		}
		item3.num = 3;
		item3.upgrade = upgradeNo;
		item3.level = level + 1;
		item3.bought = false;
		item3.cost = cost;
		upgradeScript.itemList.Add(item3);
	}
	
	lucky1 = true;
	lucky2 = true;
	lucky3 = true;
}

function Update ()
{
	if(selectedItem != null)
	{
		currcost = upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].cost;
	}

	//sees if can buy
	if(selectedItem != null && Game1SaveData.totalGold >= currcost && upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].bought == false && stop1 && stop2 && stop3)
	{
		canBuy = true;
		confirmButton.GetComponent(Button).disabled = false;
	}
	else
	{
		canBuy = false;
		confirmButton.GetComponent(Button).disabled = true;
	}
		
	//TEXT if none selected
	if(selectedItem == null)
	{
		textName.GetComponent(TextMesh).text = "Upgrade";
		textLvl.GetComponent(TextMesh).text = "";
		textBlurb.GetComponent(TextMesh).text = "";
		//textCost.GetComponent(TextMesh).text = "";
	}
	else
	{
		if(!itemList[selectedItem.GetComponent(ShopBuyButton).number-1].bought)
		{
			textName.GetComponent(TextMesh).text = selectedItem.GetComponent(ShopBuyButton).upgrade.name;
			textLvl.GetComponent(TextMesh).text = (upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].level).ToString();
			textBlurb.GetComponent(TextMesh).text = BreakLine(selectedItem.GetComponent(ShopBuyButton).upgrade.blurb, 33) + "\n" + "\n" + BreakLine(selectedItem.GetComponent(ShopBuyButton).upgrade.blurbLvl, 33);
			//textCost.GetComponent(TextMesh).text = currcost.ToString();
		}
		else
		{
			textName.GetComponent(TextMesh).text = "SOLD OUT!";
			textLvl.GetComponent(TextMesh).text = "";
			textBlurb.GetComponent(TextMesh).text = "";
			//textCost.GetComponent(TextMesh).text = "---";
		}
	}
	//COST MONEYS
	GameObject.Find("Text Cost 1").GetComponent(TextMesh).text = upgradeScript.itemList[0].cost.ToString();
	GameObject.Find("Text Cost 2").GetComponent(TextMesh).text = upgradeScript.itemList[1].cost.ToString();
	GameObject.Find("Text Cost 3").GetComponent(TextMesh).text = upgradeScript.itemList[2].cost.ToString();
	
	textGold.GetComponent(TextMesh).text = Game1SaveData.totalGold.ToString();
	textStock.GetComponent(TextMesh).text = Game1SaveData.totalStock.ToString();
	
	//if one of the spins are true, disables buy button and spins every x seconds
	if(spinTimer<=0)
	{
		if(spin1 > 0)
		{
			lucky1 = false;
			var i: int;
			var upgradeNo: int;
			for(i = 0; i<30; i++)
			{
				upgradeNo = Random.Range(0, 18);
				if(upgradeScript.upgradesList[upgradeNo].level != 5)
				{
					break;
				}
			}
			var level: int; level = upgradeScript.upgradesList[upgradeNo].level;
			var cost: int; cost = costList[upgradeScript.upgradesList[upgradeNo].level];
			if(upgradeScript.upgradesList[upgradeNo].active)
			{
				cost *= 2;
			}
			upgradeScript.itemList[0].num = 1;
			upgradeScript.itemList[0].upgrade = upgradeNo;
			upgradeScript.itemList[0].level = level + 1;
			upgradeScript.itemList[0].bought = false;
			upgradeScript.itemList[0].cost = cost;
		}
		else if (!stop1)
		{
			//check if lucky?
			stop1 = true;
		}
		if(spin2 > 0)
		{
			for(i = 0; i<30; i++)
			{
				upgradeNo = Random.Range(0, 18);
				if(upgradeScript.upgradesList[upgradeNo].level != 5)
				{
					break;
				}
			}
			level = upgradeScript.upgradesList[upgradeNo].level;
			cost = costList[upgradeScript.upgradesList[upgradeNo].level];
			if(upgradeScript.upgradesList[upgradeNo].active)
			{
				cost *= 2;
			}
			upgradeScript.itemList[1].num = 2;
			upgradeScript.itemList[1].upgrade = upgradeNo;
			upgradeScript.itemList[1].level = level + 1;
			upgradeScript.itemList[1].bought = false;
			upgradeScript.itemList[1].cost = cost;
		}
		else if (!stop2)
		{
			//check if lucky?
			stop2 = true;
		}
		if(spin3 > 0)
		{
			lucky2 = false;
			for(i = 0; i<30; i++)
			{
				upgradeNo = Random.Range(0, 18);
				if(upgradeScript.upgradesList[upgradeNo].level != 5)
				{
					break;
				}
			}
			level = upgradeScript.upgradesList[upgradeNo].level;
			cost = costList[upgradeScript.upgradesList[upgradeNo].level];
			if(upgradeScript.upgradesList[upgradeNo].active)
			{
				cost *= 2;
			}
			upgradeScript.itemList[2].num = 3;
			upgradeScript.itemList[2].upgrade = upgradeNo;
			upgradeScript.itemList[2].level = level + 1;
			upgradeScript.itemList[2].bought = false;
			upgradeScript.itemList[2].cost = cost;
		}
		else if (!stop3)
		{
			//check if lucky?
			stop3 = true;
		}
		
		spinTimer = 0.05;
	}
	else
	{
		spinTimer -= Time.deltaTime;
	}
	//constantly pulls down the timers
	spin1 -= Time.deltaTime;
	spin2 -= Time.deltaTime;
	spin3 -= Time.deltaTime;
	
	//check luck when not spinnig
	if(stop1 && stop2 && stop3)
	{
		Lucky();
	}
}

function Buy ()
{
	//checks if enough money - if so, buy item
	Game1SaveData.totalGold -= currcost;
	upgradeScript.upgradesList[selectedItem.GetComponent(ShopBuyButton).upgrade.num].level += 1;

	//sets playerprefs
	upgradeScript.itemList[selectedItem.GetComponent(ShopBuyButton).number-1].bought = true;
}

function BreakLine (input: String, length: int): String
{
	//splits string
	var words: String[];
	var result: String; result = "";
	var line: String; line = "";
	words = input.Split(" "[0]);
	for(word in words)
	{
		var temp: String;
		temp = line + " " + word;
		if(temp.Length > length)
		{
			result += line + "\n";
			line = word;
		}
		else
		{
			line = temp;
		}
	}
	
	result += line;
	
	return result.Substring(1,result.Length-1);
}

function NewStock (number: int)
{
	lucky3 = false;
	if(number == 1)
	{
		stop1 = false;
		lucky1 = false;
		spin1 = 0.8;
	}
	if(number == 2)
	{
		stop2 = false;
		lucky1 = false;
	lucky2 = false;
		spin2 = 0.8;
	}
	if(number == 3)
	{
		stop3 = false;
		lucky2 = false;
		spin3 = 0.8;
	}
}

function Lucky ()
{
	//2 in a row - former
	if(!lucky1 && upgradeScript.itemList[0].upgrade == upgradeScript.itemList[1].upgrade && upgradeScript.itemList[1].upgrade != upgradeScript.itemList[2].upgrade)
	{
		//popup 2-in-a-row
		Debug.Log("2 in a row! 1");
		//gives $777
		LuckMoney();
		lucky1 = true;
	}
	
	//2 in a row - latter
	if(!lucky2 && upgradeScript.itemList[1].upgrade == upgradeScript.itemList[2].upgrade && upgradeScript.itemList[0].upgrade != upgradeScript.itemList[1].upgrade)
	{
		//popup 2-in-a-row
		Debug.Log("2 in a row! 3");
		//gives $777
		LuckMoney();
		lucky2 = true;
	}
	
	//3 in a row
	if(!lucky3 && upgradeScript.itemList[0].upgrade == upgradeScript.itemList[1].upgrade && upgradeScript.itemList[1].upgrade == upgradeScript.itemList[2].upgrade)
	{
		//popup 3-in-a-row
		Debug.Log("3 in a ROWWW!");
		//gives the upgrade for free
		LuckItem();
		lucky3 = true;
	}
}

function LuckMoney ()
{
	Game1SaveData.totalGold += 777;
	audio.Play();
	sound.audio.Play();
	var popUp = Instantiate(textPref, Camera.main.transform.position + Vector3(0, -2, 1), transform.rotation);
	popUp.transform.localScale *= 0.85;
	popUp.GetComponent(TextMesh).text = "2 in a ROW!" + "\n" + "$777";
	popUp.renderer.material.color = Color(1, 0.75, 0.75);
	
	Camera.main.GetComponent(Bloom).bloomIntensity = 1.5;
	Camera.main.GetComponent(ScreenOverlay).intensity = 1.5;
}

function LuckItem ()
{
	upgradeScript.upgradesList[upgradeScript.itemList[0].upgrade].level += 1;
	upgradeScript.itemList[0].bought = true;
	upgradeScript.itemList[1].bought = true;
	upgradeScript.itemList[2].bought = true;
	audio.Play();
	sound.audio.Play();
	var popUp = Instantiate(textPref, Camera.main.transform.position + Vector3(0, -2, 1), transform.rotation);
	popUp.transform.localScale *= 0.85;
	popUp.GetComponent(TextMesh).text = "3 IN A ROW!" + "\n" + "FREE ITEM";
	popUp.renderer.material.color = Color(1, 0.75, 0.75);

	Camera.main.GetComponent(Bloom).bloomIntensity = 2;
	Camera.main.GetComponent(ScreenOverlay).intensity = 2.5;
}