#pragma strict
import System.Collections.Generic;

var upgradeScript: Game1SaveData;

var number: int;
var upgrade: Upgrade;
var hover: boolean;

var target: GameObject;
var init: boolean;

function Start ()
{

}

function Update ()
{
	if(!init)
	{
		if(upgrade.active)
		{
			target = GameObject.Find("Equip Display").GetComponent(EquipDisplay).activeButtons[0];
		}
		else
		{
			target = GameObject.Find("Equip Display").GetComponent(EquipDisplay).passiveButtons[0];
		}
	}
	
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	
	//changes sprite to show
	renderer.material.mainTexture = upgrade.icon;
	
	if(!hover)
	{
		if(upgrade.active)
		{
			if(upgrade.equipped)
			{
				renderer.material.color = Color(0,0.4,1);
			}
			else
			{
				renderer.material.color = Color(0.5, 0.5, 0.5);
			}
		}
		else
		{
			if(upgrade.equipped)
			{
				renderer.material.color = Color(0.8,0.2,0);
			}
			else
			{
				renderer.material.color = Color(0.2, 0.6, 1);
			}
		
		}
	}
	else
	{
		if(upgrade.active)
		{
			renderer.material.color = Color(0.2,0.5,1);
		}
		else
		{
			renderer.material.color = Color(1, 0.5, 0.2);
		}
	}
}

function OnMouseDown()
{
	if(upgrade.active && upgradeScript.activeEquip.Count <= 1)
	{
		if(!upgradeScript.activeEquip.Contains(upgrade) && upgradeScript.activeEquip.Count < 1)
		{
			upgradeScript.activeEquip.Add(upgrade);
			upgradeScript.upgradesList[number].equipped = true;
		}
		else if(upgradeScript.activeEquip.Contains(upgrade))
		{
			upgradeScript.activeEquip.Remove(upgrade);
			upgradeScript.upgradesList[number].equipped = false;
		}
		else
		{
			target.SendMessage("OnMouseDown");
			Invoke("Press", 0.05);
		}
	}

	if(!upgrade.active && upgradeScript.passiveEquip.Count <= 3)
	{
		if(!upgradeScript.passiveEquip.Contains(upgrade) && upgradeScript.passiveEquip.Count < 3)
		{
			upgradeScript.passiveEquip.Add(upgrade);
			upgradeScript.upgradesList[number].equipped = true;
		}	
		else if(upgradeScript.passiveEquip.Contains(upgrade))
		{
			upgradeScript.passiveEquip.Remove(upgrade);
			upgradeScript.upgradesList[number].equipped = false;
		}
		else
		{
			target.SendMessage("OnMouseDown");
			Invoke("Press", 0.05);
		}
	}	
}

function Press()
{
	if(upgrade.active)
	{
		upgradeScript.activeEquip.Add(upgrade);
	}
	else
	{
		upgradeScript.passiveEquip.Add(upgrade);
	}
	upgradeScript.upgradesList[number].equipped = true;
}

function OnMouseEnter()
{
	hover = true;
	EquipSelect.equipHover = upgrade;
}

function OnMouseExit()
{
	hover = false;
	EquipSelect.equipHover = null;
}