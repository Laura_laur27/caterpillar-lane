#pragma strict

function Start () {

}

function Update () {
	//reverts bloom back to normal
	GetComponent(Bloom).bloomIntensity = Mathf.Lerp(GetComponent(Bloom).bloomIntensity, 0.11, 0.015);
	GetComponent(Bloom).bloomThreshhold = Mathf.Lerp(GetComponent(Bloom).bloomThreshhold, 0.58, 0.015);

	//resets overlay
	GetComponent(ScreenOverlay).intensity = Mathf.Lerp(GetComponent(ScreenOverlay).intensity, 0.56, 0.01);
}