#pragma strict

var level: int;
var quit: boolean;
var menu: boolean;

function Start ()
{

}

function Update ()
{

}

function OnMouseEnter()
{
	if(menu)
	{
		renderer.material.color = Color(0.2,0.8,1,1);
	}
}

function OnMouseDown()
{
	if(menu)
	{
		renderer.material.color = Color(1,0.2,0.2,1);
	}
}

function OnMouseExit()
{
	if(menu)
	{
		renderer.material.color = Color(1,1,1,1);
	}	
}

function OnMouseUpAsButton()
{
	if(!quit)
	{
		Application.LoadLevel(level);
	}
}

function Press()
{
	Application.Quit();
	Screen.fullScreen = false;
}