#pragma strict

var timer: float; timer = 0;

function Start ()
{

}

function Update ()
{
	if(timer > 0)
	{
		timer -= Time.deltaTime;
	}
	if(timer <= 0)
	{
		renderer.material.color.a = 0;
	}
	else
	{
		renderer.material.color.a = Mathf.Lerp(0,0.66, timer/6);
	}
}