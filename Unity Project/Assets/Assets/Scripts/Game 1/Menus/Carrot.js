#pragma strict

var counter: int; counter = 0;

function Start ()
{

}

function Update ()
{
	if(counter >= 4)
	{
		Invoke("Glitch", 2);
		//GameObject.Find("Exit Button").GetComponent(Button).enabled = false;
		//GameObject.Find("Exit Button").GetComponent(ButtonMenu).enabled = false;
		GameObject.Find("Exit Button").collider.enabled = false;
	}
}

function OnMouseDown()
{
	counter ++;
}

function OnMouseExit ()
{
	counter = 0;
}

function Glitch()
{
	Instantiate(Resources.Load("Camera Screen Out Pre"), Vector3(0,0,-20), transform.rotation);
	Destroy(GameObject.FindGameObjectWithTag("Steam"));
	Destroy(gameObject);
}