#pragma strict
import System.Collections.Generic;

var upgradeScript: Game1SaveData;


var number: int;
var upgrade: Upgrade;
var unlocked: boolean;
var hover: boolean;

var init: boolean;

function Start ()
{

}

function Update ()
{
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");

	//changes sprite to show
	if(unlocked && !(upgrade.name == "Empty"))
	{
		renderer.material.mainTexture = upgrade.icon;
		renderer.material.color.a = 1;
		if(hover)
		{
			renderer.material.color = Color.yellow;
		}
		else
		{
			renderer.material.color = Color(0.9,0.8,0.6);
		}
	}
	else
	{
		//renderer.material.mainTexture = null;
		renderer.material.color.a = 0;
	}
	
	if(!init)
	{
		EquipSelect.equipHover = upgrade;
		init = true;
	}
}

function OnMouseEnter ()
{
	if(upgrade.name != "Empty")
	{
		hover = true;
		EquipSelect.equipHover = upgrade;
	}
}

function OnMouseExit ()
{
	hover = false;
	EquipSelect.equipHover = null;
}


function OnMouseDown()
{
	if(upgrade.blurb != null)
	{
		if(upgradeScript.activeEquip.Contains(upgrade))
		{
			upgradeScript.upgradesList[upgrade.num].equipped = false;
			upgradeScript.activeEquip.Remove(upgrade);
		}
		if(upgradeScript.passiveEquip.Contains(upgrade))
		{
			upgradeScript.upgradesList[upgrade.num].equipped = false;
			upgradeScript.passiveEquip.Remove(upgrade);
		}
	}
}