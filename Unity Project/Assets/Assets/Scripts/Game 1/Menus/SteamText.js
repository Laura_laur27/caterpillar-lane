#pragma strict

var timer: float; timer = 0;

function Start ()
{
	//renderer.material.color = Color(1,0.9,0.75);
}

function Update ()
{
	if(timer > 0)
	{
		timer -= Time.deltaTime;
	}
	if(timer <= 0)
	{
		renderer.material.color.a = 0;
	}
	else
	{
		renderer.material.color.a = timer/3;
	}
}