#pragma strict
import System.Collections.Generic;

var scriptSave: Game1SaveData; scriptSave = GameObject.Find("Game 1 Save").GetComponent(Game1SaveData);
var popList : List.<int>; popList = new List.<int>();
var popArray: int[];

var graphColor: Color;

var cornerBot: GameObject; cornerBot = GameObject.Find("Graph Corner Bot");
var cornerTop: GameObject; cornerTop = GameObject.Find("Graph Corner Top");
var lowL: Vector3;
var lowR: Vector3;
var topL: Vector3;
var topR: Vector3;
var height: float;
var width: float;

var xList: float[];
var yList: float[];

var num: int;
var max: float;

var init: boolean;
var count: int;

function Start ()
{
	//does poplist
	popList = scriptSave.popList;
	// clears poplist so next time its...clear - DO ON EXIT BUTTON;
	
	lowL = cornerBot.transform.position;
	lowR = Vector3(cornerTop.transform.position.x, cornerBot.transform.position.y, 0);
	topL = cornerTop.transform.position;
	topR = Vector3(cornerBot.transform.position.x, cornerTop.transform.position.y, 0);
	height = cornerTop.transform.position.y - cornerBot.transform.position.y;
	width = cornerTop.transform.position.x - cornerBot.transform.position.x;
	
	num = popList.Count;
	
	max = 0;
	for(number in popList)
	{
		if(number > max)
		{
			max = number;
		}
	}
	GameObject.Find("Text Max").GetComponent(TextMesh).text = max.ToString();
	GameObject.Find("Text Max").GetComponent(TextMesh).renderer.material.color = graphColor;
	GameObject.Find("Text Min").GetComponent(TextMesh).renderer.material.color = graphColor;
	
	count = 0;
}

function Update ()
{
	if(!init)
	{
		DrawGraph();
		
	}
}

function DrawGraph ()
{
	//creates array containing x positions
	xList = new float[num];
	for(var i = 0; i<num; i++)
	{
		xList[i] = lowL.x + i*width/(num-1);
	}
	
	//creates array containing y positions
	yList = new float[num];
	for(i = 0; i<num; i++)
	{
		yList[i] = lowL.y + (popList[i]/max)*height;
	}
	
	InvokeRepeating("Draw",0.1, 0.1);
	
	init = true;
}

function Draw ()
{
	if(count < num)
	{
		Instantiate(Resources.Load("Graph Blip"), Vector3(xList[count], yList[count], 0), transform.rotation);
		count ++;
	}
	if(count > 0 && count < num)
	{
		var target: Vector3; target = Vector3(xList[count-1], yList[count-1], 1);
		var origin: Vector3; origin = Vector3(xList[count], yList[count], 1);
		
		var line: GameObject;
		line = Instantiate(Resources.Load("Graph Line"), Vector3((xList[count-1] + xList[count])/2, (yList[count-1] + yList[count])/2, 1), transform.rotation);

		line.transform.eulerAngles.z = Vector3.Angle(Vector3(0,1,0), (target-origin));
		
		//stretches based on vector
		line.transform.localScale.y = (target-origin).magnitude;
	}
}