#pragma strict

var color: Color;
var change: boolean;

function Start ()
{
	if(change)
	{
		renderer.material.color = color;
	}
}

function Update ()
{
	renderer.material.mainTextureOffset.x += Time.deltaTime*0.03;
	renderer.material.mainTextureOffset.y -= Time.deltaTime*0.08;
}