#pragma strict

function Start () {
	Invoke("End", 8);
	renderer.material.color = Color.black;
}

function Update () {
	if(Time.timeSinceLevelLoad > 2)
	{
		renderer.material.color = Color.Lerp(renderer.material.color, Color(0.5,0.5,0.5), 0.0045);
	}
}

function End () {
	Debug.Log("END");
	Application.Quit();
	Screen.fullScreen = false;
}