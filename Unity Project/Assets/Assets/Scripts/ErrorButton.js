#pragma strict

var erHover: Texture2D;
var erNorm: Texture2D;
var erClick: Texture2D;

function OnMouseEnter ()
{
	guiTexture.texture = erHover;
}

function OnMouseExit ()
{
	guiTexture.texture = erNorm;
}

function OnMouseDown ()
{
	guiTexture.texture = erClick;
}

function OnMouseUpAsButton ()
{
	guiTexture.texture = erNorm;
	GameObject.Find("Global Controller").SendMessage("Screenout", 6);
}