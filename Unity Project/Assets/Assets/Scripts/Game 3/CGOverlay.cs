using UnityEngine;
using System.Collections;

public class CGOverlay : MonoBehaviour {
	
	public float timer;
	
	public float timerMod;
	
	public float timerMax;
	
	// Use this for initialization
	void Start () {
		
		timer = 0.0f;
		timerMax = 0.4f;
		guiTexture.pixelInset = new Rect(-Screen.height*4.0f/6.0f, -Screen.height/2.0f, Screen.height*4.0f/3.0f+1, Screen.height+1);
	}
	
	// Update is called once per frame
	void Update () {
		if(timer>0.0f){
			timer -= Time.deltaTime;
		}
		
		//first half, 1-0.5
		if(timer>(timerMax/2)){
			guiTexture.color = Color.Lerp(new Color(0.5f,0.5f,0.5f,0.0f), new Color(0.5f,0.5f,0.5f,1.0f), (timer-timerMax/2)/(-timerMax/2) + 1);
			Debug.Log((timer-timerMax/2)/(-timerMax/2) + 1);
		}
		//second half, 0.5-0
		if(timer<timerMax/2 && timer>0.0f){
			guiTexture.color = Color.Lerp(new Color(0.5f,0.5f,0.5f,1.0f), new Color(0.5f,0.5f,0.5f,0.0f), timer/(-timerMax/2) + 1);
			Debug.Log(timer/(-timerMax/2) + 1);
		}
		if(Input.GetKeyDown(KeyCode.B)){
			Fade(0.4f);
		}
		
		
	}
	
	void Fade (float mod) {
		
		timerMax = mod;
		timer = timerMax;
	}
}