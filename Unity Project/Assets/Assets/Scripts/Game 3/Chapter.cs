using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct Chapter {

	public string[] rawPages;
	public List<Page> pages;
}