using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct Page {

	public AudioClip bgm;
	public Texture2D cg;
	public List<string> lines;
}