using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PagePrinter : MonoBehaviour {
	
	public int pageCounter;
	public int lineCounter;
	
	public int lineStartHeight;
	public int lineRows;
	
	public Chapter currChapter;
	
	public int nextLevel;
	
	public GameObject text;
	public GameObject textLine;
	
	public GameObject log;
	public GameObject logLine;
	public List<GameObject> logText;
	public int logStart;
	public int logHeight;
	
	public GameObject cgObject;
	public GameObject cgFade;

	private float timer;
	private bool firstLine;
	
	private bool changingPage;
	public Game3Controller controlScript;
	
	// Use this for initialization
	void Start () {
		
		int pageCounter = 0;
		int lineCounter = 0;
		lineStartHeight = 225;
		lineRows = 0;
		timer = 0;
		
		text = GameObject.Find("Text");
		textLine = Resources.Load("Text Line") as GameObject;
		
		log = GameObject.Find ("Log");
		logLine = Resources.Load("Log Line") as GameObject;
		logText = new List<GameObject>();
		logStart = -180;
		logHeight = 0;
		
		cgObject = GameObject.Find("CG") as GameObject;
		cgFade = GameObject.Find("CG Overlay") as GameObject;
		
		changingPage = false;
		controlScript = GameObject.Find("Controller3").GetComponent("Game3Controller") as Game3Controller;
	}
	
	// Update is called once per frame
	void Update () {
		
		//inits first line
		if(timer > 0.03 && !firstLine){
			NextLine ();
			log.active = false;
			firstLine = true;
		}
		else{
			timer += Time.deltaTime;
		}
		
		//next line with keydown
		if((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetMouseButtonDown(0)) && text.active && !changingPage) {

			//checks if end of chapter, if so, next scene
			if(pageCounter == currChapter.pages.Count-1 && lineCounter == currChapter.pages[pageCounter].lines.Count){
				NextScene ();
			}
			
			//if not end of chapter, print new line
			else if(!(lineCounter >= currChapter.pages[pageCounter].lines.Count)){
				NextLine ();
			}
			
			//checks if end of page, if so, next page
			else if(lineCounter >= currChapter.pages[pageCounter].lines.Count){
				NextPage ();
			}
		}
		//rapid next line with control
	}
	
	void NextLine () {
		
		//addes new line to text
		GameObject newText;
		newText = Instantiate(textLine, new Vector3(0.5f,0.5f,0.0f), Quaternion.identity) as GameObject;
		newText.transform.parent = text.transform;
		
		newText.guiText.pixelOffset = new Vector2(-300, lineStartHeight);
		newText.guiText.text = BreakLine(currChapter.pages[pageCounter].lines[lineCounter], 100);
		
		//adds new line to log
		GameObject newLogText;
		newLogText = Instantiate(logLine, new Vector3(0.5f,0.5f,0.0f), Quaternion.identity) as GameObject;
		newLogText.transform.parent = log.transform;
		logText.Add(newLogText);
		
		newLogText.guiText.pixelOffset = new Vector2(-300, logStart);
		newLogText.guiText.text = BreakLine(currChapter.pages[pageCounter].lines[lineCounter], 100);
		newLogText.guiText.material.color = new Color(0.9f, 0.9f, 0.7f, 0.8f);
		
		//depending on how many lines the text is, add x to linestart height
		lineStartHeight -= 30 + 15*lineRows;
		
		lineCounter ++;
		
		//moves all log text by line start height
		foreach(GameObject lText in logText){
			lText.guiText.pixelOffset = new Vector2(lText.guiText.pixelOffset.x, lText.guiText.pixelOffset.y) + new Vector2(0, 30 + 15*lineRows);
		}
		if(firstLine){
			logHeight += 30 + 15*lineRows;
			controlScript.logHeight = logHeight;
		}
	}
	
	void NextPage () {
		
		changingPage = true;
		controlScript.changing = true;
		
		//cleans text page
		GameObject[] destroyList = GameObject.FindGameObjectsWithTag("Text Line");
		foreach(GameObject destroyText in destroyList){
			Destroy(destroyText);
		}
		lineCounter = 0;
		lineStartHeight = 225;
		pageCounter ++;
		
		//checks if new cg
		if(currChapter.pages[pageCounter].cg == null){
			changingPage = false;
			controlScript.changing = false;
			NextLine ();
		}			
		
		else if(currChapter.pages[pageCounter].cg != cgObject.guiTexture.texture){
			cgFade.SendMessage("Fade", 1.5f);
			StartCoroutine(ChangePage());
		}
		else{
			changingPage = false;
			controlScript.changing = false;
			NextLine ();
		}
	}
	
	IEnumerator ChangePage () {
		
		yield return new WaitForSeconds (0.75f);
		cgObject.guiTexture.texture = currChapter.pages[pageCounter].cg;
		yield return new WaitForSeconds (0.75f);
		cgFade.guiTexture.texture = currChapter.pages[pageCounter].cg;
		changingPage = false;
		controlScript.changing = false;
		NextLine ();
	}
	
	void NextScene () {
		
		//loads next scene
		Application.LoadLevel(nextLevel);
	}
	
	public string BreakLine (string input, int length) {
		
		//resets linerows
		lineRows = 0;
		
		//splits string
		string[] words;
		string result; result = "";
		string line; line = "";
		words = input.Split(new string[] {" "}, System.StringSplitOptions.None);
		foreach(string word in words){
			string temp;
			temp = line + " " + word;
			if(temp.Length > length){
				result += line + "\n";
				line = word;
				lineRows ++;
			}
			else{
				line = temp;
			}
		}
		
		result += line;
		
		return result.Substring(1, result.Length-1);		
	}
		
}