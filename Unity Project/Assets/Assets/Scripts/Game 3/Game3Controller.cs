using UnityEngine;
using System.Collections;

public class Game3Controller : MonoBehaviour {
	
	public bool inLog;
	
	public GameObject log;
	public GameObject[] logText;
	public GameObject logBox;
	
	public GameObject text;
	public GameObject textBox;
	
	public GameObject overlay;

	public bool working;
	
	public int logHeight;
	public int scrollHeight;
	public float currScroll;
	
	public bool changing;
	
	// Use this for initialization
	void Start () {
		
		inLog = false;
		working = false;
		log = GameObject.Find("Log");
		logBox = GameObject.Find("Log Box");
		text = GameObject.Find("Text");
		textBox = GameObject.Find("Text Box");
		overlay = GameObject.Find("CG Overlay");
		currScroll = 0;
		logBox.active = false;
		changing = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!working && !changing){
			if(Input.GetMouseButtonDown(1) && !inLog){
				overlay.SendMessage("Fade", 0.4f);
				working = true;
				Invoke("OpenLog", 0.2f);
			}
			
			else if(Input.GetMouseButtonDown(1) && inLog){
				overlay.SendMessage("Fade", 0.4f);
				working = true;
				Invoke("CloseLog", 0.2f);
			}
		}
			
		if(inLog && logHeight > 400){
			Scroll ();
		}
	}
	
	void OpenLog () {
		
		//calculate ammount of scrolling allowed
		scrollHeight = logHeight - 350;
		
		text.active = false;
		textBox.active = false;
		log.active = true;
		logBox.active = true;
		logText = GameObject.FindGameObjectsWithTag("Log Line");
		inLog = true;
		working = false;
	}
	
	void CloseLog () {
		
		//resets position of log
		foreach(GameObject line in logText){
			line.guiText.pixelOffset += new Vector2(0, currScroll);
		}
		currScroll = 0;
		
		text.active = true;
		textBox.active = true;
		log.active = false;
		logBox.active = false;
		inLog = false;
		working = false;
	}
	
	void Scroll () {
		
		//allows the player to scroll through the log
		float scrollDir;
		scrollDir = 200.0f*Input.GetAxis("Mouse ScrollWheel");
		
		//checks to see if the scrollling is within limits
		if((currScroll < scrollHeight && scrollDir > 0) || (currScroll > 0 && scrollDir < 0)){
			currScroll += scrollDir;
			Vector2 addVector;
			addVector = new Vector2(0, scrollDir);
			foreach(GameObject line in logText){
				line.guiText.pixelOffset -= addVector;
			}
		}		
	}
}