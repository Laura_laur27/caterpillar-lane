using UnityEngine;
using System.Collections;
 
public class AutoType : MonoBehaviour {
 
	public float letterPause = 0.0002f;
 
	string message;
 
	// Use this for initialization
	void Start () {
		message = guiText.text;
		//guiText.text = "";
		//StartCoroutine(TypeText ());
		guiText.material.color = new Color(1.0f,1.0f,1.0f,0.4f);
	}

	IEnumerator TypeText () {
		foreach (char letter in message.ToCharArray()) {
			guiText.text += letter;
			yield return new WaitForSeconds (0);
		}      
	}

	void Update () {
		guiText.material.color = Color.Lerp(guiText.material.color, new Color(1.0f,1.0f,1.0f,1.0f), 0.05f);
	}
}