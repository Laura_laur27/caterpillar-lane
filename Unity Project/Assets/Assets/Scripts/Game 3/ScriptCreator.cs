using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScriptCreator : MonoBehaviour {
	
	//initialization
	private bool init;
	
	public string temp;
	public List<string> temp2;
	
	//string dump
	public string rawString;
	//whole chapter
	public Chapter chapter;
	
	void Awake () {
        //chapter = GetComponent<Chapter>();
	}
	
	// Use this for initialization
	void Start () {
		//rawPageList = new List<string>();
		chapter = new Chapter();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!init){
			//generate chapter variable from rawstring
			chapter = GenerateScript(rawString);
			//pass chapter onto printer script and log script
			PagePrinter printerScript;
			printerScript = gameObject.GetComponent("PagePrinter") as PagePrinter;
			printerScript.currChapter = chapter;
			//test
			temp = chapter.rawPages[1];
			init = true;
		}
	}
	
	//generates formatted script as a chapter
	public Chapter GenerateScript (string input) {
		
		//OUTPUT CHAPTER
		Chapter output = new Chapter();
		
		//breaks raw chapter string into raw pages
		//defines the rawpage list
		output.rawPages = input.Split(new string[] {"NEWPAGE: "}, System.StringSplitOptions.None);
		
		//create temporary list of pages to store
		List<Page> tempPages = new List<Page>();
		
		//for each of the rawpage list elements, convert into page class
		output.pages = new List<Page>();
		foreach (string rawPage in output.rawPages){
			
			//ignores empty pages
			if(rawPage == ""){
				continue;
			}
				
			//defines temporary page and variables
			Page tempPage;
			tempPage = new Page();
			//tempPage = gameObject.AddComponent(Page);
			string tempCg;
			string tempBGM;
			string tempText;
			
			//finds and receives the cg of the scene
			tempCg = rawPage.Split(new string[] {".CG "}, System.StringSplitOptions.None)[0];
			tempText = rawPage.Split(new string[] {".CG "}, System.StringSplitOptions.None)[1];
			tempPage.cg = (Texture2D) Resources.Load("CG" + tempCg, typeof(Texture2D));
			
			//finds and receives the audio of the scene
			tempBGM = tempText.Split(new string[] {".BGM"}, System.StringSplitOptions.None)[0];
			tempText = tempText.Split(new string[] {".BGM"}, System.StringSplitOptions.None)[1];
			tempPage.bgm = (AudioClip) Resources.Load("BGM" + tempBGM, typeof(AudioClip));
			
			//breaks the lines (sans cg + sound) into lines
			string[] lineHolder = tempText.Split(new string[] {"\r\n", "\n"}, System.StringSplitOptions.None);
			
			tempPage.lines = new List<string>();
			foreach (string line in lineHolder){
				//checks that line is not empty, if empty, ignore
				if(line != ""){
					tempPage.lines.Add(line);
				}
			}
			temp2 = tempPage.lines;
			
			//adds page to page list
			output.pages.Add(tempPage);
		}
		
		return output;
	}
}