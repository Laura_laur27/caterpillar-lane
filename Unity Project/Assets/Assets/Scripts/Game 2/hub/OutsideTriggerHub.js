#pragma strict

var trigger: boolean; trigger = false;

var IC: GameObject;
var IClights: GameObject;

var door1: GameObject;

var hub: GameObject;
var hubSound: GameObject;

function Start ()
{

}

function Update ()
{
	if(trigger)
	{
		Camera.main.GetComponent(DepthBlur).length = Mathf.Lerp(Camera.main.GetComponent(DepthBlur).length, 3, 0.005);
		Camera.main.GetComponent(DepthBlur).size = Mathf.Lerp(Camera.main.GetComponent(DepthBlur).size, 1.23, 0.005);
		Camera.main.audio.volume = Mathf.Lerp(Camera.main.audio.volume, 0, 0.01);
		hubSound.audio.volume = Mathf.Lerp(hubSound.audio.volume, 1, 0.005);
	}
}

function OnTriggerEnter (collider: Collider)
{
	if(collider.gameObject.tag == "Player")
	{
		if(!trigger)
		{
			//turns off old things
			IC.active = false;
			
			//turns on new doors
			door1.active = true;
			
			trigger = true;
		}
	}
}

function Trigger()
{
	IClights.active = false;
	hub.active = true;
			
	Camera.main.GetComponent(DepthBlur).length = 3;
	Camera.main.GetComponent(DepthBlur).size = 1.25;
}