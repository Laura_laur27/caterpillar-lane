#pragma strict

var reading: boolean;
var note: GameObject;
var overlay: GameObject;
var timer: float;

static var forceOn: boolean; forceOn = false;

var trigger: boolean; trigger = false;

function Start ()
{

}

function Update ()
{
	if(reading)
	{
		note.active = true;
		overlay.active = true;
		if(Input.GetMouseButtonUp(0) && !trigger)
		{
			trigger = true;
		}
		
		if(Input.GetMouseButton(0) && trigger && !forceOn)
		{
			reading = false;
			this.SendMessage("Resume");
		}
	}
	else
	{
		note.active = false;
		overlay.active = false;	
	}
	if(timer > 0)
	{
		timer -= Time.deltaTime;
	}
}

function Pause ()
{
	//Time.timeScale = 0;
	
	//sets movement and camera to 0	
	AudioListener.volume = 0.5;
}

function Resume ()
{
	//Time.timeScale = 1;
	AudioListener.volume = 1;
	trigger = false;
	timer = 0.1;
}