#pragma strict

var player: GameObject;
var pole: Transform; pole = transform.Find("S_StreetLight");

function Start ()
{

}

function Update ()
{
	if(!mirrorHandle.held)
	{
		if((player.transform.position - transform.position).magnitude <= 20)
		{
			light.intensity = Mathf.Lerp(light.intensity, 8, 0.07);
			pole.renderer.materials[1].color = Color.Lerp(pole.renderer.materials[1].color, Color(1,0.9,0.55), 0.07);
		}
		else
		{
			light.intensity = Mathf.Lerp(light.intensity, 0, 0.1);
			pole.renderer.materials[1].color = Color.Lerp(pole.renderer.materials[1].color, Color(0,0,0), 0.1);
		}
	}
	else
	{
		light.intensity = Mathf.Lerp(light.intensity, 8, 0.07);
		pole.renderer.materials[1].color = Color.Lerp(pole.renderer.materials[1].color, Color(1,0.9,0.55), 0.07);
	}
}