#pragma strict

var follow: boolean;
var player: GameObject;

static var end: boolean;
static var held: boolean; held = false;

var barriers: GameObject;
var endBarrier: GameObject;
var setBarrier: boolean;
var distance: float;

var mirrorDragAudio: GameObject;
var oldPos: float;
var newPos: float;

var sound: GameObject;

var timer: float;

function Start ()
{
	oldPos = transform.position.z;
	newPos = transform.position.z;
}

function Update ()
{
	if(true)
	{
		if(follow)
		{
			if(!setBarrier)
			{
			barriers.transform.position.x = player.transform.position.x;
			distance = transform.position.z - player.transform.position.z;
			setBarrier = true;
			}
			endBarrier.active = true;
			barriers.active = true;	
			
			if(transform.position.z >= -7.378042)
			{
				transform.position.z = -7.378042;
			}
			
			if(player.transform.position.z + distance < -7.378042)
			{
				transform.position.z = player.transform.position.z + distance;
			}
			else
			{
				transform.position.z = -7.378042;
			}
			
			if(Mathf.Abs(newPos - oldPos) > 0.01)
			{
				timer = 0.05;
			}
			else
			{
				timer -= Time.deltaTime;
			}
			if(timer > 0 && !mirrorDragAudio.audio.isPlaying)
			{
				mirrorDragAudio.audio.Play();
			}
			else if(timer <= 0)
			{
				mirrorDragAudio.audio.Pause();
			}
			
			oldPos = newPos;
			newPos = transform.position.z;
		}
		else
		{
			endBarrier.active = false;
			barriers.active = false;
			setBarrier = false;
		}
	}
	if(end)
	{
		mirrorDragAudio.audio.Pause();
	}
	
	if(transform.position.z <= -54.3 && !end)
	{
		follow = false;
		end = true;
		transform.position.z = -54.31;
		sound.audio.Play();
	}
}

function Interact ()
{
	held = true;
	
	if(!end)
	{
		if(follow)
		{
			follow = false;
		}
		else
		{
			follow = true;
		}
	}
}

function PopUp ()
{
	if(!end)
	{
		if(follow)
		{
			Game2Var.textInteract.GetComponent(TextMesh).text = "Release";
		}
		else
		{
			Game2Var.textInteract.GetComponent(TextMesh).text = "Grab";
		}
	}
	else
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Stuck";
	}
}