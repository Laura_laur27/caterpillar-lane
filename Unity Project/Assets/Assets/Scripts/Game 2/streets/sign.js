#pragma strict

var num: int;

var player: GameObject;
var distance: float;

var newDist: float; newDist = -1;
var acc: float; acc = 0;

function Start ()
{
	if(num != 0)
	{
		renderer.material.mainTexture = Resources.Load("sign" + num);
	}
}

function Update ()
{
	if(!mirrorHandle.held)
	{
		distance = Mathf.Abs(transform.position.x - player.transform.position.x);
		
		if(distance <= 6.5)
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(1,1,1), 0.01);
		}
		else if (distance <= 19)
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(1,1,1), 0.01);
			//renderer.material.color = Color.Lerp(renderer.material.color, Color(0.66,0.66,0.66), 0.02);
		}
		else if (distance <= 30)
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(1,1,1), 0.02);
		}
		else
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(0.05,0.05,0.05), 0.01);
		}
	}
	else if (!mirrorHandle.end)
	{
		distance = Mathf.Abs(transform.position.x -8.5);
				
		if(distance <= newDist)
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(1,1,1), 0.03);		
		}		
		else
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(0.03,0.03,0.03), 0.01);
		}
	}
	else
	{
		distance = Mathf.Abs(transform.position.x -8.5);

		newDist += (3+acc)*Time.deltaTime;
		acc += 3.25*Time.deltaTime;
		
		if(distance <= newDist)
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(1,1,1), 0.03);		
		}
		else
		{
			renderer.material.color = Color.Lerp(renderer.material.color, Color(0.03,0.03,0.03), 0.01);
		}
	}
}