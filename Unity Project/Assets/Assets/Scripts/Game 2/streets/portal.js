#pragma strict

var arrival: Transform;

function Start () {
	arrival = transform.Find("Arrival");
}

function Update () {

}

function OnTriggerEnter (collider: Collider) {
	if(collider.gameObject.tag == "Player")
	{
		collider.gameObject.transform.position.x = arrival.position.x;
	}
}