#pragma strict

var init: boolean;
var back: GameObject;

var player: GameObject;

function Start ()
{

}

function Update ()
{
	if((player.transform.position - transform.position).magnitude < 60)
	{
		player.audio.volume = (player.transform.position - transform.position).magnitude/60 - 0.13;
	}
}

function Interact ()
{
	if(!init)
	{
		back.active = false;
		Examine.forceOn = true;
		Invoke("Out", 3.3);
		Invoke("Sound", 1.6);
		init = true;
	}
}

function Out ()
{
	GameObject.Find("Global Controller").SendMessage("Screenout", 7);
}

function Sound ()
{
	audio.Play();
}