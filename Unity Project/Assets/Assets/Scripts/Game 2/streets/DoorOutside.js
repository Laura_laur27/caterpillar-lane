#pragma strict

var physics: GameObject;
var reverse: GameObject;
var mirror: GameObject;

var trigger: GameObject;

function Start ()
{

}

function Update ()
{

}

function Interact ()
{
	if(Input.GetMouseButtonDown(0) && !GetComponent(Door).locked)
	{
		if(!physics.active)
		{
			physics.active = true;
		}
		if(!reverse.active)
		{
			reverse.active = true;
		}
		if(!mirror.active)
		{
			mirror.active = true;
		}
		trigger.SendMessage("Trigger");
	}
}