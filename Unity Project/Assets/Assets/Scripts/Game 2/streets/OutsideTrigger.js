#pragma strict

var trigger: boolean; trigger = false;

var currPlayer: GameObject;
var currCam: GameObject;
var newPlayer: GameObject;
var newCam: GameObject;
var save: GameObject;

var IC: GameObject;
var IClights: GameObject;

var outside1: GameObject;
var outside2: GameObject;
var door1: GameObject;
var door2: GameObject;


function Start ()
{

}

function Update ()
{
	if(trigger)
	{
		Camera.main.GetComponent(DepthBlur).length = Mathf.Lerp(Camera.main.GetComponent(DepthBlur).length, 3, 0.005);
		Camera.main.GetComponent(DepthBlur).size = Mathf.Lerp(Camera.main.GetComponent(DepthBlur).size, 1, 0.005);
	}
}

function OnTriggerEnter (collider: Collider)
{
	if(collider.gameObject.tag == "Player")
	{
		if(!trigger)
		{
			//turns off old things
			IC.active = false;
			
			//turns on new doors
			door1.active = true;
			door2.active = true;
			
			trigger = true;
		}
	}
}

function Trigger()
{
	IClights.active = false;
	
	//changes player + camera to new
	newPlayer.transform.position = currPlayer.transform.position;
	newPlayer.transform.rotation = currPlayer.transform.rotation;
	newCam.transform.position = currCam.transform.position;
	newCam.transform.rotation = currCam.transform.rotation;
	newCam.audio.time = currCam.audio.time;
	newCam.audio.volume = currCam.audio.volume;
	newPlayer.active = true;
	currPlayer.active = false;
	save.GetComponent(SavedTrigger).timer = 5.0f;
	
	Camera.main.GetComponent(DepthBlur).length = 3;
	Camera.main.GetComponent(DepthBlur).size = 1.25;
}