#pragma strict

var timer: float;

function Start ()
{
	GetComponent(TextMesh).renderer.material.color.a = 0;
}

function Update ()
{
	if(timer>=3)
	{
		GetComponent(TextMesh).renderer.material.color.a = 1;
	}
	else if(timer > 0 && timer < 3)
	{
		GetComponent(TextMesh).renderer.material.color.a = Mathf.Lerp(1, 0.0, 1-(timer/3));
	}
	else
	{
		GetComponent(TextMesh).renderer.material.color.a = 0;
	}
	
	if(timer > 0)
	{
		timer -= Time.deltaTime;
	}
}