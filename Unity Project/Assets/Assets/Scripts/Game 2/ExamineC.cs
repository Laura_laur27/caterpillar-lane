using UnityEngine;
using System.Collections;

public class ExamineC : MonoBehaviour {
	
	public GameObject goal;
	public vp_FPCamera goalScript;
	public vp_FPInput goalScript2;
		
	// Use this for initialization
	void Start () {
		goal = GameObject.Find("Player/Camera");
		goalScript = goal.GetComponent<vp_FPCamera>();
		goalScript2 = goal.transform.parent.GetComponent<vp_FPInput>();
		
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void Pause () {
		goal = GameObject.Find("Player/Camera");
		goalScript = goal.GetComponent<vp_FPCamera>();
		goalScript2 = goal.transform.parent.GetComponent<vp_FPInput>();
		goalScript.MouseSensitivity = new Vector2(0.0f, 0.0f);
		goalScript2.enabled = false;
	}
	void Resume () {
		goal = GameObject.Find("Player/Camera");
		goalScript = goal.GetComponent<vp_FPCamera>();
		goalScript2 = goal.transform.parent.GetComponent<vp_FPInput>();
		goalScript2.enabled = true;
		goalScript.MouseSensitivity = new Vector2(3.5f, 3.5f);
	}
}