#pragma strict

var openSize = 89.0f;

//CLASSROOM ONLY
var script: TicTacToe; script = GameObject.Find("Tic Tac Toe").GetComponent(TicTacToe);
var realRoomNum: int;
var roomNum: int;
var occupied: boolean;

var locked: boolean;
var open: boolean;
var startAngle: Vector3; 
var openAngle: Vector3;

var closeAction: boolean;
var openAction: boolean;

//sounds
var audioOpen: Transform; audioOpen = transform.Find("AudioOpen");
var audioClose: Transform; audioClose = transform.Find("AudioClose");
var audioLocked: Transform; audioLocked = transform.Find("AudioLocked");

function Start ()
{
	InitDoor ();
	startAngle = transform.parent.transform.localEulerAngles;
	openAngle = Vector3(startAngle.x, startAngle.y + openSize, startAngle.z);
	if(openAngle.y < 0)
	{
		startAngle.y += 359.5;
		transform.parent.transform.localEulerAngles.y = startAngle.y;
		openAngle.y = 359.5+openAngle.y;
	}
}

function FixedUpdate ()
{
	if(open)
	{
		Open();
	}
	if(!open)
	{
		Close();
	}
	if(realRoomNum == 4 || realRoomNum == 6)
	{
		locked = true;
	}
}

function InitDoor ()
{
	roomNum = realRoomNum;
	if(realRoomNum != 4 || realRoomNum != 6)
	{
		occupied = false;
	}
	else
	{
		occupied = true;
	}	
}

function Interact ()
{
	if(Input.GetMouseButtonDown(0) && locked)
	{
		Locked();
	}
	
	if(Input.GetMouseButtonDown(0) && !locked)
	{
		if(!open && !script.inRoom2)
		{
			open = true;
			audioOpen.audio.Play();
			//TIC TAC TOE
			script.currScript = this;
			script.OpenDoor();
		}
		else if(!open && script.inRoom2)
		{
			//TIC TAC TOE
			script.OpenDoorL();
		}
		else
		{
			open = false;
			audioClose.audio.Play();
		}
	}

}

function TeleOpen ()
{
	open = true;
	audioOpen.audio.Play();
}

function PopUp ()
{
	if(locked)
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Locked";
	}
	if(!locked && open)
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Close Door";
	}
	if(!locked && !open)
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Open Door";
	}
}

function Open ()
{
	transform.parent.transform.localEulerAngles = Vector3.Slerp(transform.parent.transform.localEulerAngles, openAngle, 0.05);
}

function Close ()
{
	transform.parent.transform.localEulerAngles = Vector3.Slerp(transform.parent.transform.localEulerAngles, startAngle, 0.055);
}

function Locked ()
{
	audioLocked.audio.Play();
}

function CloseBehind ()
{
	open = false;
	audioClose.audio.Play();
}