#pragma strict

var classroom: GameObject;
var lights: GameObject;
var door: GameObject;
var ticTacToe: GameObject;

var trigger: GameObject;

function Start ()
{

}

function Update ()
{

}

function Interact ()
{
	if(Input.GetMouseButtonDown(0) && !GetComponent(Door).locked)
	{
		if(!classroom.active)
		{
			classroom.active = true;
		}
		if(!lights.active)
		{
			lights.active = true;
		}
		if(!ticTacToe.active)
		{
			ticTacToe.active = true;
		}
		door.GetComponent(Door).locked = true;

		trigger.SendMessage("Trigger");
	}
}