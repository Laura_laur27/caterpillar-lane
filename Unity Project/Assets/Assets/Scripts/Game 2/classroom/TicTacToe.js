#pragma strict
import System.Collections.Generic;

var won: boolean; won = false;
var lost: boolean; won = false;

//stores in-world objects
var inRoom: boolean;
var inRoom2: boolean;

var corridorMaster: GameObject; corridorMaster = GameObject.Find("Corridor Master");
var roomMaster: GameObject; roomMaster = GameObject.Find("Classroom Master");
var rooms = new GameObject[5];
var roomLights: GameObject;
var doors = new GameObject[9];

var doorBlock1: GameObject;
var doorBlock2: GameObject;

var roof: GameObject;

//marks
var marks = new GameObject[9];
var circle: Material;
var cross: Material;

var roomTurn: int;

//stores current room
var currRoom : GameObject;
var currScript : ClassroomDoor;
var exitRoom : int;

//boolean to see if rooms have been initiated
var startRoom: boolean;

//contains sequence of moves
var moveList : List.<int>; moveList = new List.<int>();

//classroom dictionary
var roomList = new int[9]; //gives the altered identity of the room

var valueList1 = new int[9]; valueList1 = [1,2,3,4,5,6,7,8,9];
var valueList2 = new int[9]; valueList2 = [1,2,3,4,5,6,7,8,9];
var valueList3 = new int[9]; valueList3 = [3,2,1,6,5,4,9,8,7];
var valueList7 = new int[9]; valueList7 = [7,8,9,4,5,6,1,2,3];
var valueList8 = new int[9]; valueList8 = [7,8,9,4,5,6,1,2,3];
var valueList9 = new int[9]; valueList9 = [9,8,7,6,5,4,3,2,1];

//big ass list of choice trees
var choiceList : List.< List.<int> > = new List.< List.<int> >();
var currChoiceList : List.< List.<int> > = new List.< List.<int> >();

//raw intergers used to create choiceList
var intRaw = new int[180]; intRaw = [1,9,2,3,7,8,0,0,0,1,9,2,3,8,7,0,0,0,1,9,3,2,7,4,8,5,0,1,9,3,2,8,7,0,0,0,1,9,7,4,2,3,8,6,0,1,9,7,4,3,5,2,10,10,1,9,7,4,3,5,8,6,0,1,9,7,4,8,6,2,3,0,1,9,7,4,8,6,3,5,0,1,9,8,3,2,6,0,0,0,1,9,8,3,7,6,0,0,0,2,1,3,7,8,4,0,0,0,2,1,3,7,9,4,0,0,0,2,1,7,9,3,5,0,0,0,2,1,7,9,8,5,0,0,0,2,1,8,5,3,9,0,0,0,2,1,8,5,7,9,0,0,0,2,1,8,5,9,7,3,4,0,2,1,9,7,3,4,0,0,0,2,1,9,7,8,4,0,0,0];

function Start ()
{
	//creates choice list from intRaw
	currChoiceList.Clear();
	for(var i = 0; i<20; i++)
	{
		var tempList: List.<int> = new List.<int>();
		//adds 9 sequencial ints from raw int list into new list
		for(var i2 = 0; i2<9; i2++)
		{
			tempList.Add(intRaw[i2 + 9*i]);
		}
		currChoiceList.Add(tempList);
	}
	
	StartGame ();
}

function Update ()
{

}

function StartGame ()
{
	//resets choiceList
	currChoiceList.Clear();
	for(var i = 0; i<20; i++)
	{
		var tempList: List.<int> = new List.<int>();
		//adds 9 sequencial ints from raw int list into new list
		for(var i2 = 0; i2<9; i2++)
		{
			tempList.Add(intRaw[i2 + 9*i]);
		}
		currChoiceList.Add(tempList);
	}
	
	//no starter room yet
	startRoom = false;
	
	roomTurn = 0;
	
	//inits doors
	for(door in doors)
	{
		if(door != null)
		{
			door.SendMessage("InitDoor");
			door.GetComponent(ClassroomDoor).locked = false;
			door.GetComponent(ClassroomDoor).occupied = false;
		}
	}
	
	//resets marks
	for(mark in marks)
	{
		mark.renderer.material = cross;
		mark.active = false;
	}
	
	roomLights.GetComponent(CRLights).on = false;
}

function OpenDoor ()
{
	roomLights.GetComponent(CRLights).on = true;
	doorBlock1.active = false;
	doorBlock2.active = false;

	//if starting room, based on opening move, determines room configuration
	if(!startRoom)
	{
		if(currScript.roomNum == 1)
		{
			roomList = valueList1;
		}
		if(currScript.roomNum == 2)
		{
			roomList = valueList2;
		}
		if(currScript.roomNum == 3)
		{
			roomList = valueList3;
		}
		if(currScript.roomNum == 7)
		{
			roomList = valueList7;
		}
		if(currScript.roomNum == 8)
		{
			roomList = valueList8;
		}
		if(currScript.roomNum == 9)
		{
			roomList = valueList9;
		}
		
		//sets the room numbers of all the doors
		for(door in doors)
		{
			if(door != null)
			{
				var doorScript: ClassroomDoor;
				doorScript = door.GetComponent(ClassroomDoor);
				doorScript.roomNum = roomList[doorScript.realRoomNum-1];
			}
		}
		
		startRoom = true;
	}
	
	//is now in INROOM state
	inRoom = true;
	//temporarily locks all doors
	for(door in doors)
	{
		if(door != null)
		{
			door.GetComponent(ClassroomDoor).locked = true;
		}
	}
	
	var tempListHolder : List.< List.<int> > = new List.< List.<int> >();
	
	
	// AIIIIII
	
	//finds all lists that start with current room number, remove others
	for(var listNo = 0; listNo < currChoiceList.Count; listNo++)
	{
		if(currChoiceList[listNo][0] == currScript.roomNum)
		{
			tempListHolder.Add(currChoiceList[listNo]);
		}
	}
	currChoiceList = tempListHolder;
	if(currChoiceList[0][1] == 10 || currChoiceList[0][2] == 10)
	{
		//trigger win
		Win();
	}
	if(currChoiceList[0][2] == 0 && roomTurn != 3)
	{
		roomTurn = 3;
	}	
	//generates classroom
	GenerateClassroom();
}

function EnterRoom ()
{
	//closes door behind
	currScript.CloseBehind();

	//rooms are now occupied
	currScript.occupied = true;

	inRoom2 = true;
	
	//unlocks this door only
	currScript.locked = false;

	//draws scratches signifying taken areas
	marks[currScript.realRoomNum-1].active = true;
	marks[currScript.realRoomNum-1].renderer.material = circle;
	
	//prevents people from running through doors
	doorBlock1.active = true;
	doorBlock2.active = true;
	
	//turns doors into fake ones
	doors[3].GetComponent(twoWayDoors).usual = false;
	doors[5].GetComponent(twoWayDoors).usual = false;
}

function ExitRoom ()
{
	if(!won)
	{
		roomLights.GetComponent(CRLights).on = false;
		
		//prevents people from running through doors
		doorBlock1.active = true;
		doorBlock2.active = true;
	
		//not in rooms anymore
		inRoom = false;
		inRoom2 = false;
		
		Invoke("HideRooms", 1.2);
		
		//closes door behind
		currScript.CloseBehind();
		
		//rooms are now occupied
		if(!lost)
		{
			currScript.occupied = true;
		}
		else
		{
			lost = false;
		}
		
		//unlocks all unlockable doors, locks all occupied doors
		for(door in doors)
		{
			if(door != null && !door.GetComponent(ClassroomDoor).occupied)
			{
				door.GetComponent(ClassroomDoor).locked = false;
			}
			if(door != null && door.GetComponent(ClassroomDoor).occupied)
			{
				door.GetComponent(ClassroomDoor).locked = true;
			}
		}
	}
	else
	{
		roomLights.GetComponent(CRLights).on = false;
		currScript.gameObject.active = false;
		HideRooms();
		GameObject.Find("Wind").audio.Play();
	}

	//set currscript to null
	currScript = null;
	
	//turns doors into real ones
	doors[3].GetComponent(twoWayDoors).usual = true;
	doors[5].GetComponent(twoWayDoors).usual = true;
}

function HideRooms ()
{
	//hides rooms again
	for(room in rooms)
	{
		room.active = false;
	}	
}

function OpenDoorL ()
{
	if(!won)
	{
		//prevents people from running through doors
		doorBlock1.active = false;
		doorBlock2.active = false;

		if(currChoiceList[0][2] == 0)
		{
			//trigger loss
			Lose();
		}
	
		//determines ai room based on next number
		if(currChoiceList[0][1] == 5)
		{
			currScript.TeleOpen();
			marks[4].active = true;
		}
		else
		{
			GenerateCorridor ();
		}
	
		//removes starting two number for all elements in the list
		for(var listNo1 = 0; listNo1 < currChoiceList.Count; listNo1++)
		{
			currChoiceList[listNo1].RemoveAt(1);
			currChoiceList[listNo1].RemoveAt(0);
		}
	}
	else
	{
		currScript.TeleOpen();
		roof.active = true;
		currScript.gameObject.transform.parent.parent = roomMaster.transform;
		corridorMaster.active = false;
	}
}

function GenerateClassroom ()
{
	//turns the right one on
	for(room in rooms)
	{
		room.active = false;
	}
	if(!won)
	{
		rooms[roomTurn].active = true;
	}
	else
	{
		rooms[4].active = true;
	}
	
	//moves room to position
	roomMaster.GetComponent(ClassroomTransport).Transport(currScript.realRoomNum);
	
	//adds to turn of rooms
	roomTurn ++;
}

function GenerateCorridor ()
{
	var tempRoom: GameObject;
	//finds the room with corresponding number
	for(door in doors)
	{
		if(door != null && door.GetComponent(ClassroomDoor).roomNum == currChoiceList[0][1])
		{
			tempRoom = door;
		}
	}
	//moves corridor to position
	corridorMaster.GetComponent(CorridorTransport).Transport(tempRoom.GetComponent(ClassroomDoor).realRoomNum);
	
	//sets script to new door
	currScript = tempRoom.GetComponent(ClassroomDoor);
	
	//draws scratches signifying taken areas
	marks[currScript.realRoomNum-1].active = true;

	//opens the other door
	tempRoom.GetComponent(ClassroomDoor).TeleOpen();
}

function Lose ()
{
	GameObject.Find("Giggle").audio.Play();
	Invoke("StartGame", 0);
	lost = true;
}

function Win ()
{
	won = true;
}