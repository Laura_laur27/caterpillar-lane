#pragma strict

var corridor: GameObject;

var room1: Transform; room1 = transform.Find("ClassroomLoc1");
var room2: Transform; room2 = transform.Find("ClassroomLoc2");
var room3: Transform; room3 = transform.Find("ClassroomLoc3");
var room7: Transform; room7 = transform.Find("ClassroomLoc7");
var room8: Transform; room8 = transform.Find("ClassroomLoc8");
var room9: Transform; room9 = transform.Find("ClassroomLoc9");

var doors = new GameObject[9];

var room: GameObject;
var player: GameObject;

//sides right = 0, up = 1, left = 2, down = 3
var oldSide: int; oldSide = 0;
var newSide: int;

function Start ()
{
	
}

function Update ()
{
	if(Input.GetKeyDown("1"))
	{
		newSide = 2;
		Flip();
		FindRooms ();		
		corridor.transform.position -= (room1.position-room.transform.position);
	}
	if(Input.GetKeyDown("2"))
	{
		newSide = 2;
		Flip();
		FindRooms ();		
		corridor.transform.position -= (room2.position-room.transform.position);
	}
	if(Input.GetKeyDown("3"))
	{
		newSide = 2;
		Flip();
		FindRooms ();		
		corridor.transform.position -= (room3.position-room.transform.position);
	}
	if(Input.GetKeyDown("4"))
	{
		newSide = 1;
		Flip();
		FindRooms ();
		corridor.transform.position -= (room3.position-room.transform.position);
	}
	if(Input.GetKeyDown("6"))
	{
		newSide = 3;
		Flip();
		FindRooms ();
		corridor.transform.position -= (room3.position-room.transform.position);
	}
	if(Input.GetKeyDown("7"))
	{
		newSide = 0;
		Flip();
		FindRooms ();		
		corridor.transform.position -= (room7.position-room.transform.position);
	}
	if(Input.GetKeyDown("8"))
	{
		newSide = 0;
		Flip();
		FindRooms ();		
		corridor.transform.position -= (room8.position-room.transform.position);
	}
	if(Input.GetKeyDown("9"))
	{
		newSide = 0;
		Flip();
		FindRooms ();		
		corridor.transform.position -= (room9.position-room.transform.position);
	}
}

function Flip ()
{
	var angle: int;
	
	//based on old and newsides, calculates degree of flipping
	if(oldSide == 0)
	{
		if(newSide == 0)
		{
			angle = 0;
		}
		if(newSide == 1)
		{
			angle = 90;
		}
		if(newSide == 2)
		{
			angle = 180;
		}
		if(newSide == 3)
		{
			angle = -90;
		}
	}	
	if(oldSide == 1)
	{
		if(newSide == 0)
		{
			angle = -90;
		}
		if(newSide == 1)
		{
			angle = 0;
		}
		if(newSide == 2)
		{
			angle = 90;
		}
		if(newSide == 3)
		{
			angle = 180;
		}
	}	
	if(oldSide == 2)
	{
		if(newSide == 0)
		{
			angle = 180;
		}
		if(newSide == 1)
		{
			angle = -90;
		}
		if(newSide == 2)
		{
			angle = 0;
		}
		if(newSide == 3)
		{
			angle = 90;
		}
	}	
	if(oldSide == 3)
	{
		if(newSide == 0)
		{
			angle = 90;
		}
		if(newSide == 1)
		{
			angle = 180;
		}
		if(newSide == 3)
		{
			angle = -90;
		}
		if(newSide == 3)
		{
			angle = 0;
		}
	}	
	corridor.transform.Rotate(Vector3(0,angle,0));
	oldSide = newSide;
}


function FindRooms ()
{
	room1 = transform.Find("ClassroomLoc1");
	room2 = transform.Find("ClassroomLoc2");
	room3 = transform.Find("ClassroomLoc3");
	room7 = transform.Find("ClassroomLoc7");
	room8 = transform.Find("ClassroomLoc8");
	room9 = transform.Find("ClassroomLoc9");
}