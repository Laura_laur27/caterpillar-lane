#pragma strict

var on: boolean;

var blob: GameObject;
var dir: GameObject;
var omni: GameObject;

function Start ()
{

}

function Update ()
{
	if(on)
	{
		blob.light.intensity = Mathf.Lerp(blob.light.intensity, 1.8, 0.07);
		dir.light.intensity = Mathf.Lerp(dir.light.intensity, 0.5, 0.07);
		omni.light.intensity = Mathf.Lerp(omni.light.intensity, 0.1, 0.07);
	}
	else
	{
		blob.light.intensity = Mathf.Lerp(blob.light.intensity, 0, 0.07);
		dir.light.intensity = Mathf.Lerp(dir.light.intensity, 0, 0.07);
		omni.light.intensity = Mathf.Lerp(omni.light.intensity, 0.33, 0.07);	
	}
}