#pragma strict

var lockList = new boolean[9];
var doorList = new GameObject[9];

function Start ()
{

}

function Update ()
{

}

function Init ()
{
	lockList = [false,false,false,true,true,true,false,false,false];
	DoorUpdate();
}

function DoorUpdate ()
{
	for(var i = 0; i<9; i++)
	{
		if(doorList[i] != null)
		{
			doorList[i].GetComponent(ClassroomDoor).locked = lockList[i];
		}
	}
}