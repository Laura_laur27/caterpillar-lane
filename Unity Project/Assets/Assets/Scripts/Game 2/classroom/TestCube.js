#pragma strict

var script: TicTacToe; script = GameObject.Find("Tic Tac Toe").GetComponent(TicTacToe);

var realRoomNum: int;
var roomNum: int;

var occupied: boolean;

function Start () {
	roomNum = realRoomNum;
}

function Update () {

}

function OnMouseEnter () {
	script.currRoom = gameObject;
}

function OnMouseExit () {
	script.currRoom = null;
}

function OnMouseUpAsButton () {
	if(roomNum != 4 && roomNum != 5 && roomNum != 6 && !occupied){
		script.EnterRoom();
		occupied = true;
		renderer.material.color = Color.green;
	}
}

function EnemyOccupy () {
	occupied = true;
	renderer.material.color = Color.red;
}