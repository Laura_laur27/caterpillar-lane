#pragma strict

var reading: boolean;
var examineCam: GameObject; examineCam = GameObject.Find("Camera Examine");
var played: boolean;
var quiet: boolean; 

function Start ()
{
	quiet = false;
}

function Update ()
{
	if(quiet)
	{
		audio.volume = Mathf.Lerp(audio.volume, 0, 0.1);
	}
}

function Interact ()
{
	if(!examineCam.GetComponent(Examine).reading && examineCam.GetComponent(Examine).timer <= 0 && gameObject.layer == 0)
	{
		if(!played)
		{
			audio.Play();
			var noteList = GameObject.FindGameObjectsWithTag("Interactable");
			for(singleNote in noteList)
			{
				if(singleNote.GetComponent(notes) != null && singleNote.GetComponent(notes).played)
				{
					singleNote.GetComponent(notes).quiet = true;
				}
			}
			
			played = true;
		}
		examineCam.GetComponent(Examine).reading = true;
		examineCam.GetComponent(Examine).note.renderer.material.mainTexture = renderer.material.mainTexture;
		//examineCam.GetComponent(Examine).Pause();
		examineCam.SendMessage("Pause");
	}
}

function PopUp ()
{
	if(!examineCam.GetComponent(Examine).reading && examineCam.GetComponent(Examine).timer <= 0 && gameObject.layer == 0)
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Read";
	}
}