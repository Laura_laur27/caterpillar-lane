#pragma strict

var objects: GameObject[];

function Start ()
{
	for(object in objects)
	{
		object.AddComponent(InteractGlow);
	}
}

function Update ()
{
	objects = GameObject.FindGameObjectsWithTag("Interactable");
}