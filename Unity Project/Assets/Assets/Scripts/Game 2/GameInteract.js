#pragma strict

static var objectInteract: GameObject;

var panorama: boolean;

function Start ()
{

}

function Update ()
{
	var ray: Ray = Camera.main.ScreenPointToRay(Vector3(camera.pixelWidth/2, camera.pixelHeight/2, 0));
	var hits: RaycastHit[];
	if(!panorama)
	{
		hits = Physics.RaycastAll(ray, 4);
	}
	else
	{
		hits = Physics.RaycastAll(ray, 50);
	}	
	var containsObject: boolean; containsObject = false;
	for(var i=0; i<hits.length; i++)
	{
		if(hits[i].collider.gameObject.tag == "Interactable")
		{
			objectInteract = hits[i].collider.gameObject;
			containsObject = true;
			break;
		}
	}
	if(!containsObject)
	{
		objectInteract = null;
	}
	
	if(Input.GetMouseButtonDown(0) && objectInteract != null)
	{
		objectInteract.SendMessage("Interact");
	}
	
	if(objectInteract != null)
	{
		PopUp();
	}
}

function PopUp ()
{
	objectInteract.SendMessage("PopUp");
}