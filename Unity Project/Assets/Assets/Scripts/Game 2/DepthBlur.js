#pragma strict

var distance: float;
var script: DepthOfFieldScatter; script = GetComponent(DepthOfFieldScatter);

var size: float; size = 0.88;
var length: float; length = 3.31;

function Start ()
{

}

function Update ()
{
	if(Input.GetMouseButton(1))
	{
		//casts ray to see if looking at close object
		var ray: Ray = Camera.main.ScreenPointToRay(Vector3(camera.pixelWidth/2, camera.pixelHeight/2, 0));
		var hit: RaycastHit;
		if (Physics.Raycast(ray, hit))
		{
			distance = 0;//hit.distance + 0.5;
		}
		else
		{
			distance = 8.5;
		}
		script.focalSize = 2;//distance*0.02 + 0.8;
		script.focalLength = distance;
	}
	else
	{
		script.focalSize = size;
		script.focalLength = length;
	}
}