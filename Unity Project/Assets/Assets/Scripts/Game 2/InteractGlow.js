#pragma strict

var timer: float; timer = 0;
var startColor: Color;

var child: GameObject;

function Start ()
{
	if(GetComponent(Renderer) != null)
	{
		startColor = renderer.material.color;
	}
	else
	{
		for(var obj : Transform in transform)
		{
			if(obj.GetComponent(MeshRenderer) != null)
			{
				child = obj.gameObject;
				startColor = child.renderer.material.color;
			}
		}
	}
}

function Update ()
{
	if(GetComponent(Renderer) != null)
	{
		if(timer > 0)
		{
			timer -= Time.deltaTime;
			renderer.material.color = Color.Lerp(renderer.material.color, Color(1,0.4,0), 0.13);
		}
		else
		{
			renderer.material.color = Color.Lerp(renderer.material.color, startColor, 0.15);
		}
	}
	else
	{
		if(timer > 0)
		{
			timer -= Time.deltaTime;
			child.renderer.material.color = Color.Lerp(child.renderer.material.color, Color(1,0.4,0), 0.13);
		}
		else
		{
			child.renderer.material.color = Color.Lerp(child.renderer.material.color, startColor, 0.15);
		}
	}
}

function PopUp ()
{
	timer = 0.1;
}