#pragma strict

static var init: boolean; init = false;

function Awake ()
{
	// Sets target fps to 60
	Application.targetFrameRate = 60;
	//DontDestroyOnLoad (transform.gameObject);
}

function Start ()
{
	//checks if game has been initialized before, if so load Game 1 Save
	if(PlayerPrefs.HasKey("Game1SaveDataString"))
	{
		var Game1SaveDataString: String; Game1SaveDataString = PlayerPrefs.GetString("Game1SaveDataString");
		var Game1SaveData: byte[]; Game1SaveData = System.Convert.FromBase64String(Game1SaveDataString);
		Game1SaveData.LoadObjectTree(null);
		init = true;
	}

	Application.LoadLevel(4);
}

function Update ()
{
	if(Input.GetKeyDown("0"))
	{
		Application.LoadLevel(0);
	}
	if(Input.GetKeyDown("9"))
	{
		Application.LoadLevel(1);
	}
	if(Input.GetKeyDown("8"))
	{
		Application.LoadLevel(2);
	}
}