///////////////////////////////////////////////////////////
// Component Preset Script
///////////////////////////////////////////////////////////

ComponentType vp_FPCamera
BobAmplitude 0 0.85 0 0.5
ShakeAmplitude 5 5 0
RenderingFieldOfView 35
ShakeSpeed 0.075
MouseSensitivity 0.75 0.75
MouseSmoothSteps 10
MouseAcceleration True
BobRate -0.5 1 0.5 0.5
