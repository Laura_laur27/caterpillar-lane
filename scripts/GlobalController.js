#pragma strict

function Start ()
{

}

function Update ()
{

}

function Screenout (level: int)
{
	//disables mouse clicking
	
	
	//creates screen out camera for effect
	Instantiate(Resources.Load("Camera Screen Out"), Camera.main.transform.position, Camera.main.transform.rotation);
	
	//after x seconds, loads new level
	StartCoroutine(LoadLevel(level));
}

function LoadLevel (level: int)
{
	yield WaitForSeconds(4);
	Application.LoadLevel(level);
}