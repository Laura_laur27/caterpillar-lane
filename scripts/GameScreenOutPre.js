#pragma strict

var timer: float;
var error: GameObject; error = GameObject.Find("Error");
var screen: GameObject; screen = GameObject.Find("White Screen");

function Start ()
{
	Invoke("Error", 6);
	InvokeRepeating("GlitchAudio", 0, 1.3);
	Invoke("ScreenOut", 10);
	Camera.main.audio.pitch = -0.6;
}

function Update ()
{
	timer += Time.deltaTime;
	if(timer >= 6)
	{
		//fades screen in
		screen.renderer.material.color.a = Mathf.Lerp(0, 0.5, (timer-6)/0.33);
		error.guiTexture.color.a = Mathf.Lerp(0, 1, (timer-6)/0.2);
	}
	else
	{
		GetComponent(ContrastEnhance).intensity = Mathf.Lerp(0, -10, timer/5);
		GetComponent(ScreenOverlay).intensity = Mathf.Lerp(0,0.85, timer/5);
		GetComponent(NoiseEffect).grainSize = Mathf.Lerp(0,0.5, timer/5);
	}
}

function Error ()
{
	Camera.main.audio.volume = 0;
	screen.audio.volume = 0;
	audio.Play();
	//sets cursor to new
	Cursor.SetCursor(Resources.Load("aero_arrow"), Vector2.zero, CursorMode.Auto);
}

function ScreenOut ()
{
	GameObject.Find("Global Controller").SendMessage("Screenout", 6);
}

function GlitchAudio ()
{
	Camera.main.audio.pitch = 2*(Random.value-0.5);
}