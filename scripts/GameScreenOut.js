#pragma strict

var goal: GameObject; goal = GameObject.Find("Player/Camera");
var seconds: float; seconds = 4;
var timer: float; timer = 0;
var startRot: Quaternion; startRot = transform.rotation;
var startPos: Vector3; startPos = transform.position;

function Start ()
{
	//goal.active = false;
	Invoke("ActivateCamera", 10);
}

function Update ()
{
	//
	timer += Time.deltaTime;
	//transforms camera
	transform.position.x = Mathf.Lerp(transform.position.x, goal.transform.position.x, Time.deltaTime/seconds);
	transform.position.y = Mathf.Lerp(transform.position.y, goal.transform.position.y, Time.deltaTime/seconds);
	transform.position.z = startPos.z + 1.5 - 0.0043*Mathf.Pow(timer, 3) + 0.0679*Mathf.Pow(timer, 2) - 0.1115*timer - 1.3868;
	transform.rotation = Quaternion.Slerp(startRot, goal.transform.rotation, timer/6);
	/*
	transform.rotation.x = Quaternion.Slerp(transform.rotation.x, goal.transform.rotation.x, Time.deltaTime/seconds);
	transform.rotation.y = Quaternion.Slerp(transform.rotation.y, goal.transform.rotation.y, Time.deltaTime/seconds);
	transform.rotation.z = Quaternion.Slerp(transform.rotation.z, goal.transform.rotation.z, Time.deltaTime/seconds);
	*/
	GetComponent(DepthOfFieldScatter).focalLength = Mathf.Lerp(GetComponent(DepthOfFieldScatter).focalLength, goal.GetComponent(DepthOfFieldScatter).focalLength, Time.deltaTime/5);
	GetComponent(DepthOfFieldScatter).focalSize = Mathf.Lerp(GetComponent(DepthOfFieldScatter).focalSize, goal.GetComponent(DepthOfFieldScatter).focalSize, Time.deltaTime/5);
	
}

function ActivateCamera ()
{
	//active = false;
}