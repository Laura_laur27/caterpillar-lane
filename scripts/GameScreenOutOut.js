#pragma strict

var timer: float; timer = 0;
var seconds: float; seconds = 0.13;
var screenOut: boolean; screenOut = false;

var cam2: GameObject; cam2 = GameObject.Find("Camera Screen Out 2");
var blindTop: GameObject; blindTop = GameObject.Find("Blind Top");
var blindBot: GameObject; blindBot = GameObject.Find("Blind Bot");

function Start ()
{
	Camera.main.audio.volume = 0;
	Screen.showCursor = false;
	//audio.Play();
}

function Update ()
{
	//counts
	timer += Time.deltaTime;
	
	//adds noise
	GetComponent(NoiseEffect).grainSize = Mathf.Lerp(0, 2, timer/seconds);
	//screen overlay
	GetComponent(ScreenOverlay).intensity = Mathf.Lerp(0, 5, timer/seconds);
	//chroma
	GetComponent(Vignetting).chromaticAberration = Mathf.Lerp(0, 100, timer/seconds);
	
	
	//Cam 2
	//fisheyes
	cam2.GetComponent(Fisheye).strengthX = Mathf.Lerp(0, 0.6, timer/seconds);
	cam2.GetComponent(Fisheye).strengthY = Mathf.Lerp(0, 0.5, timer/seconds);
	//sun shaft
	if(blindTop.transform.localScale.x < 34.5)
	{
		cam2.GetComponent(SunShafts).sunShaftIntensity = Mathf.Lerp(0, 4, timer/seconds);
	}
	else
	{
		cam2.GetComponent(SunShafts).sunShaftIntensity = 0;
	}
	//vignetting
	cam2.GetComponent(Vignetting).intensity = Mathf.Lerp(0, 7, timer/seconds);
	cam2.GetComponent(Vignetting).blur = Mathf.Lerp(0, 4, timer/seconds);

	//closes blinds
	blindTop.transform.localScale.x = Mathf.Lerp(11, 35, timer/seconds);//-106.67*timer*timer + 101.33*timer + 11;
	blindBot.transform.localScale.x = Mathf.Lerp(11, 35, timer/seconds);//blindTop.transform.localScale.x;
	
	//fades out
	if(timer > seconds && !screenOut)
	{
		cam2.GetComponent(CameraFade).fadeOut(1);
		screenOut = true;
	}
}