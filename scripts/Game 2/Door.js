#pragma strict

var openSize = 110.0f;

var locked: boolean;
var open: boolean;
var startAngle: Vector3; 
var openAngle: Vector3;

var closeAction: boolean;
var openAction: boolean;

//sounds
var audioOpen: Transform; audioOpen = transform.Find("AudioOpen");
var audioClose: Transform; audioClose = transform.Find("AudioClose");
var audioLocked: Transform; audioLocked = transform.Find("AudioLocked");

function Start ()
{
	startAngle = transform.parent.transform.eulerAngles;
	openAngle = Vector3(startAngle.x, startAngle.y + openSize, startAngle.z);
	if(openAngle.y < 0)
	{
		startAngle.y += 359.5;
		transform.parent.transform.eulerAngles.y = startAngle.y;
		openAngle.y = 359.5+openAngle.y;
	}
}

function Update ()
{
	if(open && !locked)
	{
		Open();
	}
	if(!open && !locked)
	{
		Close();
	}
}

function Interact ()
{
	if(Input.GetMouseButtonDown(0) && !locked)
	{
		open = !open;
		if(open)
		{
			audioOpen.audio.Play();
		}
		else
		{
			audioClose.audio.Play();
		}
	}
	if(Input.GetMouseButtonDown(0) && locked)
	{
		Locked();
	}
}

function PopUp ()
{
	if(locked)
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Locked";
	}
	if(!locked && open)
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Close Door";
	}
	if(!locked && !open)
	{
		Game2Var.textInteract.GetComponent(TextMesh).text = "Open Door";
	}
}

function Open ()
{
	transform.parent.transform.eulerAngles = Vector3.Slerp(transform.parent.transform.eulerAngles, openAngle, 0.05);
}

function Close ()
{
	transform.parent.transform.eulerAngles = Vector3.Slerp(transform.parent.transform.eulerAngles, startAngle, 0.055);
}

function Locked ()
{
	audioLocked.audio.Play();
}