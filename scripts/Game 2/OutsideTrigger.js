#pragma strict

var trigger: boolean; trigger = false;

function Start ()
{

}

function Update ()
{
	if(trigger)
	{
		Camera.main.SendMessage("fadeOut", (1.0/40.0));
		Camera.main.audio.volume -= Time.deltaTime/20;
		Camera.main.GetComponent(DepthBlur).length = Mathf.Lerp(Camera.main.GetComponent(DepthBlur).length, 8, 0.005);
		Camera.main.GetComponent(DepthBlur).size = Mathf.Lerp(Camera.main.GetComponent(DepthBlur).size, 1.5, 0.005);
	}
}

function OnTriggerEnter (collider: Collider)
{
	if(collider.gameObject.tag == "Player")
	{
		trigger = true;
		Invoke("Die", 21);
	}
}

function Die ()
{
	Application.Quit();
	Screen.fullScreen = false;
}