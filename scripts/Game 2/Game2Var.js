#pragma strict

static var textInteract: GameObject;
var timer: float;

//var screenInit: boolean;

function Awake ()
{
	// Make the game run as fast as possible in the web player
	Application.targetFrameRate = 60;
}
	
function Start ()
{
	textInteract = GameObject.Find("Camera GUI/Text Interact");
	Screen.showCursor = false;
	
	Camera.main.audio.volume = 0;
}

function Update ()
{
	timer += Time.deltaTime;
	
	//slowly increases volume after start
	if(timer > 3 && timer < 15 && Camera.main.audio.volume < 0.99)
	{
		Camera.main.audio.volume = Mathf.Lerp(0, 1, (timer-3)/10);
	}
}