#pragma strict

var timer: float; timer = 0;

var shift: boolean; shift = false;
var c: boolean; c = false;
var rmb: boolean; rmb = false;

function Start ()
{
	renderer.material.color.a = 0;
}

function Update ()
{
	timer += Time.deltaTime;
	
	if(timer > 10 && !(rmb && shift && c))
	{
		if(renderer.material.color.a < 1)
		{
			renderer.material.color.a += 0.02;
		}
		if(Input.GetKeyDown("c"))
		{
			c = true;
		}
		if(Input.GetKeyDown(KeyCode.LeftShift))
		{
			shift = true;
		}
		if(Input.GetMouseButtonDown(1))
		{
			rmb = true;
		}	
	}
	if(rmb && shift && c)
	{
		renderer.material.color.a -= 0.02;
		Invoke("Die", 2);
	}
}

function Die ()
{
	Destroy(gameObject);
}