#pragma strict

var target = float;

function Start ()
{

}

function Update ()
{
	if(GameInteract.objectInteract == null)
	{
		GetComponent(TextMesh).renderer.material.color.a = Mathf.Lerp(GetComponent(TextMesh).renderer.material.color.a, 0, 0.23);
		GetComponent(TextMesh).text = null;
	}
	else
	{
		GetComponent(TextMesh).renderer.material.color.a = Mathf.Lerp(GetComponent(TextMesh).renderer.material.color.a, 0.6, 0.1);
	}
}