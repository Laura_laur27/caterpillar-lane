#pragma strict

static var objectInteract: GameObject;

function Start ()
{

}

function Update ()
{
	var ray: Ray = Camera.main.ScreenPointToRay(Vector3(camera.pixelWidth/2, camera.pixelHeight/2, 0));
	var hits: RaycastHit[];
	hits = Physics.RaycastAll(ray, 4);
	var containsObject: boolean; containsObject = false;
	for(var i=0; i<hits.length; i++)
	{
		if(hits[i].collider.gameObject.tag == "Interactable")
		{
			objectInteract = hits[i].collider.gameObject;
			containsObject = true;
		}
	}
	if(!containsObject)
	{
		objectInteract = null;
	}
	
	if(Input.GetMouseButtonDown(0) && objectInteract != null)
	{
		objectInteract.SendMessage("Interact");
	}
	
	if(objectInteract != null)
	{
		PopUp();
	}
}

function PopUp()
{
	objectInteract.SendMessage("PopUp");
}