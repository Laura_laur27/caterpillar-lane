using UnityEngine;
using System.Collections;

public class GameScreemOutC : MonoBehaviour {
	
	public GameObject goal;
	public vp_FPCamera goalScript;
	public vp_FPInput goalScript2;
	public bool init;
	public float timer;
	
	
	// Use this for initialization
	void Start () {
		init = false;
		timer = 10.0f;
		goal = GameObject.Find("Player/Camera");
		goalScript = goal.GetComponent<vp_FPCamera>();
		goalScript2 = goal.transform.parent.GetComponent<vp_FPInput>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!init){
			goalScript.MouseSensitivity = new Vector2(0.0f, 0.0f);
			goalScript2.enabled = false;
			init = true;
		}
		//slowly enables movement
		goalScript.MouseSensitivity = Vector2.Lerp(goalScript.MouseSensitivity, new Vector2(0.5f,0.5f), Time.deltaTime/4);
		
		if(timer>0){
			timer -= Time.deltaTime;
		}
		else{
			goalScript2.enabled = true;
			goalScript.MouseSensitivity = new Vector2(3.5f, 3.5f);
			active = false;
		}

	}
}