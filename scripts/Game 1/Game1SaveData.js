#pragma strict
import System.Collections.Generic;

//INIT
var init: boolean; init = false;

//special parameters
static var rounds: int;
var friended: boolean; friended = false;

//constant game parameters
static var totalGold: int;
static var totalDistance: float;

static var nextDistance: float;
static var stockIteration: int; stockIteration = 0;
static var stockNew: boolean; stockNew = true;

//game over parameters
static var lastGold: int;
static var lastDistance: float;
var popList : List.<int>; popList = new List.<int>();
static var housesInvaded: int;
var itemList: List.<StoreItem>; itemList = new List.<StoreItem>();

//contains list of currently equipped skills
var activeEquip : List.<Upgrade>; activeEquip = new List.<Upgrade>(); //contains list of actives
var passiveEquip : List.<Upgrade>; passiveEquip = new List.<Upgrade>(); //contains list of passives

//for creating upgrade list
var nameList = new String[19];
var blurbList = new String[19];
var blurbLvlList = new String[19];
var activeList = new boolean[19];

//for storing upgrades
class Upgrade
{
	//static params
	var num: int;
	var name: String;
	var icon: Texture;
	var blurb: String;
	var blurbLvl: String;
	var active: boolean;
	var slot: int;
	
	//dynamic params
	var equipped: boolean;
	var level: int;
}
//for storing store items
class StoreItem
{
	var num: int;
	var upgrade: int;
	var level: int;
	var bought: boolean;
	var cost: int;
}

var upgradesList = new Upgrade[19];

function Awake ()
{
	DontDestroyOnLoad (transform.gameObject);
}

function Start ()
{
	//lists
	nameList = ['Mercenary','March Hare','The Heat is On','Infestation','Timewarp','Undo','Invader','Titanfall','Furry Fury','Conscription','Adventurous','Hour of Destiny','Déjà vu','Rabbits Foot','Timewalk','Hot-rods','Fleet of Foot','Vanguard','Empty'];
	blurbList = ['ACTIVE HOLD: Summons rabbits to invade the selected house, based on current gold. ','ACTIVE INSTANT: All rabbits on the map instantly travel towards target house. ','ACTIVE HOLD: Dramatically increases breeding speed and attack, and speed of all rabbits when held. ','ACTIVE INSTANT: Target house explodes into rabbit nest, sending rabbits in all directions, causing chain effect. ','ACTIVE HOLD: Slows time when the active button is held. ','ACTIVE INSTANT: Instantly rewinds the screen back to a previous position. ','Slightly refills power gauge whenever a house is invaded. ','Occupied Titans now damage houses they pass through. ','Upon death, rabbits explode, damaging nearby houses. ','Increases the starting number of rabbits. ','Rabbits can now breed outside. ','Upon reaching critical numbers, slows scrolling. ','Grants chance to send a second wave of rabbits with each command. ','Increases chance of encountering titans, houses, and powerups. ','Grants chance to instantly teleport rabbit to position. ','Gives rabbits a burning aura, damaging nearby houses as they pass. ','Rabbits are more resilient to slowing effects. ','Passively grants increased damage and defences to rabbits towards the front of the screen.',''];
	blurbLvlList = ['Levels increase number of rabbits summoned.','Levels decrease gauge cost.','Levels increase stat gains.','Levels increase explosion power.','Levels decrease gauge drain rate.','Levels decrease gauge cost.','Levels increase gauge gain.','Levels increase damage.','Levels increase damage and range','Levels increase starting number.','Levels increase breeding rate.','Levels increase slow.','Levels increase activation chance.','Levels further increases chance of appearance.','Levels increase activation chance.','Levels increase aura damage.','Levels increase resilience.','Levels increase stat bonuses.',''];
	activeList = [true,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false];
	
	//initializes constants		
	for(var i = 0; i < upgradesList.length; i++)
	{
		upgradesList[i].num = i;
		upgradesList[i].name = nameList[i];
		upgradesList[i].icon = Resources.Load("Icon" + (i));
		upgradesList[i].blurb = blurbList[i];
		upgradesList[i].blurbLvl = blurbLvlList[i];
		upgradesList[i].active = activeList[i];
	}
	if(!init)
	{
		for(var i2 = 0; i2 < upgradesList.length; i2++)
		{
			upgradesList[i2].slot = 0;
			upgradesList[i2].equipped = false;
			upgradesList[i2].level = 0;//Random.Range(0,2);
		}
		totalGold = 0;
		totalDistance = 0;
		upgradesList = upgradesList;
		init = true;
		nextDistance = 500;
	}
}

function Update ()
{
	//testing only
	if(Input.GetMouseButtonDown(0) && ((Application.loadedLevel == 2) || (Application.loadedLevel == 3) || (Application.loadedLevel == 5)))
	{
		SaveGame1 ();
	}
	
	if(!Screen.fullScreen)
	{
		Screen.fullScreen = true;
	}
	
	//on the 5th round, initiate friend
	if(rounds == 4 && !friended)
	{
		Invoke("Friend", 3.5);
		friended = true;
	}
}

function SaveGame1 ()
{
	var Game1SaveData: byte[]; Game1SaveData = LevelSerializer.SaveObjectTree(gameObject);
	var Game1SaveDataString: String; Game1SaveDataString = System.Convert.ToBase64String(Game1SaveData);
	PlayerPrefs.SetString("Game1SaveDataString", Game1SaveDataString);
}

function Friend ()
{
	//after a while, do pop-up
	Instantiate(Resources.Load("Steam PopUp"), Vector3(0.5,0.5,0), Quaternion.Euler(0,0,0));
	//after a while, text appears
	Instantiate(Resources.Load("Steam Text Spawner"), Vector3(0,0,0), Quaternion.Euler(0,0,0));	
}