#pragma strict

var script: ShopControl; script = GameObject.Find("Shop Controller").GetComponent(ShopControl);
var canBuy: boolean; canBuy = false;

var hover: boolean; hover = false;

function Start ()
{

}

function Update ()
{
	if(!hover)
	{
		if(canBuy)
		{
			renderer.material.color = Color(1,1,0,0.25);
		}
		else
		{
			renderer.material.color = Color(0,0,0,0.25);
		}
	}
}

function OnMouseEnter()
{
	if(canBuy)
	{
		renderer.material.color = Color(1,1,1,0.25);
	}
	hover = true;
}

function OnMouseDown()
{
	if(canBuy)
	{
		renderer.material.color = Color(1,0,0,0.25);
	}
}

function OnMouseExit()
{
	hover = false;
}

function OnMouseUpAsButton()
{
	script.Buy();
}