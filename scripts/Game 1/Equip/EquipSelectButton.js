#pragma strict
import System.Collections.Generic;

var upgradeScript: Game1SaveData;

var number: int;
var upgrade: Upgrade;
var hover: boolean;

function Start ()
{

}

function Update ()
{
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	
	//changes sprite to show
	renderer.material.mainTexture = upgrade.icon;
	
	if(!hover)
	{
		if(upgrade.active)
		{
			if(upgrade.equipped)
			{
				renderer.material.color = Color.red;
			}
			else
			{
				renderer.material.color = Color(0.6, 0.5, 0.5);
			}
		}
		else
		{
			if(upgrade.equipped)
			{
				renderer.material.color = Color.green;
			}
			else
			{
				renderer.material.color = Color(0.5, 0.6, 0.5);
			}
		
		}
	}
	else
	{
		renderer.material.color = Color.yellow;
	}
}

function OnMouseDown()
{
	if(upgrade.active && upgradeScript.activeEquip.Count <= 1)
	{
		if(!upgradeScript.activeEquip.Contains(upgrade) && upgradeScript.activeEquip.Count < 1)
		{
			upgradeScript.activeEquip.Add(upgrade);
			upgradeScript.upgradesList[number].equipped = true;
		}
		else if(upgradeScript.activeEquip.Contains(upgrade))
		{
			upgradeScript.activeEquip.Remove(upgrade);
			upgradeScript.upgradesList[number].equipped = false;
		}
	}
	if(!upgrade.active && upgradeScript.passiveEquip.Count <= 3)
	{
		if(!upgradeScript.passiveEquip.Contains(upgrade) && upgradeScript.passiveEquip.Count < 3)
		{
			upgradeScript.passiveEquip.Add(upgrade);
			upgradeScript.upgradesList[number].equipped = true;
		}	
		else if(upgradeScript.passiveEquip.Contains(upgrade))
		{
			upgradeScript.passiveEquip.Remove(upgrade);
			upgradeScript.upgradesList[number].equipped = false;
		}
	}	
}

function OnMouseEnter()
{
	hover = true;
	EquipSelect.equipHover = upgrade;
}

function OnMouseExit()
{
	hover = false;
	EquipSelect.equipHover = null;
}