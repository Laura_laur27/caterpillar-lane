#pragma strict

var number: int;
var upgrade: Upgrade;
var unlocked: boolean;
var hover: boolean;

function Start ()
{

}

function Update ()
{
	//changes sprite to show
	if(unlocked && !(upgrade.name == "Empty"))
	{
		renderer.material.mainTexture = upgrade.icon;
		renderer.material.color.a = 1;
		if(hover)
		{
			renderer.material.color = Color.yellow;
		}
		else
		{
			renderer.material.color = Color(0.9,0.8,0.6);
		}
	}
	else
	{
		//renderer.material.mainTexture = null;
		renderer.material.color.a = 0;
	}
}

function OnMouseEnter ()
{
	if(upgrade.name != "Empty")
	{
		hover = true;
		EquipSelect.equipHover = upgrade;
	}
}

function OnMouseExit ()
{
	hover = false;
	EquipSelect.equipHover = null;
}