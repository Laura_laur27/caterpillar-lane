#pragma strict
import System.Collections.Generic;

//prefabs
var prefabButton: GameObject;

var activeButtons = new GameObject[1];
var passiveButtons = new GameObject[3];
var upgradeScript: Game1SaveData; 

function Start ()
{
	//updates list
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	
	//generates location vectors, and buttons
	var currX = transform.position.x;
	var currY = transform.position.y;
	var newVector: Vector3;	newVector = Vector3(0,0,0);
	for(var i = 0; i<1; i++)
	{
		newVector.x = currX;
		newVector.y = currY;
		
		activeButtons[i] = Instantiate(prefabButton, newVector, Quaternion.Euler(90,180,0));
		activeButtons[i].transform.parent = transform;
		activeButtons[i].GetComponent(EquipDisplayButton).number = i;
		if(i < 1)
		{
			activeButtons[i].GetComponent(EquipDisplayButton).unlocked = true;
		}
	}
	currX = transform.position.x + 5.74;
	for(var i2 = 0; i2<3; i2++)
	{
		newVector.x = currX;
		newVector.y = currY;
		
		passiveButtons[i2] = Instantiate(prefabButton, newVector, Quaternion.Euler(90,180,0));
		passiveButtons[i2].transform.parent = transform;
		passiveButtons[i2].GetComponent(EquipDisplayButton).number = i2+3;
		if(i2 < 3)
		{
			passiveButtons[i2].GetComponent(EquipDisplayButton).unlocked = true;
		}		
		currX += 4.14;
	}
}

function Update ()
{
	//updates list
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	
	//changes upgrade of buttons to reflect current status
	for(var i = 0; i<upgradeScript.activeEquip.Count; i++)
	{
		activeButtons[i].GetComponent(EquipDisplayButton).upgrade = upgradeScript.activeEquip[i];		
	}
	for(var i2 = 0; i2<upgradeScript.passiveEquip.Count; i2++)
	{
		passiveButtons[i2].GetComponent(EquipDisplayButton).upgrade = upgradeScript.passiveEquip[i2];
	}
	//sets others to null
	for(var i3 = upgradeScript.activeEquip.Count; i3 < 1; i3++)
	{
		activeButtons[i3].GetComponent(EquipDisplayButton).upgrade = upgradeScript.upgradesList[18];
	}
	for(var i4 = upgradeScript.passiveEquip.Count; i4 < 3; i4++)
	{
		passiveButtons[i4].GetComponent(EquipDisplayButton).upgrade = upgradeScript.upgradesList[18];
	}
}