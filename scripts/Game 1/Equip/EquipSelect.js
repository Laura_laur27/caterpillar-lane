#pragma strict
import System.Collections.Generic;

//prefabs
var prefabButton: GameObject;

static var equipHover: Upgrade; equipHover = null;

var buttons = new GameObject[18];

var upgradeScript: Game1SaveData;
var upgradesList = new Upgrade[18];

var textName: GameObject; textName = GameObject.Find("Text Name");
var textBlurb: GameObject; textBlurb = GameObject.Find("Text Blurb");
var textLevel: GameObject; textLevel = GameObject.Find("Text Level");
var displayIcon: GameObject; displayIcon = GameObject.Find("Display Icon");

function Start ()
{
	//generates location vectors, and buttons
	var currX = transform.position.x;
	var currY = transform.position.y;
	var newVector: Vector3;	newVector = Vector3(0,0,0);
	for(var i = 0; i<18; i++)
	{
		newVector.x = currX;
		newVector.y = currY;
		
		buttons[i] = Instantiate(prefabButton, newVector, Quaternion.Euler(90,180,0));
		buttons[i].transform.parent = transform;
		buttons[i].GetComponent(EquipSelectButton).number = i;
		
		if(currY > transform.position.y -4)
		{
			currX += 3.91;
		}
		if(currY < transform.position.y -4)
		{
			currX += 3.27;
		}
		if((currX)> 3.91*2+transform.position.x+0.1 && currY > transform.position.y -1)//end of first row
		{
			currX = transform.position.x;
			currY -= 3.8;
		}
		if((currX)> 3.91*2+transform.position.x+0.1 && currY < transform.position.y -1 && currY > transform.position.y -4)//end of second row
		{
			currX = transform.position.x -1.106988;
			currY -= 5.16;
		}
		if((currX)> 3.27*3+transform.position.x-1.106988+0.1 && currY < transform.position.y -4)//end of third row
		{
			currX = transform.position.x -1.106988;
			currY -= 3.57;
		}
	}
	
	equipHover = null;
	
	//colors the texts
	textName.renderer.material.color = Color(1,0.8,0.5);
	textLevel.renderer.material.color = Color(1,0.8,0.5);
	textBlurb.renderer.material.color = Color(1,0.6,0.6);
}

function Update ()
{
	//updates list
	upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
	upgradesList = upgradeScript.upgradesList;
	
	for(var i = 0; i<18; i++)
	{
		if(upgradesList[i].level > 0)
		{
			buttons[i].GetComponent(EquipSelectButton).upgrade = upgradesList[i];
		}
		else
		{
			Destroy(buttons[i]);
		}
	}
	
	//changes text
	if(equipHover != null)
	{
		textName.GetComponent(TextMesh).text = equipHover.name;
	}
	else
	{
		textName.GetComponent(TextMesh).text = "Upgrade";
	}
	if(equipHover != null)
	{
		textBlurb.GetComponent(TextMesh).text = BreakLine(equipHover.blurb, 33) + "\n" + "\n" + BreakLine(equipHover.blurbLvl, 33);
	}
	else
	{
		textBlurb.GetComponent(TextMesh).text = "Ability Description";
	}
	if(equipHover != null)
	{
		textLevel.GetComponent(TextMesh).text = equipHover.level.ToString();
	}
	else
	{
		textLevel.GetComponent(TextMesh).text = "";
	}
	if(equipHover == null)
	{
		displayIcon.renderer.material.mainTexture = Resources.Load("Icon18");
		displayIcon.renderer.material.color.a = 1;
	}
	else
	{
		displayIcon.renderer.material.mainTexture = equipHover.icon;
		displayIcon.renderer.material.color.a = 1;
	}
}

function BreakLine (input: String, length: int): String
{
	//splits string
	var words: String[];
	var result: String; result = "";
	var line: String; line = "";
	words = input.Split(" "[0]);
	for(word in words)
	{
		var temp: String;
		temp = line + " " + word;
		if(temp.Length > length)
		{
			result += line + "\n";
			line = word;
		}
		else
		{
			line = temp;
		}
	}
	
	result += line;
	
	return result.Substring(1,result.Length-1);
}