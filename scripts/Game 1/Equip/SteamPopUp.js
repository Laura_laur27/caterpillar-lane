#pragma strict

var finalPosX: int;
var finalPosY: int;

var play: boolean;
var timer: float; timer = 0;

function Awake ()
{
	DontDestroyOnLoad (transform.gameObject);
}

function Start ()
{
	finalPosX = ((Screen.width/2)-250);
	finalPosY = -(Screen.height/2);
	guiTexture.pixelInset.x = finalPosX;
	guiTexture.pixelInset.y = finalPosY-74;
}

function Update ()
{
	timer+= Time.deltaTime;
	
	if(timer < 2)
	{
	guiTexture.pixelInset.y = Mathf.Lerp(guiTexture.pixelInset.y, finalPosY, timer/2);
	}
	
	if(timer > 0 && !play)
	{
		audio.Play();
		play = true;
	}
	
	if(timer > 4.5)
	{
	guiTexture.pixelInset.y = Mathf.Lerp(guiTexture.pixelInset.y, finalPosY-74, (timer-4.5)/4);
	guiTexture.color.a -= 0.04;
	}
	
	if(timer > 10)
	{
		Destroy(gameObject);
	}
}