#pragma strict

var moneyText: GameObject; moneyText = Resources.Load("Text Money");

function Start ()
{

}

function Update ()
{
	transform.position = Camera.main.transform.position;
}

function GainGold (sender: GameObject, gold: int)
{
	Game1Var.currGold += gold/10;

	if(sender.tag == "House" || sender.tag == "Rabbit" || sender.tag == "Token" || sender.tag == "Powerup")
	{
		var scoreText = Instantiate(moneyText, sender.transform.position + Vector3(0,2,-4), Quaternion.Euler(0, 0, 0));
		scoreText.GetComponent(TextMesh).text = "+$" + Mathf.Round(gold/10).ToString();
	}
	if(sender.tag == "House")
	{
		Game1Var.cameraFX.Burst(1.1);
		audio.Play();
	}
	if(sender.tag == "Token")
	{
		Game1Var.cameraFX.Burst(3.0);
		audio.Play();
	}
	if(sender.tag == "Powerup")
	{
		audio.volume = 0.7;
		audio.Play();
	}
	else
	{
		audio.volume = 1.0;
	}
}