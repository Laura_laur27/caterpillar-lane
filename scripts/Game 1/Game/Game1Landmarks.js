#pragma strict

//prefabs
var textPref: GameObject; textPref = Resources.Load("Text Popup");

var landmarks = new float[6]; landmarks = [25.0, 50.0, 100, 150, 200, 250];
var sting = new String[6]; sting = ["great!", "fantastic!", "awesome!", "excellent!", "magnificent!", "amazing!"];
var currLandmark: int; currLandmark = 0;


function Start ()
{

}

function Update ()
{
	if(currLandmark<6)
	{
		if(Game1Var.distanceTravelled > landmarks[currLandmark])
		{
			//PopUp ();
			currLandmark += 1;
		}
	}
}

function PopUp ()
{
	var popUp = Instantiate(textPref, Camera.main.transform.position + Vector3(0, 10, 1), transform.rotation);
	popUp.GetComponent(TextMesh).text = landmarks[currLandmark].ToString("F0") + "m - " + sting[currLandmark];
	popUp.renderer.material.color = Color(1, 1, 0.5);
	Game1Var.cameraFX.Burst(4.5);
	Game1Var.soundApplause.audio.Play();
	Game1Var.soundCheer.audio.Play();
}