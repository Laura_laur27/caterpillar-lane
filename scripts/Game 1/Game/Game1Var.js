#pragma strict
import System.Collections.Generic;

//UPGRADES LIST
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

//OBJECTS
static var garbageDestroyer: GameObject; garbageDestroyer = GameObject.Find("Garbage Destroyer");

//sounds
static var beat: float; beat = 0.42935510128;
static var soundApplause: GameObject; soundApplause = GameObject.Find("Main Camera/Sounds/Applause");
static var soundCheer: GameObject; soundCheer = GameObject.Find("Main Camera/Sounds/Cheer");
static var soundTimewalk: GameObject; soundTimewalk = GameObject.Find("Main Camera/Sounds/Timewalk");
static var soundDejavu: GameObject; soundDejavu = GameObject.Find("Main Camera/Sounds/Dejavu");

//CONTROLLER!
var objectHover: GameObject;
var objectSelect = new List.<GameObject>();

//states
static var isGameOver: boolean;

//parameters
static var totalPop: int;
static var externalPop: int;
var popList : List.<int>; popList = new List.<int>(); popList.Add(0);
var internalPop: int;
static var breedingSpeed: float;
static var currGold: int;
static var gauge: float;
static var distanceTravelled: float;
static var housesInvaded: int; housesInvaded = 0;

//scripts
static var moneyControl: MoneyController; moneyControl = GameObject.Find("Money Controller").GetComponent(MoneyController);
static var levelupControl: LevelUpController; levelupControl = GameObject.Find("Levelup Controller").GetComponent(LevelUpController);
static var powerupControl: PowerupController; powerupControl = GameObject.Find("Powerup Controller").GetComponent(PowerupController);
static var cameraFX: CameraFX; cameraFX = Camera.main.GetComponent(CameraFX);

//powerups
static var berserk: float;
static var invulnerable: float;
static var haste: float;
static var rabidityLvl: int;
static var virilityLvl: int;
static var rushLvl: int;

//prefabs
static var prefabRabbit: GameObject; prefabRabbit = Resources.Load("Rabbit Base");
static var prefabPowerup: GameObject; prefabPowerup = Resources.Load("Powerup Base");
static var prefabHouse: GameObject; prefabHouse = Resources.Load("House Base");
static var prefabToken: GameObject; prefabToken = Resources.Load("Token Base");
static var prefabHazard: GameObject; prefabHazard = Resources.Load("Hazard Base");
static var cursorHover: GameObject; cursorHover = Resources.Load("Cursor Hover");
static var cursorTarget: GameObject; cursorTarget = Resources.Load("Cursor Target");
static var explosionRabbit: GameObject;
static var explosionToken: GameObject;
static var summonRabbitgeddon: GameObject;
static var summonMother: GameObject;
static var summonHareicane: GameObject;

function Start ()
{
	
	//notifications
	NotificationCenter.DefaultCenter().AddObserver(this, "NewGameStart");
	NotificationCenter.DefaultCenter().AddObserver(this, "GameOver");
	NotificationCenter.DefaultCenter().AddObserver(this, "HouseInvaded");

	//initialises states & params
	externalPop = 0;
	
	//resets game variables
	totalPop = 0;
	currGold = 0;
	gauge = 100.0;
	distanceTravelled = 0;
	rabidityLvl = 0;
	virilityLvl = 0;
	rushLvl = 0;
	berserk = 0;
	invulnerable = 0;
	haste = 0;
	upgradesList = upgradeScript.upgradesList;
	isGameOver = false;
	objectHover = null;
	objectSelect.Clear();
	
	//adds pop to list every 5 seconds
	InvokeRepeating("AddPopList", 0, 5);
}

function Update ()
{
	Select();
	RabbitCount();
	
	//makes sure gauge is never negative or over 100
	if(gauge<0)
	{
		gauge = 0;
	}
	if(gauge>100)
	{
		gauge = 100;
	}
	
	//tracks powerup status
	if(berserk>0)
	{
		berserk -= Time.deltaTime;
	}
	else
	{
		berserk = 0;
	}
	if(invulnerable>0)
	{
		invulnerable -= Time.deltaTime;
	}
	else
	{
		invulnerable = 0;
	}
	if(haste>0)
	{
		haste -= Time.deltaTime;
	}
	else
	{
		haste = 0;
	}
	
	//tracks distance travelled
	distanceTravelled = Camera.main.transform.position.y;
}

function Select () //determines which houses are currently being selected
{
	//if object is house, adds object to selection after entering
 	if (objectHover != null && objectHover.tag == "House" && !objectSelect.Contains(objectHover) && Input.GetMouseButton(0) && objectSelect.Count < 6)
	{
		var scriptHouse: House = objectHover.GetComponent(House);
		if(scriptHouse.population >= 1)
		{
			objectSelect.Add(objectHover);
		}
	}
	if (Input.GetMouseButtonUp(0))
	{
		Invoke("UnSelect", 0.05);
	}
}

function UnSelect ()
{
	objectSelect.Clear();
}

function RabbitCount ()
{
	internalPop = 0;
	breedingSpeed = 0;
	var houses : GameObject[];
	houses = GameObject.FindGameObjectsWithTag("House");
	for (singlehouse in houses)
	{
		var script: House;
		script = singlehouse.GetComponent(House);
		internalPop += script.population;
		breedingSpeed += script.breedingSpeed;
	}
	totalPop = externalPop + internalPop;
}

function AddPopList ()
{
	popList.Add(totalPop);
}

function GameOver ()
{
	berserk = 0;
	invulnerable = 0;
	haste = 0;
	Game1SaveData.lastGold = currGold;
	Game1SaveData.lastDistance = distanceTravelled;
}