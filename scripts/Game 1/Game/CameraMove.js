#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

//controls camera movement
static var camVelocity: float;
var timer: float;
var count: int; count = 3;

function Start ()
{
	 timer = 0;
	 InvokeRepeating("Count", 1, 1);
}

function Update ()
{
	//Slowly moves the camera upwards
	transform.position.y += camVelocity*Time.deltaTime;
	
	//adds to timer
	timer += Time.deltaTime;
	
	//slowly increase velocity over time
	if(timer<4)
	{
		camVelocity = 0;
	}
	else if(timer>=4 && timer<40)
	{
		camVelocity = 0.025*timer + 1.2;
	}	
	else
	{
		camVelocity = 0.0125*timer + 1.2;
	}
	
	//hour of destiny
	if(upgradesList[11].equipped && Game1Var.totalPop < 100)
	{
		camVelocity *= (0.0006*upgradesList[11].level + 0.0062)*Game1Var.totalPop -0.06*upgradesList[11].level + 0.3;
	}
}

function Count ()
{
	if(count>=0)
	{
		var popUp: GameObject;
		popUp = Instantiate(Resources.Load("Text Popup"), Camera.main.transform.position + Vector3(0, 10, 1), transform.rotation);
		popUp.GetComponent(TextMesh).text = count.ToString();
		popUp.renderer.material.color = Color(1, 0.5, 0.8);
		popUp.transform.localScale.x *= 1.3;
		Game1Var.cameraFX.Burst(1.5);
		count --;
	}
}