#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

//Holds and calculates modified stats from upgrades

//dynamic parameters
static var modAttack: float; modAttack = 1;
static var modControlSize: float; modControlSize = 1;
static var modHealth: float; modHealth = 1;
static var modBreedRate: float; modBreedRate = 1;
static var modSpeed: float; modSpeed = 1;
static var modGaugeRate: float; modGaugeRate = 1;
static var modGoldRate: float; modGoldRate = 1;
static var modStartRate: float; modStartRate = 1;
static var modCC: float; modCC = 1;

//static parameters
static var modStartSize: float; modStartSize = 1;

//dynamic states
static var theHeatIsOn: boolean; theHeatIsOn = false;

function Start ()
{
	//notifications
	NotificationCenter.DefaultCenter().AddObserver(this, "TokenAcquired");

	//Resets mods to defaults
	modAttack = 1;
	modControlSize = 1;
	modHealth = 1;
	modBreedRate = 1;
	modSpeed = 1;
	modGaugeRate = 1;
	modGoldRate = 1;
	modGaugeRate = 1;
	modStartRate = 1;
	modCC = 1;
	
	//Checks what upgrades are what
	upgradesList = upgradeScript.upgradesList;
	
	//re-calculates certain mods at the beginning of each new game
	ControlSize ();
	GaugeRate ();
	GoldRate ();
	StartRate ();
	CC ();
}

function Update ()
{
	//constantly updating values
	Attack ();
	Health ();
	BreedRate ();
	Speed ();
	
	//testing purposes only
	upgradesList = upgradeScript.upgradesList;
}

function TokenAcquired (tokenType: int)
{
	//re-calculates certain mods at the acquisition of token
	ControlSize ();
}

function Attack ()
{
	var rabidMod: float; rabidMod = 1;
	var heatMod: float; heatMod = 1;
	
	if(Game1Var.rabidityLvl != 0)//attack
	{
		rabidMod = 0.725*Game1Var.rabidityLvl + 1.2;
	}
	if(theHeatIsOn)//the HEAT IS ON!!!
	{
		heatMod = 0.25*upgradesList[2].level + 1.75;
	}
	
	modAttack = rabidMod*heatMod;
	
	if(Game1Var.berserk > 0) // berserk bonus
	{
		modAttack = 1000;
	}
}

function ControlSize ()
{
	var rabidMod: float; rabidMod = 1;

		if(Game1Var.rabidityLvl != 0)//control
	{
		rabidMod = 0.725*Game1Var.rabidityLvl + 1.2;
	}
	modControlSize = rabidMod;
}

function Health ()
{
	var virilMod: float; virilMod = 1;

	if(Game1Var.virilityLvl != 0)//health
	{
		virilMod = 0.4175*Game1Var.virilityLvl + 0.9125;
	}

	modHealth = virilMod;
}

function BreedRate ()
{
	var virilMod: float; virilMod = 1;
	var heatMod: float; heatMod = 1;
	var viralMod: float; viralMod = 1;

	if(Game1Var.virilityLvl != 0)//breeding speed
	{
		virilMod = 0.4175*Game1Var.virilityLvl + 0.9125;
	}
	if(theHeatIsOn)//the HEAT IS ON!!!
	{
		heatMod = 0.3*upgradesList[2].level + 3.25;
	}
	if(Game1Var.invulnerable > 0) //invul viral bonus
	{
		viralMod = 2;
	}
	
	modBreedRate = virilMod*heatMod*viralMod;
}

function GaugeRate ()
{
	var rushMod: float; rushMod = 1;
	var hyperMod: float; hyperMod = 1;
	if(Game1Var.rushLvl != 0)//gauge
	{
		rushMod = 0.4175*Game1Var.rushLvl + 0.9125;
	}
	if(Game1Var.haste > 0) //haster hyperactive bonus
	{
		hyperMod = 2;
	}
	
	modGaugeRate = rushMod*hyperMod;
}

function GoldRate ()
{
	if(Game1Var.berserk > 0) //berserk rabid bonus
	{
		modGoldRate = 2;
	}
}

function Speed ()
{
	var rushMod: float; rushMod = 1;
	var heatMod: float; heatMod = 1;

	if(Game1Var.rushLvl != 0)//speed
	{
		rushMod = 0.4175*Game1Var.rushLvl + 0.9125;
	}
	if(theHeatIsOn)//the HEAT IS ON!!!
	{
		heatMod = 0.25*upgradesList[2].level + 1.75;
	}
	
	modSpeed = rushMod*heatMod;
	
	if(Game1Var.haste > 0)//haste
	{
		modSpeed *= 3;
	}
}

function StartRate ()
{
	if(upgradesList[9].equipped)
	{
		modStartRate = 0.5*upgradesList[9].level + 1;
	}
}

function CC ()
{
	if(upgradesList[16].equipped)
	{
		modCC = -0.2*upgradesList[16].level + 0.33;
	}
}