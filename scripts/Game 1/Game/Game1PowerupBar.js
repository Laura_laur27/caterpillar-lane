#pragma strict

var gauge: float;
var textR: Texture2D;
var textG: Texture2D;
var textB: Texture2D;
var text: GameObject; text = GameObject.Find("Text Powerup");
var glow: GameObject; glow = GameObject.Find("Powerup Glow");
var ticker: float;

function Start ()
{
	ticker = Game1Var.beat;
}

function Update ()
{
	gauge = Game1Var.berserk + Game1Var.invulnerable + Game1Var.haste;
	if(Game1Var.berserk > 0)
	{
		renderer.material.mainTexture = textR;
		text.GetComponent(TextMesh).text = "BERSERK";
		text.GetComponent(TextMesh).renderer.material.color = Color(1,0.8,0.8);
	}
	if(Game1Var.invulnerable > 0)
	{
		renderer.material.mainTexture = textB;
		text.GetComponent(TextMesh).text = "INVULNERABLE";
		text.GetComponent(TextMesh).renderer.material.color = Color(0.8,1,1);
	}	
	if(Game1Var.haste > 0)
	{
		renderer.material.mainTexture = textG;
		text.GetComponent(TextMesh).text = "HASTE";
		text.GetComponent(TextMesh).renderer.material.color = Color(0.8,1,0.8);
	}
	if(gauge>0)
	{
		transform.localScale.z = 25*gauge/10;
		text.GetComponent(TextMesh).renderer.material.color.a = Mathf.Lerp(text.GetComponent(TextMesh).renderer.material.color.a,1,0.033);
		GameObject.Find("Main Camera/Camera GUI").GetComponent(ScreenOverlay).intensity = gauge/10+0.2;
	}
	else
	{
		transform.localScale.z = 0;
		text.GetComponent(TextMesh).renderer.material.color.a = Mathf.Lerp(text.GetComponent(TextMesh).renderer.material.color.a,0,0.02);
		GameObject.Find("Main Camera/Camera GUI").GetComponent(ScreenOverlay).intensity = Mathf.Lerp(GameObject.Find("Main Camera/Camera GUI").GetComponent(ScreenOverlay).intensity,0,0.1);
	}
}