#pragma strict
import System.Collections.Generic;

var locations : List.<Vector3>; locations = new List.<Vector3>(); //contains locations for possible spawns
var xBound = 8.0;
var yBound = 5.0;

function Start ()
{
	//generates location vectors
	var currX = -xBound;
	var currY = -yBound;
	for(var i = 0; i<15; i++)
	{
		var newVector: Vector3;
		newVector.x = currX;
		newVector.y = currY;
		newVector.z = 0;
		locations.Add(newVector);
		
		currX += xBound/2;
		if(currX>xBound+0.1)
		{
			currX = -xBound;
			currY += yBound/2;
		}
	}
}

function Update ()
{
	if(transform.position.y - Camera.main.transform.position.y < 60)
	{
		RandomSpawn();
		transform.position.y += yBound*2;
	}
}

function RandomSpawn ()
{
	var spawnNum: int; spawnNum = Random.Range(4, 6); //chooses number of houses in this spawn area
	var chosenLocations : List.<Vector3>; chosenLocations = new List.<Vector3>();  //the chosen locations for spawning
	
	for(var i = 0; i<spawnNum; i++)
	{
		var randomLocation: Vector3;
		var ticker: boolean; ticker = false;
		randomLocation = Vector3(1,1,1);
		while(!ticker)
		{
			randomLocation = locations[Random.Range(0, 15)];
			if(!chosenLocations.Contains(randomLocation))
			{
				chosenLocations.Add(randomLocation);
				ticker = true;
			}
		}
		Instantiate(Game1Var.prefabHouse, randomLocation + transform.position + Vector3((Random.value-0.5)*1, (Random.value-0.5)*2, 0), transform.rotation);
	}
}