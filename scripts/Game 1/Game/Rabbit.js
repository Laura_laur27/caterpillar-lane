#pragma strict

//OTHER SCRIPTS
var upgradeScript: Game1SaveData; upgradeScript = GameObject.Find("Game 1 Save").GetComponent("Game1SaveData");
var upgradesList = new Upgrade[19]; upgradesList = upgradeScript.upgradesList;

//states
var underInvasion: boolean;

//parameters
var attack: float;
var health: float;
var speed: float; 
var speedCC: float; 
var target: GameObject;

//objects
var sprite: Transform;

function OnSpawned ()
{
	//adds count to total pop
	Game1Var.externalPop += 1;
	
	//generates health value
	health = Random.Range(0.0, 200.0)*Game1Stats.modHealth;
	
	sprite = transform.Find("Sprite");
	
	//changes sprite to 0
	if(upgradesList[17].equipped)
	{
		sprite.transform.localScale.x = 1;
		sprite.transform.localScale.z = 1;
		sprite.renderer.material.color = Color(1,1,1);
	}
	if(upgradesList[16].equipped || upgradesList[12].equipped)
	{
		sprite.renderer.material.color = Color(1,1,1);
	}
		
	//not underInvasion
	underInvasion = false;
	speed = 1;
	attack = 1;
	speedCC = 1;
}

function Update ()
{
	
	//gives new target if old one expires
	if(target == null)
	{
		target == Game1Var.garbageDestroyer;
	}
	
	//destroys rabbit when off screen
	if (transform.position.y - Camera.main.transform.position.y <= -17)
	{
		PoolManager.Pools["RabbitPool"].Despawn(transform);
	}
	
	//kills rabbit when health is low
	if (health <= 0)
	{
		Death ();
	}
	
	//updates parameters
	//attack
	attack = 2.5*Game1Stats.modAttack;
	
	//speed
	speed = 1*Game1Stats.modSpeed*speedCC;

	//vanguard mod
	if(upgradesList[17].equipped)
	{
		var vanMod: float;
		vanMod = (0.013*(transform.position.y-Game1Var.distanceTravelled) + 1.1333)*(0.1*(upgradesList[17].level)+0.9);
		attack *= vanMod;
		speed *= vanMod;
		if(transform.position.y-Game1Var.distanceTravelled>0)
		{
			sprite.transform.localScale.x = (transform.position.y-Game1Var.distanceTravelled)/9 + 1;
			sprite.transform.localScale.z = (transform.position.y-Game1Var.distanceTravelled)/7.5 + 1;
			if(!upgradesList[16].equipped)
			{
				sprite.renderer.material.color = Color.Lerp(Color(1,1,1), Color(1,0.5,0.7), (transform.position.y-Game1Var.distanceTravelled)/7.5);
			}
		}
	}
	
	//fleet
	if(upgradesList[16].equipped)
	{
		sprite.renderer.material.color = Color.Lerp(sprite.renderer.material.color, Color(1,1,1), 0.02);
	}
	
	//changes opacity based on health
	sprite.renderer.material.color.a = health/100;

	//interact with houses once close enough
	if (target != null && target.tag == "House")
	{
		InteractHouse();
	}
	if (target != null && target.tag == "Token")
	{
		InteractToken();
	}
	
	//with adventurous, breed outside
	if(upgradesList[10].equipped)
	{
		Adventure();
	}
	
	//starves rabbit over time - based on distance travelled;
	var starveDamage: float;
	starveDamage = 0.05*Game1Var.distanceTravelled + 10;
	TakeDamage(starveDamage);
	
	//if hotrods, damages near houses (when not attacking)
	if(upgradesList[15].equipped)
	{
		var damage: float; damage = (0.3*upgradesList[15].level + 0.25)*(1+(Game1Stats.modSpeed-1)*0.3); //determines damage of explosion based on level
		for(object in Physics.OverlapSphere(transform.position, 1.5))
		{
			if(object.collider.gameObject.tag == "House")
			{
				var script: House; script = object.collider.gameObject.GetComponent("House");
				script.health -= damage*Time.deltaTime;
			}
		}
	}
}

function FixedUpdate ()
{
	//moves rabbit towards destination
	if(target != null)
	{
		Travel ();
	}
	else
	{
		target == Game1Var.garbageDestroyer;
	}
}

function InteractHouse ()
{
	var toHouse: Vector3; //vector towards house
	toHouse = (target.transform.position - transform.position);
	toHouse.z = 0;
	if (toHouse.magnitude <= 2)//tries to go into house when close enough
	{
		GoIn ();
	}
	if (toHouse.magnitude <= 3)//attacks house when close enough
	{
		Invade ();
	}
}

function InteractToken ()
{
	var toToken: Vector3; //vector towards token
	toToken = (target.transform.position - transform.position);
	toToken.z = 0;
	if (toToken.magnitude <= 2)//go into token when close enough
	{
		GoInToken ();
	}
}

function SetTarget (house: GameObject)
{
	//sets the target of the rabbit
	target = house;
}

function Travel ()
{
	//moves rabbit towards destination
	transform.position = transform.position.MoveTowards(transform.position, target.transform.position + Vector3(0, 0, 0), speed*0.13);

	//rotates rabbit to face target house
	transform.LookAt(target.transform.position, Vector3.back);
}

function GoIn ()
{
	var script: House = target.GetComponent(House);
	
	//once Invaded, go into house
	if (script.health <= 0)
	{
		script.populationFloat += 1;
		PoolManager.Pools["RabbitPool"].Despawn(transform);
	}
}

function GoInToken ()
{
	var script: Token = target.GetComponent(Token);
	//go into token
	script.populationFloat += 1;
	PoolManager.Pools["RabbitPool"].Despawn(transform);
}

function Invade ()
{
	var scriptHouse: House = target.GetComponent(House);

	//if unInvaded, begin invasion
	if (!scriptHouse.underInvasion && scriptHouse.health > 0)
	{
		scriptHouse.underInvasion = true;
	}
	if (scriptHouse.underInvasion)
	{
		underInvasion = true;
		TakeDamage(scriptHouse.attack); //takes damage from house
		scriptHouse.health -= attack*Time.deltaTime; //damages house
	}
	if (scriptHouse.health <= 0)
	{
		scriptHouse.underInvasion = false;
	}
}

function TakeDamage (damage: float)
{
	if(!(Game1Var.invulnerable > 0))
	{
		health -= damage*Time.deltaTime*0.5;
	}
}

function Death ()
{
	//if furry fury, explode
	if(upgradesList[8].equipped && Random.value < 0.05)
	{
		var radius: float; radius = 1.3*upgradesList[8].level + 7.25;//determines radius of explosion based on level
		var damage: float; damage = 3.75*upgradesList[8].level + 1.25; //determines damage of explosion based on level
		for(object in Physics.OverlapSphere(transform.position, 1))
		{
			if(object.collider.gameObject.tag == "House")
			{
				var script: House; script = object.collider.gameObject.GetComponent("House");
				script.health -= 20*damage;
			}
		}
	}
	
	PoolManager.Pools["RabbitPool"].Despawn(transform);
}

function OnDespawned ()
{
	Game1Var.externalPop -= 1;
}

function Adventure ()
{
	var adventureTimer: float; adventureTimer = 2.0;
	if(adventureTimer>0)
	{
		adventureTimer -= Time.deltaTime;
	}
	else
	{
		//check if breeds
		var chance:float;
		chance = (0.02*upgradesList[10].level + 0.075)*(1+((Game1Stats.modBreedRate-1)*0.3)); //based on adventure level and breed rate
		
		if(Random.value<chance)
		{
			Instantiate(this);
			Debug.Log("YAY!");
		}
		//resets timer
		adventureTimer = 2.0;
	}
}