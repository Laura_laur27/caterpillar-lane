#pragma strict
var sprite: Transform;
var speed = 25.0f; //how fast it shakes
var rotate: float;
	
function Start ()
{
	sprite = transform.Find("Sprite");
	//renderer.material.color = Color(0.6,1,1);
}

function Update ()
{
	rotate = Mathf.Sin(Time.time * speed);
	transform.Rotate(Vector3(0, 0, rotate));
	
	//transform.position.y += 0.02;
	renderer.material.color.a -= 0.013;
	sprite.renderer.material.color.a -= 0.013;
	if (renderer.material.color.a <= 0)
	{
		Destroy (gameObject);
	}
}