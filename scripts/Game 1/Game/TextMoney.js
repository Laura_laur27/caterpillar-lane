#pragma strict

function Start ()
{
	renderer.material.color = Color.yellow;
}

function Update ()
{
	transform.position.y += 0.02;
	renderer.material.color.a -= 0.006;
	if (renderer.material.color.a <= 0)
	{
		Destroy (gameObject);
	}
}