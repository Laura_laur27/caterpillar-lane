#pragma strict
import System.Collections.Generic;

//scripts
var scriptVar: Game1Var; scriptVar = GameObject.Find("Controller1").GetComponent(Game1Var);
var scriptSave: Game1SaveData; scriptSave = GameObject.Find("Game 1 Save").GetComponent(Game1SaveData);

//controls death and reset for player
var deathWait: float; deathWait = 0.5;
var deathWait2: float; deathWait2 = 1.0;

function Start ()
{
 
}

function Update ()
{
	if (Game1Var.isGameOver)
	{
		Camera.main.SendMessage("fadeOut", 1);
		if (deathWait > 0)
		{
			deathWait -= Time.deltaTime;
		}
		else
		{
			//saves current stats
			Game1SaveData.lastGold = Game1Var.currGold;
			Game1SaveData.lastDistance = Game1Var.distanceTravelled;
			Game1SaveData.housesInvaded = Game1Var.housesInvaded;
			scriptSave.popList = scriptVar.popList;
			
			//goes to game over screen
			Application.LoadLevel(5);
			
			//Set gameover state to false, fade back in
			Game1Var.isGameOver = false;
			deathWait = 0.5;
			Camera.main.SendMessage("fadeIn", 1);
		}
	}
	if (deathWait2 > 0)
	{
		deathWait2 -= Time.deltaTime;
	}
	else
	{
		death();
	}
}

function death ()
{	
	//Checks if 0 rabbits or less
	if(Game1Var.totalPop <= 0)
	{
		Game1Var.isGameOver = true;
	}
}