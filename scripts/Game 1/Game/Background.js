#pragma strict

var check:boolean;
var timer = 1.0;
var newColor = new Color[6]; newColor = [Color.red, Color.blue, Color.yellow, Color.magenta];
var chosenColor: Color; chosenColor = Color.magenta;

function Start () {

}

function Update ()
{
	if(timer>0)
	{
		timer -= Time.deltaTime;
	}
	else
	{
		chosenColor = newColor[Random.Range(0,4)];
		timer = 0.5;
	}
	renderer.material.color = Color.Lerp(renderer.material.color, chosenColor, 0.05);
	transform.position.y = Game1Var.distanceTravelled;
}