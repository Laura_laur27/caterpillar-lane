#pragma strict

var ticker: float;

function Start ()
{
	ticker = Game1Var.beat;
}

function FixedUpdate ()
{

	
	//constantly reduces alpha to 0
	renderer.material.color.a -= 0.06;
	
	//every 1 second glow
	if (ticker <= 0)
	{
		renderer.material.color.a = 1.0;
		//Camera.mainCamera.GetComponent(ColorCorrectionCurves).saturation = 2;
		ticker = Game1Var.beat;
	}
	else
	{
		ticker -= Time.deltaTime;
	}
}